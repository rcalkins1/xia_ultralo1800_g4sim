//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: XIARun.hh 66536 2012-12-19 14:32:36Z ihrivnac $
//
/// \file XIARun.hh
/// \brief Definition of the XIARun class

#ifndef XIARun_h
#define XIARun_h 1

#include "G4Run.hh"
#include "globals.hh"

#include <vector>

class G4Event;

/// Run class
///

class XIARun : public G4Run
{
    public:
        XIARun();
        virtual ~XIARun();

        // method from the base class
        virtual void Merge(const G4Run*);

        void AddEdepVetoTop (G4double edep); 
        void AddEdepVetoBottom (G4double edep); 

        inline void ClearVectors(){
            Q_time .clear();
            pulse_time .clear();
            anode_wafer_Q .clear();
            anode_wafer_pulse .clear();
            guard_wafer_Q .clear();
            guard_wafer_pulse .clear();
            anode_full_Q .clear();
            anode_full_pulse .clear();
            guard_full_Q .clear();
            guard_full_pulse .clear();


            X1.clear();    
            Y1.clear();    
            Z1.clear();    
            T1.clear();    
            X3.clear();    
            Y3.clear();    
            Z3.clear();    
            T3.clear();    
            PX1.clear();   
            PY1.clear();   
            PZ1.clear();   
            KE1.clear();   
            Edep.clear();  
            Ptype.clear(); 
            PX3.clear();   
            PY3.clear();   
            PZ3.clear();   
            KE3.clear();   
            TrkID.clear(); 
            StepN.clear(); 


            primary_Ptype.clear();
            primary_X.clear();
            primary_Y.clear();
            primary_Z.clear();
            primary_PX.clear();
            primary_PY.clear();
            primary_PZ.clear();
            primary_KE.clear();
            primary_T.clear();



            fEdepVetoTop=0;
            fEdepVetoBottom=0;

            return;
        }

        // get methods
        G4double GetEdepVetoTop()  const { return fEdepVetoTop; }
        G4double GetEdepVetoBottom()  const { return fEdepVetoBottom; }


        std::vector<G4double>&  Get_Q_time()       {  return  Q_time       ;  }
        std::vector<G4double>&  Get_pulse_time()   {  return  pulse_time   ;  }


        std::vector<G4double>&  Get_anode_wafer_Q()      {  return  anode_wafer_Q      ;  }
        std::vector<G4double>&  Get_anode_wafer_pulse()  {  return  anode_wafer_pulse  ;  }
        std::vector<G4double>&  Get_guard_wafer_Q()      {  return  guard_wafer_Q      ;  }
        std::vector<G4double>&  Get_guard_wafer_pulse()  {  return  guard_wafer_pulse  ;  }
        std::vector<G4double>&  Get_anode_full_Q()      {  return  anode_full_Q      ;  }
        std::vector<G4double>&  Get_anode_full_pulse()  {  return  anode_full_pulse  ;  }
        std::vector<G4double>&  Get_guard_full_Q()      {  return  guard_full_Q      ;  }
        std::vector<G4double>&  Get_guard_full_pulse()  {  return  guard_full_pulse  ;  }

        std::vector<G4double>&  Get_X1()           {  return  X1;          }
        std::vector<G4double>&  Get_Y1()           {  return  Y1;          }
        std::vector<G4double>&  Get_Z1()           {  return  Z1;          }
        std::vector<G4double>&  Get_T1()           {  return  T1;          }
        std::vector<G4double>&  Get_X3()           {  return  X3;          }
        std::vector<G4double>&  Get_Y3()           {  return  Y3;          }
        std::vector<G4double>&  Get_Z3()           {  return  Z3;          }
        std::vector<G4double>&  Get_T3()           {  return  T3;          }
        std::vector<G4double>&  Get_PX1()          {  return  PX1;         }
        std::vector<G4double>&  Get_PY1()          {  return  PY1;         }
        std::vector<G4double>&  Get_PZ1()          {  return  PZ1;         }
        std::vector<G4double>&  Get_KE1()          {  return  KE1;         }
        std::vector<G4double>&  Get_Edep()         {  return  Edep;        }
        std::vector<G4double>&  Get_Ptype()        {  return  Ptype;       }
        std::vector<G4double>&  Get_PX3()          {  return  PX3;         }
        std::vector<G4double>&  Get_PY3()          {  return  PY3;         }
        std::vector<G4double>&  Get_PZ3()          {  return  PZ3;         }
        std::vector<G4double>&  Get_KE3()          {  return  KE3;         }
        std::vector<G4double>&  Get_TrkID()        {  return  TrkID;       }
        std::vector<G4double>&  Get_StepN()        {  return  StepN;       }

        std::vector<G4double>&  Get_primary_Ptype()  {  return  primary_Ptype;  }
        std::vector<G4double>&  Get_primary_X()      {  return  primary_X;      }
        std::vector<G4double>&  Get_primary_Y()      {  return  primary_Y;      }
        std::vector<G4double>&  Get_primary_Z()      {  return  primary_Z;      }
        std::vector<G4double>&  Get_primary_PX()     {  return  primary_PX;     }
        std::vector<G4double>&  Get_primary_PY()     {  return  primary_PY;     }
        std::vector<G4double>&  Get_primary_PZ()     {  return  primary_PZ;     }
        std::vector<G4double>&  Get_primary_KE()     {  return  primary_KE;     }
        std::vector<G4double>&  Get_primary_T()      {  return  primary_T;      }





    private:
        G4double  fEdepVetoTop;
        G4double  fEdepVetoBottom;


        std::vector<G4double> Q_time ;
        std::vector<G4double> pulse_time ;

        std::vector<G4double> anode_wafer_Q ;
        std::vector<G4double> anode_wafer_pulse ;
        std::vector<G4double> guard_wafer_Q ;
        std::vector<G4double> guard_wafer_pulse ;

        std::vector<G4double> anode_full_Q ;
        std::vector<G4double> anode_full_pulse ;
        std::vector<G4double> guard_full_Q ;
        std::vector<G4double> guard_full_pulse ;


        //Step vectors
        std::vector<G4double> X1;
        std::vector<G4double> Y1;
        std::vector<G4double> Z1;
        std::vector<G4double> T1;
        std::vector<G4double> X3;
        std::vector<G4double> Y3;
        std::vector<G4double> Z3;
        std::vector<G4double> T3;
        std::vector<G4double> PX1;
        std::vector<G4double> PY1;
        std::vector<G4double> PZ1;
        std::vector<G4double> KE1;
        std::vector<G4double> Edep;
        std::vector<G4double> Ptype;
        std::vector<G4double> PX3;
        std::vector<G4double> PY3;
        std::vector<G4double> PZ3;
        std::vector<G4double> KE3;
        std::vector<G4double> TrkID;
        std::vector<G4double> StepN;

        //Primaries vector
        std::vector<G4double> primary_Ptype;
        std::vector<G4double> primary_X;
        std::vector<G4double> primary_Y;
        std::vector<G4double> primary_Z;
        std::vector<G4double> primary_PX;
        std::vector<G4double> primary_PY;
        std::vector<G4double> primary_PZ;
        std::vector<G4double> primary_KE;
        std::vector<G4double> primary_T;



};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

