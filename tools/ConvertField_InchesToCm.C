#include <fstream>
#include <iostream>
#include <sstream>
#include <cstring>

using namespace std;

int main(int argc, char * argv[]){

    string input="", output="MyOutputField.dat" ; 


    if(argc<2){
        cerr<<"Specify an input file : ./" << argv[0] <<" <input> <output- optional> \n";
        return 0;
    }else if(argc==2){
        input=argv[1];
    }else if(argc>=3){
        input=argv[1];
        output=argv[2];
    }


    ifstream in;
    in.open(input.data());

    ofstream out;
    out.open(output.data());

    float X,Y,Z,EX,EY,EZ;

    while(true) {
        char line[856];
        in.getline(line, 856);
        if (!in.good()) break;

        char *searching = strchr(line,'#');
        if(searching != NULL) continue;
        searching = strchr(line,'@');
        if(searching != NULL) continue;
        searching = strchr(line,'%');
        if(searching != NULL) continue;



        stringstream ss ;
        ss << line;

        ss >> X >> Y >>Z >> EX>> EY >> EZ ;

        X*=2.54; 
        Y*=2.54; 
        Z*=2.54; 

      X -=  0.5*23.75*2.54; 
      Y -=  0.5*23.75*2.54; 
      Z -=  0.5*5.9*2.54; 
    
  //EX/100.0;
  //EY/100.0;
  //EZ/100.0;


        out<<X << " " << Y << " " << Z << " " << EX << " "<< EY << " " << EZ << endl;
    }


    in.close();
    out.close();

    return 0;
}







