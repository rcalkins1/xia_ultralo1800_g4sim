//******************************************************************************
// CRYPrimaryGeneratorAction.cc
//
// 1.00 JMV, LLNL, Jan-2007:  First version.
//******************************************************************************
//

#include <iomanip>
#include "CRYPrimaryGeneratorAction.hh"
using namespace std;

#include "G4Event.hh"
#include "G4SystemOfUnits.hh"


//----------------------------------------------------------------------------//
CRYPrimaryGeneratorAction::CRYPrimaryGeneratorAction(const char *inputfile)
{
    // define a particle gun
    particleGun = new G4ParticleGun();
    fGPS = new G4GeneralParticleSource();

    // Read the cry input file
    std::ifstream inputFile;
    inputFile.open(inputfile,std::ios::in);
    char buffer[1000];

    if (inputFile.fail()) {
        if( *inputfile !=0)  //....only complain if a filename was given
            G4cout << "CRYPrimaryGeneratorAction: Failed to open CRY input file= " << inputfile << G4endl;
        InputState=-1;
    }else{
        std::string setupString("");
        while ( !inputFile.getline(buffer,1000).eof()) {
            setupString.append(buffer);
            setupString.append(" ");
        }

        CRYSetup *setup=new CRYSetup(setupString,datadir);

        gen = new CRYGenerator(setup);

        // set random number generator
        RNGWrapper<CLHEP::HepRandomEngine>::set(CLHEP::HepRandom::getTheEngine(),&CLHEP::HepRandomEngine::flat);
        setup->setRandomFunction(RNGWrapper<CLHEP::HepRandomEngine>::rng);
        InputState=0;
    }
    // create a vector to store the CRY particle properties
    vect=new std::vector<CRYParticle*>;

    // Create the table containing all particle names
    particleTable = G4ParticleTable::GetParticleTable();

    // Create the messenger file
    gunMessenger = new CRYPrimaryGeneratorMessenger(this);

    //Z-offset
    Zoffset = 11*m+3*m; //I don't like hard coding this but the building isn't going to change much

    fMessenger = new G4GenericMessenger(this, "/CRY/", "CRY Primary Control" );
    G4GenericMessenger::Command&  ZoffsetCmd =  fMessenger->DeclarePropertyWithUnit("Zoffset", "m",  Zoffset,  "Z-position above XIA tray for cosmic ray origin plane"        );
    ZoffsetCmd.SetParameterName("Zoffset_file",  true , true);


}

//----------------------------------------------------------------------------//
CRYPrimaryGeneratorAction::~CRYPrimaryGeneratorAction()
{
    delete fMessenger;
}

//----------------------------------------------------------------------------//
void CRYPrimaryGeneratorAction::InputCRY()
{
    InputState=1;
}

//----------------------------------------------------------------------------//
void CRYPrimaryGeneratorAction::UpdateCRY(std::string* MessInput)
{
    CRYSetup *setup=new CRYSetup(*MessInput,datadir);

    gen = new CRYGenerator(setup);

    // set random number generator
    RNGWrapper<CLHEP::HepRandomEngine>::set(CLHEP::HepRandom::getTheEngine(),&CLHEP::HepRandomEngine::flat);
    setup->setRandomFunction(RNGWrapper<CLHEP::HepRandomEngine>::rng);
    InputState=0;


    //Zoffset= 
}

//----------------------------------------------------------------------------//
void CRYPrimaryGeneratorAction::CRYFromFile(G4String newValue)
{
    // Read the cry input file
    std::ifstream inputFile;
    inputFile.open(newValue,std::ios::in);
    char buffer[1000];

    if (inputFile.fail()) {
        G4cout << "Failed to open input file " << newValue << G4endl;
        G4cout << "Make sure to define the cry library on the command line" << G4endl;
        InputState=-1;
    }else{
        std::string setupString("");
        while ( !inputFile.getline(buffer,1000).eof()) {
            setupString.append(buffer);
            setupString.append(" ");
        }

        CRYSetup *setup=new CRYSetup(setupString,datadir);

        gen = new CRYGenerator(setup);

        // set random number generator
        RNGWrapper<CLHEP::HepRandomEngine>::set(CLHEP::HepRandom::getTheEngine(),&CLHEP::HepRandomEngine::flat);
        setup->setRandomFunction(RNGWrapper<CLHEP::HepRandomEngine>::rng);
        InputState=0;
    }
}

//----------------------------------------------------------------------------//
void CRYPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{ 
    if (InputState != 0) {
        G4String* str = new G4String("CRY library was not successfully initialized");
        //G4Exception(*str);
        G4Exception("CRYPrimaryGeneratorAction", "1",
                RunMustBeAborted, *str);
    }
    G4String particleName;
    vect->clear();
    gen->genEvent(vect);

    //....debug output
    //  G4cout << "\nEvent=" << anEvent->GetEventID() << " "
    //       << "CRY generated nparticles=" << vect->size()
    //       << G4endl;

    for ( unsigned j=0; j<vect->size(); j++) {
        particleName=CRYUtils::partName((*vect)[j]->id());

        //....debug output  
        //  cout << "  "          << particleName << " "
        //   << "charge="      << (*vect)[j]->charge() << " "
        //   << setprecision(4)
        //   << "energy (MeV)=" << (*vect)[j]->ke()*0.001<< " "
        //   << "pos (m)"
        //   << G4ThreeVector((*vect)[j]->x(), (*vect)[j]->y(), (*vect)[j]->z()  )
        //   << " " << "direction cosines "
        //   << G4ThreeVector((*vect)[j]->u(), (*vect)[j]->v(), (*vect)[j]->w())
        //   << " " << endl;

        particleGun->SetParticleDefinition(particleTable->FindParticle((*vect)[j]->PDGid()));
        particleGun->SetParticleEnergy((*vect)[j]->ke()*MeV);
        particleGun->SetParticlePosition(G4ThreeVector((*vect)[j]->x()*m , (*vect)[j]->y()*m, (*vect)[j]->z()*m   + Zoffset   ) );
        particleGun->SetParticleMomentumDirection(G4ThreeVector((*vect)[j]->u(), (*vect)[j]->v(), (*vect)[j]->w()));

        //If you want to shoot straight down
        //particleGun->SetParticleMomentumDirection(G4ThreeVector(0,0, (*vect)[j]->w()));

        //particleGun->SetParticleTime((*vect)[j]->t());
        particleGun->SetParticleTime(0);

        particleGun->GeneratePrimaryVertex(anEvent);
        delete (*vect)[j];
    }
}
