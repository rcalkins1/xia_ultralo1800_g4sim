#!/bin/bash

#This is your working directory by finding out where this script lives
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"


. /hpc/group/cdms2/setup.sh 


export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$QTLIBPATH


#We are going to set G4WORKDIR to our directory to keep track of our binaries
G4WORKDIR=${DIR}

#add it to the path
PATH=$G4WORKDIR/bin/Linux-g++/:$PATH
export HEED_DATABASE=$GARFIELD_HOME/share/Heed/database
export LD_LIBRARY_PATH=$GARFIELD_HOME/lib64/:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$CRYHOME/lib/:$LD_LIBRARY_PATH

#module load gcc-6.3 
