#!/bin/python
#This script calls on FEniCS project (https://fenicsproject.org/) to calculate the various fields by solving 
#the Laplace equation with the boundary conditions of the experiment and then taking the gradiant to determine the 
#field.  The default is for an empty chamber but it should #be trivial to extend this to various case studies such 
#as a 'thick' sample or a sample that has some charge on it.#There are five fields calculated, one E-field and the
#four Ramo weighing fields. Begin by installing FEniCS and setup your enviroment:
#>source activate fenicsproject
#then execution is simply:
#>python FenicsFields.py
#it will take on the order of ~10 m to finish and you should find yourself with five FENICS_*txt files when it has
#completed. 


# We are going to use Fenics to calculate the various fields so let's load up our packages
from dolfin import *
import matplotlib.pyplot as plt

#Let's name our output something meaningful
prefix="FENICS_gap"

#define the main box
#The bottom is at z=0 and we'll use cm as our units
#x-y = 0.5*23.75 inches *2.54 cm/inch = 30.1625
#We'll use a 100x100x100 point grid 
npoints=100
box = BoxMesh(Point( float(-30.1625) ,float( -30.1625) , -7.5 ), Point( float(30.1625) , float(30.1625) , 7.5 )  , npoints , npoints , npoints  )

#Create function space
V = FunctionSpace( box, "P", 1)

#Let's define one function to do the calculation because all we are really doing is changing boundary conditions
def CalculateField(boundary, name):

    #Now that everything is setup, let's solve E-M
    u = TrialFunction(V)
    v = TestFunction(V)
    f=Constant(0.0)
    a = dot(grad(u), grad(v))*dx
    #a = inner(grad(u), grad(v))*dx
    #L = Constant(0)*v*ds + Constant(0)*v*dx
    L=f*v*dx 

    #Actually solve this
    u = Function( V )
    solve( a == L , u, boundary , solver_parameters=dict(linear_solver='gmres', preconditioner='ilu')) 

    #Set boundary conditions 
    # Plot solution of potential 
    #plot(u.leaf_node(), title="Solution on final mesh")
    #plt.show()
    
    #Calculate the field
    E = project(-grad(u) , solver_type="gmres", preconditioner_type="ilu" )
    #E = project(-grad(u) , solver_type="cg", preconditioner_type="amg" )
    #plot(E,mode="glyphs", wireframe=True )
    #plt.show()
    WriteFieldToFile(E, name )
    
    pvdout = name.replace('.txt', '.pvd') 
    field = File(pvdout )
    field  << E

    pvdout = pvdout.replace('.pvd', '_potential.pvd' ) 
    field = File(pvdout )
    field  << u

#Define a function to write out our solution
def WriteFieldToFile(sol, output="test.txt"):
    f_out=open(output, "w")

    #Calculate step size, loop over and dump the numbers to a file
    XYstep = 2.0*30.1625/npoints
    Zstep = 15.0/npoints
    for k in range(0,npoints):
        Z= -7.5 + (0.5+k)*Zstep 
        for j in range(0,npoints):
            Y= -30.1625 + (0.5+j)*XYstep 
            for i in range(0,npoints):
                X= -30.1625 + (0.5+i)*XYstep 
                val=sol(X,Y,Z)
                f_out.write( str(X)+" "+
                    str(Y)+" "+
                    str(Z)+" "+
                    str(val[0])+" "+
                    str(val[1])+" "+
                    str(val[2])+ "\n") 
    f_out.close()



# 1100 V on the top, grounded tray and linear voltage drop on the sides
HV = 1100.0 
V_z = Constant( HV ) 
ground = Constant( 0.0 )

#There is a small gap between the electrodes 
gap=0.5*.25*2.54    

bctop    = DirichletBC( V, V_z    ,  " near(x[2],  7.5 ) " ) 
bcbottom = DirichletBC( V, ground ,  " near(x[2], -7.5 ) " ) 

#Set the sides potential
bcsides = DirichletBC( V, Expression("(x[2]+7.5)*1100.0/15.0", degree=1 ),  " near(x[0] , -30.1625, 10*DOLFIN_EPS  ) || near( x[0] , 30.1625, 10*DOLFIN_EPS  ) || near( x[1] , -30.1625, 10*DOLFIN_EPS  ) || near( x[1] , 30.1625, 10*DOLFIN_EPS  ) ") 

#Let's make a simple grounded sample as an example. Pretend it is a circular piece of Copper that covers the 'wafer' : r^2 = 707/pi
bcsample = DirichletBC( V, ground,  " x[0]*x[0] + x[1]*x[1] < 225.05-0.3175 &&  near(x[2], -7.5 )  ") 

#List of boundary conditions
bc=[ bcsides, bctop, bcbottom , bcsample ]

#We are setup for the efield so let's turn the crank
CalculateField(bc, prefix+"_Efield.txt" )

#Let's move on to the weighing fields. We'll start by grounding the non-anodes
bcsides.set_value( ground )
bcsample.set_value( ground )

#Now need to define domains for the various anodes with 1V for hte anode of interest
V_ramo=Constant(1.0) 
bcwafer      = DirichletBC( V, ground,  " sqrt(x[0]*x[0] + x[1]*x[1]) < 15.00 - 0*0.3175 &&  near(x[2], 7.5 )  ") 
bcwaferguard = DirichletBC( V, ground,  " sqrt(x[0]*x[0] + x[1]*x[1]) > 15.00 +2*0.3175 &&  near(x[2], 7.5 )  ") 
bcfull       = DirichletBC( V, ground,  " x[0] > -21.209+2*0.3175 && x[0] < 21.209-0*0.3175 &&  x[1] > -21.209+2*0.3175 && x[1] < 21.209-0*0.3175  &&  near(x[2], 7.5 )  ") 
bcfullguard  = DirichletBC( V, ground,  "(x[0] < -21.209-0*0.3175 || x[0] > 21.209+2*0.3175 ||  x[1] < -21.209-0*0.3175 || x[1] > 21.209+2*0.3175) &&  near(x[2], 7.5 )  ") 

#bcwafer      = DirichletBC( V, ground,  " x[0]*x[0] + x[1]*x[1] < 225.05-0.3175 &&  near(x[2], 7.5 )  ") 
#bcwaferguard = DirichletBC( V, ground,  " x[0]*x[0] + x[1]*x[1] > 225.05+0.3175 &&  near(x[2], 7.5 )  ") 
#bcfull       = DirichletBC( V, ground,  " x[0] > -21.209+0.3175 && x[0] < 21.209-0.3175 &&  x[1] > -21.209+0.3175 && x[1] < 21.209-0.3175  &&  near(x[2], 7.5 )  ") 
#bcfullguard  = DirichletBC( V, ground,  "(x[0] < -21.209-0.3175 || x[0] > 21.209+0.3175 ||  x[1] < -21.209-0.3175 || x[1] > 21.209+0.3175) &&  near(x[2], 7.5 )  ") 


#Start with the wafer
bcwafer.set_value(V_ramo)
CalculateField([bcsides, bcbottom, bcsample, bcwafer, bcwaferguard],  prefix+"_WfieldAnodeWafer.txt" )

bcwafer.set_value(ground)
bcwaferguard.set_value(V_ramo)
CalculateField([bcsides, bcbottom, bcsample, bcwafer, bcwaferguard],  prefix+"_WfieldGuardWafer.txt" )


#now the full tray 
bcfull.set_value(V_ramo)
CalculateField([bcsides, bcbottom, bcsample, bcfull, bcfullguard],  prefix+"_WfieldAnodeFull.txt" )

bcfull.set_value(ground)
bcfullguard.set_value(V_ramo)
CalculateField([bcsides, bcbottom, bcsample, bcfull, bcfullguard],  prefix+"_WfieldGuardFull.txt" )








