/*
 * GarfieldPhysics.hh
 *
 *  Created on: Jul 17, 2014
 *      Author: dpfeiffe
 */

#ifndef GARFIELDMODELCONFIG_HH_
#define GARFIELDMODELCONFIG_HH_

#include <map>
#include <vector>
#include <iostream>
#include <string>


#include "Sensor.hh"
#include "AvalancheMC.hh"
#include "AvalancheMicroscopic.hh"
#include "ComponentAnalyticField.hh"
#include "ComponentConstant.hh"
#include "ComponentVoxel.hh"
#include "TrackHeed.hh"
#include "TrackSimple.hh"
#include "MediumMagboltz.hh"
#include "GeometryRoot.hh"
#include "GeometrySimple.hh"
#include "SolidTube.hh"
#include "SolidBox.hh"

#include "G4GenericMessenger.hh"
//#include "GarfieldPhysicsMessenger.hh"


#include "TCanvas.h"
#include "TH1D.h"
//#include "ViewCell.hh"
//#include "ViewDrift.hh"
#include "DriftLineRKF.hh"
#include "TPolyMarker3D.h"




#include "G4VFastSimulationModel.hh"

typedef std::pair<G4double, G4double> EnergyRange_keV;
typedef std::map< const std::string, EnergyRange_keV> MapParticlesEnergy;

class GarfieldElectron{
    public:
        GarfieldElectron( G4double ekin_eV,G4double time, G4double x_cm,G4double y_cm,G4double z_cm, G4double dx,G4double dy,G4double dz):fEkin_MeV(ekin_eV/1000000), fTime(time), fx_mm(10*x_cm),fy_mm(10*y_cm), fz_mm(10*z_cm),  fdx(dx), fdy(dy), fdz(dz){}
        ~GarfieldElectron(){};

        G4double getX_mm() {return fx_mm;}
        G4double getY_mm(){return fy_mm;}
        G4double getZ_mm(){return fz_mm;}
        G4double getEkin_MeV(){return fEkin_MeV;}
        G4double getTime(){return fTime;}
        G4double getDX(){return fdx;}
        G4double getDY(){return fdy;}
        G4double getDZ(){return fdz;}


    private:
        G4double fEkin_MeV, fTime, fx_mm,fy_mm,fz_mm,fdx,fdy,fdz;

};


class GarfieldPhysics {
    public:
        static GarfieldPhysics* GetInstance();
        static void Dispose();

        void InitializePhysics();
        void CreateGeometry();

        void DoIt(std::string particleName, G4double ekin_keV,G4double time,
                G4double x_cm, G4double y_cm, G4double z_cm, G4double dx, G4double dy, G4double dz  ,//);
             const G4FastTrack& fastTrack,
             G4FastStep& fastStep) ;

        void AddParticleName(const std::string particleName, G4double ekin_min_keV, G4double ekin_max_keV);
        bool FindParticleName(const std::string name);
        bool FindParticleNameEnergy(std::string name, G4double ekin_keV);
        std::vector<GarfieldElectron*>* GetSecondaryElectrons();
        void DeleteSecondaryElectrons();
        inline void EnableCreateSecondariesInGeant4() {createSecondariesInGeant4 = true;};
        inline void DisableCreateSecondariesInGeant4() {createSecondariesInGeant4 = false;};
        inline bool GetCreateSecondariesInGeant4() {return createSecondariesInGeant4;};
        inline void EnableElectronDrift(bool dodrift=true){ fEnableElectronDrift = dodrift; }
        inline void EnableIonDrift(bool dodrift=true){ fEnableIonDrift = dodrift; }
        inline G4double GetEnergyDeposit_MeV() {return fEnergyDeposit/1000000;};
        inline G4double GetAvalancheSize() {return fAvalancheSize;};
        inline G4double GetGain() {return fGain;};
        inline void Clear() {fEnergyDeposit=0;fAvalancheSize=0;fGain=0;nsum=0;  
            h_pulse_anode_full ->Reset();
            h_pulse_anode_wafer ->Reset();
            h_pulse_guard_full ->Reset();
            h_pulse_guard_wafer ->Reset();
            fSensor->ClearSignal(); 
            g_drift->Clear();
            fFirstInstance = true; 
            fTimeOffset =0;
    
        }
        inline G4double GetAnodePulseBin(int bin){ return fSensor->GetSignal("AnodeSignal", bin ); }
        inline G4double GetGuardPulseBin(int bin){ return fSensor->GetSignal("GuardSignal", bin ); }
        inline TH1F * GetAnodePulseFull(){return h_pulse_anode_full; }
        inline TH1F * GetGuardPulseFull(){return h_pulse_guard_full; }
        inline TH1F * GetAnodePulseWafer(){return h_pulse_anode_wafer; }
        inline TH1F * GetGuardPulseWafer(){return h_pulse_guard_wafer; }
        inline TPolyMarker3D * GetDrift(){return g_drift; }

        inline std::vector<double> GetElectronStepPoint(int i){return (electronStepPoints)[i]; }
        inline int GetElectronNums(){return electronStepPoints.size(); }
        inline std::vector<double> GetElectronMomPoint(int i){return (electronMomPoints)[i]; }
        inline int GetElectronMomNums(){return electronMomPoints.size(); }
        inline void ClearElectronMomPoints(){electronMomPoints.clear(); }


        inline  void  SetEfield(G4String              name){  G4cout<<"Setting  file  "<<  name<<  G4endl;  Efield_file              =  name;  }
        inline  void  SetWfield_anode_full(G4String   name){  G4cout<<"Setting  file  "<<  name<<  G4endl;  Wfield_anode_full_file   =  name;  }
        inline  void  SetWfield_anode_wafer(G4String  name){  G4cout<<"Setting  file  "<<  name<<  G4endl;  Wfield_anode_wafer_file  =  name;  }
        inline  void  SetWfield_guard_full(G4String   name){  G4cout<<"Setting  file  "<<  name<<  G4endl;  Wfield_guard_full_file   =  name;  }
        inline  void  SetWfield_guard_wafer(G4String  name){  G4cout<<"Setting  file  "<<  name<<  G4endl;  Wfield_guard_wafer_file  =  name;  }

        inline  G4String  GetEfield(){              return  Efield_file              ;  }
        inline  G4String  GetWfield_anode_full(){   return  Wfield_anode_full_file   ;  }
        inline  G4String  GetWfield_anode_wafer(){  return  Wfield_anode_wafer_file  ;  }
        inline  G4String  GetWfield_guard_full(){   return  Wfield_guard_full_file   ;  }
        inline  G4String  GetWfield_guard_wafer(){  return  Wfield_guard_wafer_file  ;  }

        inline  G4double  CalcQ_anode_full(   G4double  x,  G4double  y,  G4double  z,  G4double  vx,  G4double  vy,  G4double  vz){  G4double  ex,ey,ez;  G4int  status=0;  Garfield::Medium  *  medium=  fMediumMagboltz;  fComponentVoxel_Wfield_anode_full   ->ElectricField(  x,  y,  z  ,  ex,  ey,  ez,  medium,  status  );  return vx*ex  + vy*ey  + vz*ez;  }
        inline  G4double  CalcQ_anode_wafer(  G4double  x,  G4double  y,  G4double  z,  G4double  vx,  G4double  vy,  G4double  vz){  G4double  ex,ey,ez;  G4int  status=0;  Garfield::Medium  *  medium=  fMediumMagboltz;  fComponentVoxel_Wfield_anode_wafer  ->ElectricField(  x,  y,  z  ,  ex,  ey,  ez,  medium,  status  );  return vx*ex  + vy*ey  + vz*ez;  }
        inline  G4double  CalcQ_guard_full(   G4double  x,  G4double  y,  G4double  z,  G4double  vx,  G4double  vy,  G4double  vz){  G4double  ex,ey,ez;  G4int  status=0;  Garfield::Medium  *  medium=  fMediumMagboltz;  fComponentVoxel_Wfield_guard_full   ->ElectricField(  x,  y,  z  ,  ex,  ey,  ez,  medium,  status  );  return vx*ex  + vy*ey  + vz*ez;  }
        inline  G4double  CalcQ_guard_wafer(  G4double  x,  G4double  y,  G4double  z,  G4double  vx,  G4double  vy,  G4double  vz){  G4double  ex,ey,ez;  G4int  status=0;  Garfield::Medium  *  medium=  fMediumMagboltz;  fComponentVoxel_Wfield_guard_wafer  ->ElectricField(  x,  y,  z  ,  ex,  ey,  ez,  medium,  status  );  return vx*ex  + vy*ey  + vz*ez;  }


        inline G4double GetTimeOffset(){ return fTimeOffset; }
        inline void SetTimeOffset(G4double t){ fTimeOffset=t; }
        inline bool GetFirstInstance(){ return fFirstInstance; }
        inline void SetFirstInstance(bool fI){ fFirstInstance = fI; }


        void UpdateGarfield();


    private:
        GarfieldPhysics();
        ~GarfieldPhysics();


        static GarfieldPhysics* fGarfieldPhysics;
        MapParticlesEnergy* fMapParticlesEnergy;
        TGeoManager* fGeoManager;
        Garfield::MediumMagboltz* fMediumMagboltz;
        Garfield::Sensor* fSensor;
        Garfield::AvalancheMC* fDrift;
        Garfield::AvalancheMicroscopic* fAvalanche;
        Garfield::TrackHeed* fTrackHeed;
        Garfield::GeometryRoot* fGeometryRoot;
        Garfield::GeometrySimple* fGeometrySimple;
        Garfield::ComponentAnalyticField* fComponentAnalyticField;
        Garfield::ComponentConstant * fComponentConstant;
        Garfield::SolidBox* fBox;

        Garfield::ComponentVoxel * fComponentVoxel_Efield;
        Garfield::ComponentVoxel * fComponentVoxel_Wfield_anode_full;
        Garfield::ComponentVoxel * fComponentVoxel_Wfield_anode_wafer;
        Garfield::ComponentVoxel * fComponentVoxel_Wfield_guard_full;
        Garfield::ComponentVoxel * fComponentVoxel_Wfield_guard_wafer;

        std::vector<GarfieldElectron*>* fSecondaryElectrons;

        bool createSecondariesInGeant4;
        bool fEnableElectronDrift;
        bool fEnableIonDrift;

        G4double fEnergyDeposit;
        G4double fAvalancheSize;
        G4double fGain;
        int nsum;

        G4String Efield_file;
        G4String Wfield_anode_full_file;
        G4String Wfield_anode_wafer_file;
        G4String Wfield_guard_full_file;
        G4String Wfield_guard_wafer_file;
        int nXmesh, nYmesh,nZmesh;

        G4String Gasfile;
        G4String Ionfile;


        TCanvas* myCanvas ;
        //      Garfield::ViewCell* cellView ;
        //      Garfield::ViewDrift * vd;
        Garfield::DriftLineRKF* driftline_i ;
        TH1F * h_pulse_anode_full ;
        TH1F * h_pulse_anode_wafer ;
        TH1F * h_pulse_guard_full ;
        TH1F * h_pulse_guard_wafer ;
        TPolyMarker3D       * g_drift ;
        std::vector<std::vector<G4double>> electronStepPoints;
        std::vector<G4double> electronStepPoint;
        std::vector<std::vector<G4double>> electronMomPoints;
        std::vector<G4double> electronMomPoint;

        //GarfieldPhysicsMessenger * fMessenger;
        G4GenericMessenger * fMessenger;

        G4double fTimeOffset; 
        bool fFirstInstance;

};
#endif /* GARFIELDMODELCONFIG_HH_ */
