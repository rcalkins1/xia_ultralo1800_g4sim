
This is a Geant4 based XIA UltraLo-1800 simulation. It builds heavily on other publicly accessible codes. It calculates ionization and the induced charge on the anode from the track drift. The field maps were calculated using COMSOL\R for both the physical electric field and the Ramo-Shockely weighing fields.
Ionization and drift is provided by Garfield++ (https://garfieldpp.web.cern.ch/garfieldpp/), inconjunction with Magboltz,  and the induction is calculated internally. Output is giving in ROOT format but if a user was so inclined, the ROOT dependence could be eliminated if needed. 
Currently the best overview is in Andrew Posada's thesis, https://www.physics.smu.edu/web/research/preprints/2017_PosadaAndrew_Thesis.pdf . 

This software is provided AS-IS with no expectation that it works! Due to the developer's busy schedule and other commitments, do not expect any sort of support.


This package has been developed (albeit from a lot of existing code) by Rob Calkins, Andrew Posada and Joshua Ange of SMU.



# Introduction 
We utilize Geant4 to simulate the path length of alphas transversing through the Argon counting gas.

#  Geometry 
The geometry module consists of the XIA detector and may also contain a portion of Fondren Science building for cosmic ray simulations. The XIA itself consists of a .2cm thick G4_Al shell located 'World' volume which is a 2m cube for most cases.   The G4_Ar gas layer is a 23.75"x23.75"x15cm sized box that has a uniform -66.7 V/cm z^^ E-field applied. The XIA has been lifted up so that the top of the tray is at Z#0 and the center is at (0,0) in the X-Y plane. **The bottom/center of the XIA will always be at (0,0,0).**

## Field maps
The required E-field and Ramo weighing fields are provided with this repository. These were calculated using COMSOL. Since COMSOL is commercial software, an alternative is provided since users may be interested in simulating various different configurations 
where the empty chamber field is distored either due to a charged sample or an over sized sample. A python script is provided in tools/FenicsField.py that utilizes the FEniCS finite element PDE solver to calculate the fields. This can be modified to calculate 
fields for specific studies of this type. 

# Drifting Electrons 
GEANT4 does not handle drifting of charged particles but it is possible to hand that aspect off to another program. We choose to use the GARFIELD++ package(along with HEED) to handle this for use.  As ionization clusters are created, they are killed in Geant4 and handed off to Garfield for drifting and anode ionization. 


# Setup 
This package relies on the following packages.

  * [GEANT4](https://geant4.web.cern.ch/geant4/) - Installed on ManeFrame
  * [ROOT](https://root.cern.ch/) - Installed on ManeFrame
  * [GARFIELD++](http://cern.ch/garfieldpp/) - Group installation on ManeFrame on /scratch/group/cdms/garfield/ 
  * [CRY](http://nuclear.llnl.gov/simulation/) - Group installation on ManeFrame on /scratch/group/cdms/cry_v1.7/ 

It also relies indirectly on inputs derived from.

  * [COMSOL](https://www.comsol.com/) - Modeling of E-fields with samples
  * [FEniCS](https://fenicsproject.org/) - Alternative to COMSOL  
  * [Magboltz](http://magboltz.web.cern.ch/magboltz/) - parameters for charge transport in gases 



Source the setup.sh bash script ('source setup.sh') to setup Geant4, ROOT and your PATH variable. This is valid for ManeFrame only. If you are on a different system, you are on your own. The setup script with setup the following dependencies for you :

  * ROOT
  * gcc/5.1.0
  * geant4/10.04
  * garfield++   - for electron drifting and ionization calcuation
  * CRY - Cosmic ray primary generator

The program can simply be compiled by calling 'make lib;make bin'. This will create binary executables.  

## Noise
The electronics/pulse simulation can add noise to the traces. This requires an external library of pulses that you can supply using your XIA or your can download a set that was collected with SMU's XIA Peruna. The set of "Peruna pulses" is availiable from [https://smu.box.com/s/grs97ookx6hl732c2gtihl6eeva1xlmw](https://smu.box.com/s/grs97ookx6hl732c2gtihl6eeva1xlmw). Macro commands are availiable to disable the noise if you desire and to also specify the location of your noise library. 


#  Usage 
Assuming you have correctly set things up, you should now have some binaries in  your path. They are:

  * XIA_sim - This is the default executable that uses the Garfield physics list for most particles except alphas which use  CDMS low-ion modeling physics processes. The primary particle generator is [https://geant4.web.cern.ch/geant4/UserDocumentation/UsersGuides/ForApplicationDeveloper/html/ch02s07.html](General Particle Source)
  * XIA_sim_cosmic - This uses the same XIA physics as above however the primary particle generator is [http://nuclear.llnl.gov/simulation/](CRY).  

They can be run on their own for visualization or an interactive session however they will most commonly be run while supplying a macro with commands to execute.
<code>XIA_sim Th230.mac </code>


## CounterMeasure ##
If you run the pulses through CounterMeasure with the buffer writer, they need an ADC to MeV conversion. This is a parameter in the countermeasure settings file, which is accessible through the "Settings" in the gui. Our bestfit calibration is 0.0020286 MeV/ADC for simulation versus the 0.0019601 for data.

#  Macros 
Macros contain lists of instructions for the executables to run and can configure several things during run time. 

  * Th230.mac - No sample, uniform E-Field, 4.69 MeV alphas emitted from a 2mm x 1" diameter circular source biased to only emit in the upper hemisphere. 
  * Po210_contamination.mac - No sample, Po210 is uniformly implanted in the Al tray, biased upwards. 
  * PNNL_electroformed.mac - PNNL electroformed Cu with contaminents
  * Rn222.mac - Rn222 distributed randomly throughout the counting gas
  * cosmicmuons.mac - Macro file for cosmic ray simulation (not just muons) 

## Custom Macro Commands 
G4 gives the user the ability to create custom macro commands to control parts of the simulation. We utilize these so that we can modify aspects of the simulation without having to recompile the package. A list of available commands, custom and standard, is available from the binary by doing:
''/control/manual''

## General 
  * /XIAEventAction/WriteEmptyEvents <true/false> - false by default so that we don't waste disk space writing uninteresting events to disk
  * /XIAEventAction/ReportEvents <true/false> - Print a statement every N events. Default # true
  * /XIAEventAction/ReportEventsN <int> - Set the N for event report. Default 100000
  * /XIAEventAction/DoNoise <true/false> - Add noise to pulses from pulse library
  * /XIAEventAction/NoiseLib <string> - location of noise library file
  * /CRY/Zoffset <Z> <unit> - Z-offset for the cosmic ray origin plane. Default  44m 
  * /CRY/data <string> - Location of CRY data directory. Default /scratch/group/cdms/cry_v1.7/data/ 


##  Garfield 
  * /GarfieldPhysics/Efield_file <string> - Location  of  input  E-field  file for drift
  * /GarfieldPhysics/WfieldAnodeFull_file <string> - Location  of  input  W-field  anode-full   file
  * /GarfieldPhysics/WfieldAnodeWafer_file <string> - Location  of  input  W-field  anode-wafer  file
  * /GarfieldPhysics/WfieldGuardFull_file <string> - Location  of  input  W-field  guard-full   file
  * /GarfieldPhysics/WfieldGuardWafer_file <string> - Location  of  input  W-field  guard-wafer  file
  * /GarfieldPhysics/n{X,Y,Z}mesh <int> - Number of nodes along the X,Y or Z axis
  * /GarfieldPhysics/EnableElectronDrift <true/false> - turned on by default, can be turned off for fast sim at expense of drifting/pulses
  * /GarfieldPhysics/EnableIonDrift <true/false> - turned off by default, ions move so slowly this takes a lot of CPU and their contribution to the induced signal is negligible 
  * /GarfieldPhysics/Gasfile <string> - Location of Magboltz gas file  Default:GasData/ar-100_20psi.gas
  * /GarfieldPhysics/Ionfile <string> - Location of ion mobilization file Default:GasData/IonMobility_Ar+_Ar.txt

## Geometry  
The geometry always consists of the XIA along with its frame.  Additional structures can be added however, once added they cannot be removed.  

  * /XIA/BuildFondren <true/false> - Build concrete walls of Fondren science and clean room  Default:false
  * /XIA/BuildVeto <true/false> - Build four .5x1 m scintillator panels around the XIA  Default:false
  * /XIA/BuildTrayLiner <true/false> - Build a thin (0.051 mm) conductive teflon sheet on the bottom of the XIA tray. You will typically have to move your source above this if you include it.  Default:false
  * /XIA/UpdateGeometry - Needs to be called after any and all changes have been made.  


# Output 
 The output is step information dumped into a ROOT ntuple. There is not a mapping between the steps and the pulse since the induction is caused by the drift electrons which we do not output. We currently only record alpha step information since that is the most interesting.  Units for distance are meters. Here are the branches:
  
  * EventNum (Double_T) - number of event in G4 generation
  * anode_full_Q (vector<double>) - induced charge on the full anode electrode 
  * anode_full_pulse (vector<double>) - induced full configuration charge run through electronics sim
  * guard_full_Q (vector<double>) - induced charge on the guard electrode for the full configuration
  * guard_full_pulse (vector<double>) - induced ull configuration guard charge run through electronics sim
  * anode_wafer_Q (vector<double>) - induced charge on the wafer anode electrode 
  * anode_wafer_pulse (vector<double>) - induced wafer configuration charge run through electronics sim
  * guard_wafer_Q (vector<double>) - induced charge on the guard electrode for the wafer configuration
  * guard_wafer_pulse (vector<double>) - induced wafer configuration guard charge run through electronics sim
  * pulse_time  (vector<double>) - Time steps for pulse (s)
  * Q_time  (vector<double>) - Time steps for induction (s)
  * Edep  (vector<double>) - Energy deposited during step
  * Ptype (vector<double>) - PDG particle code
  * TrkID (vector<double>) - Id number of the track associated with the step
  * StepN (vector<double>) - Step number for the track
  * X1 (vector<double>) - x-position at beginning of step
  * Y1 (vector<double>) - y-position at beginning of step
  * Z1 (vector<double>) - z-position at beginning of step
  * T1 (vector<double>) - time at beginning of step
  * X3 (vector<double>) - x-position at end of step
  * Y3 (vector<double>) - y-position at end of step
  * Z3 (vector<double>) - z-position at end of step
  * T3 (vector<double>) - time at end of step
  * PX1 (vector<double>) -  x-momentum at beginning of step
  * PY1 (vector<double>) -  y-momentum at beginning of step
  * PZ1 (vector<double>) -  z-momentum at beginning of step
  * KE1 (vector<double>) -  kinetic energy at beginning of step
  * PX3 (vector<double>) -  x-momentum at end of step
  * PY3 (vector<double>) -  y-momentum at end of step
  * PZ3 (vector<double>) -  z-momentum at end of step
  * KE3 (vector<double>) -  kinetic energy at end of step


# Papers 
  * "Characterization of XIA UltraLo-1800 Response to Measuring Charged Samples"  [arXiv](https://arxiv.org/abs/2209.08002) or [JINST](https://iopscience.iop.org/article/10.1088/1748-0221/18/01/P01027)


This product includes software developed by Members of the Geant4 Collaboration ( http://cern.ch/geant4 ). It is subject to the Geant4 license, https://geant4.web.cern.ch/geant4/license/LICENSE.html
