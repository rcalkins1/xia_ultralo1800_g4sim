//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: XIAEventAction.cc 75117 2013-10-28 09:38:37Z gcosmo $
//
/// \file XIAEventAction.cc
/// \brief Implementation of the XIAEventAction class

#include "XIAEventAction.hh"
#include "XIARun.hh"
#include "GarfieldPhysics.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"

#include "TView3D.h"

#include "RK4Pulse.h"
#include "FFTW_pulsefitter.h"
#include "Strawman_pulsefitter.h"

#include "TStyle.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

    XIAEventAction::XIAEventAction()
: G4UserEventAction(),
    fEdepVetoTop(0.),
    fEdepVetoBottom(0.)
{
    fWriteEmptyEvents = false;
    fMessenger = new G4GenericMessenger(this , "/XIAEventAction/", "XIA Event Action Physics Control" );
    G4GenericMessenger::Command&  EmptyCmd            =  fMessenger->DeclareProperty("WriteEmptyEvents",                  fWriteEmptyEvents ,  "Boolen: Write empty events to disk if true (Default:false)"        );
    EmptyCmd            .SetParameterName("WriteEmptyEvents",              true , false );
    EmptyCmd            .SetDefaultValue( "false" );


    fReportEvents = true;
    fReportEventsN = 100000;

    G4GenericMessenger::Command&  ReportEventsCmd            =  fMessenger->DeclareProperty("ReportEvents",    fReportEvents,  "Boolen: Report every N events  (Default:true, N=100000)"        );
    ReportEventsCmd            .SetParameterName("ReportEvents",  true , true );

    G4GenericMessenger::Command&  ReportEventsNCmd            =  fMessenger->DeclareProperty("ReportEventsN",    fReportEventsN,  "Boolen: Set N for reporting every N events  (Default:N=100000)"        );
    ReportEventsNCmd            .SetParameterName("ReportEventsN",  true , true );


    fDoNoise=true;
    G4GenericMessenger::Command&  DoNoiseCmd =  fMessenger->DeclareProperty("DoNoise", fDoNoise,  "Turn on noise (requires noise library)" );
    DoNoiseCmd.SetParameterName("DoNoise",  true ,  true );
    DoNoiseCmd.SetDefaultValue( "true" );

    fNoiseLib="/hpc/group/cdms2/xia_ultralo1800_g4sim/NoiseLibrary/noise.root";
    G4GenericMessenger::Command&  NoiseLibCmd            =  fMessenger->DeclareProperty("NoiseLib",  fNoiseLib,  "Location  of  noise library"        );
    NoiseLibCmd            .SetParameterName("NoiseLib",              true ,  true );
    NoiseLibCmd.SetDefaultValue( "/hpc/group/cdms2/xia_ultralo1800_g4sim/NoiseLibrary/noise.root" );


} 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

XIAEventAction::~XIAEventAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void XIAEventAction::BeginOfEventAction(const G4Event* event )
{    
    fEdepVetoTop = 0.;
    fEdepVetoBottom = 0.;
    // G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    // analysisManager->FillNtupleDColumn(12 , event->GetEventID()   );

    XIARun* run 
        = static_cast<XIARun*>(
                G4RunManager::GetRunManager()->GetNonConstCurrentRun());

    run->ClearVectors();

    GarfieldPhysics* garfieldPhysics = GarfieldPhysics::GetInstance();
    garfieldPhysics->Clear();

    G4int eventN =  G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID() ;
    if(!(eventN%fReportEventsN) && fReportEvents ) {
        G4cout << "Event #" << eventN <<"/"<<  G4RunManager::GetRunManager()->GetCurrentRun()->GetNumberOfEventToBeProcessed()   <<  G4endl;
    }


}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void XIAEventAction::EndOfEventAction(const G4Event* event)
{   
    // accumulate statistics in XIARun
    XIARun* run 
        = static_cast<XIARun*>(
                G4RunManager::GetRunManager()->GetNonConstCurrentRun());
    //    run->AddEdep(fEdep);

    GarfieldPhysics* garfieldPhysics = GarfieldPhysics::GetInstance();

    std::vector<G4double>*Q_time      =  &(run->Get_Q_time());
    std::vector<G4double>*pulse_time  =  &(run->Get_pulse_time());


    std::vector<G4double>*anode_wafer_Q     =  &(run->Get_anode_wafer_Q());
    std::vector<G4double>*anode_wafer_pulse =  &(run->Get_anode_wafer_pulse());
    std::vector<G4double>*guard_wafer_Q     =  &(run->Get_guard_wafer_Q());
    std::vector<G4double>*guard_wafer_pulse =  &(run->Get_guard_wafer_pulse());

    std::vector<G4double>*anode_full_Q     =  &(run->Get_anode_full_Q());
    std::vector<G4double>*anode_full_pulse =  &(run->Get_anode_full_pulse());
    std::vector<G4double>*guard_full_Q     =  &(run->Get_guard_full_Q());
    std::vector<G4double>*guard_full_pulse =  &(run->Get_guard_full_pulse());


    int eventnum= G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID();
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    analysisManager->FillH1(0, 1); //Adding an entry for the new event 


    TH1D * h_anodewafer = (TH1D*)garfieldPhysics->GetAnodePulseWafer()->Clone();
    TH1D * h_anodefull  = (TH1D*)garfieldPhysics->GetAnodePulseFull()->Clone();
    TH1D * h_guardfull  = (TH1D*)garfieldPhysics->GetGuardPulseFull()->Clone();
    TH1D * h_guardwafer = (TH1D*)garfieldPhysics->GetGuardPulseWafer()->Clone();

    //      for debugging 
    //      h_anodewafer->Reset();
    //      h_anodefull->Reset();
    //      h_guardwafer->Reset();
    //      h_guardfull->Reset();

    //Calculate the alpha contribution
    std::vector<G4double> * alphas_X1= &(run->Get_X1() );
    std::vector<G4double> * alphas_Y1= &(run->Get_Y1() );
    std::vector<G4double> * alphas_Z1= &(run->Get_Z1() );
    std::vector<G4double> * alphas_T1= &(run->Get_T1() );

    std::vector<G4double> * alphas_X3= &(run->Get_X3() );
    std::vector<G4double> * alphas_Y3= &(run->Get_Y3() );
    std::vector<G4double> * alphas_Z3= &(run->Get_Z3() );
    std::vector<G4double> * alphas_T3= &(run->Get_T3() );


    G4double dt=0,t=0,x=0,y=0,z=0,vx=0,vy=0,vz=0;
    for(int i=0;i<alphas_X1->size();++i){ 
        dt=alphas_T3->at(i)-alphas_T1->at(i) ;
        if(dt < 1e-30 || dt==0 ) continue;
        t= (alphas_T3->at(i)+alphas_T1->at(i))/(2*1000) + 90 ;
        x= 0.5*(alphas_X1->at(i)+alphas_X3->at(i))/10  ;  // Geant4 works in mm while Garfield is in cm
        y= 0.5*(alphas_Y1->at(i)+alphas_Y3->at(i))/10  ;
        z= 0.5*(alphas_Z1->at(i)+alphas_Z3->at(i))/10  ;
        vx= ((alphas_X3->at(i)-alphas_X1->at(i))/10)/dt  ;
        vy= ((alphas_Y3->at(i)-alphas_Y1->at(i))/10)/dt  ;
        vz= ((alphas_Z3->at(i)-alphas_Z1->at(i))/10)/dt  ;

        //Let's ignore the alpha. The time cale is tiny
        //      h_anodewafer->Fill(  t  ,  2*1.60217662e-19  *garfieldPhysics->CalcQ_anode_wafer(  x  ,  y  ,  z -7.5  ,  vx  ,  vy  ,  vz  )  );
        //      h_anodefull->Fill(   t  ,  2*1.60217662e-19  *garfieldPhysics->CalcQ_anode_full(   x  ,  y  ,  z -7.5  ,  vx  ,  vy  ,  vz  )  );
        //      h_guardwafer->Fill(  t  ,  2*1.60217662e-19  *garfieldPhysics->CalcQ_guard_wafer(  x  ,  y  ,  z -7.5  ,  vx  ,  vy  ,  vz  )  );
        //      h_guardfull->Fill(   t  ,  2*1.60217662e-19  *garfieldPhysics->CalcQ_guard_full(   x  ,  y  ,  z -7.5  ,  vx  ,  vy  ,  vz  )  );
    }


    //Fill the primaries
    std::vector<G4double>  *  primary_Ptype  =  &(run->  Get_primary_Ptype()  );
    std::vector<G4double>  *  primary_X      =  &(run->  Get_primary_X()      );
    std::vector<G4double>  *  primary_Y      =  &(run->  Get_primary_Y()      );
    std::vector<G4double>  *  primary_Z      =  &(run->  Get_primary_Z()      );
    std::vector<G4double>  *  primary_PX     =  &(run->  Get_primary_PX()     );
    std::vector<G4double>  *  primary_PY     =  &(run->  Get_primary_PY()     );
    std::vector<G4double>  *  primary_PZ     =  &(run->  Get_primary_PZ()     );
    std::vector<G4double>  *  primary_KE     =  &(run->  Get_primary_KE()     );
    std::vector<G4double>  *  primary_T      =  &(run->  Get_primary_T()      );




    for(int i=0;i< event->GetNumberOfPrimaryVertex(); ++i){
        G4PrimaryVertex * vertex = event->GetPrimaryVertex(i);
        G4PrimaryParticle * primary = vertex->GetPrimary();
        while(primary){
            primary_X      ->push_back(  vertex->GetX0()/CLHEP::m    );
            primary_Y      ->push_back(  vertex->GetY0()/CLHEP::m    );
            primary_Z      ->push_back(  vertex->GetZ0()/CLHEP::m    );
            primary_T      ->push_back(  vertex->GetT0()/CLHEP::s   );
            primary_Ptype  ->push_back(  primary->GetPDGcode()  );
            primary_PX     ->push_back(  primary->GetPx()/CLHEP::keV   );
            primary_PY     ->push_back(  primary->GetPy()/CLHEP::keV   );
            primary_PZ     ->push_back(  primary->GetPz()/CLHEP::keV   );
            primary_KE     ->push_back(  primary->GetKineticEnergy()/CLHEP::keV   );

            G4PrimaryParticle * daughter = primary->GetDaughter();
            while(daughter){

                primary_X      ->push_back(  vertex->GetX0()/CLHEP::m    );
                primary_Y      ->push_back(  vertex->GetY0()/CLHEP::m    );
                primary_Z      ->push_back(  vertex->GetZ0()/CLHEP::m    );
                primary_T      ->push_back(  vertex->GetT0()/CLHEP::s   );
                primary_Ptype  ->push_back(  daughter->GetPDGcode()  );
                primary_PX     ->push_back(   daughter->GetPx()/CLHEP::keV   );
                primary_PY     ->push_back(   daughter->GetPy()/CLHEP::keV   );
                primary_PZ     ->push_back(  daughter->GetPz()/CLHEP::keV   );
                primary_KE     ->push_back(   daughter->GetKineticEnergy()/CLHEP::keV   );

                daughter = daughter->GetNext();
            }
            primary = primary->GetNext();


        }
    }

    /*
    // IMAGE MAKER
    TCanvas * C = new TCanvas();
    C->Clear();
    TView3D *view = (TView3D*) TView::CreateView(1);
    view->SetRange(-50,-50,-20,50,50,20);
    TPolyMarker3D * drift = (TPolyMarker3D*) garfieldPhysics->GetDrift();
    drift->Draw();
    view->ShowAxis();

    C->SaveAs("mydriftQ.png");
    C->SaveAs("mydriftQ.C");
    delete C ; */
    /*
    // GIF MAKER
    //Getting minimum and maximum points, there must be a more efficient way to do this
    G4cout << "Number of Electrons Surveyed: " << garfieldPhysics->GetElectronNums() << G4endl;
    
    std::vector<double> electronPoint = garfieldPhysics->GetElectronStepPoint(0);
    double minimumPoint = electronPoint[3];
    double maximumPoint = electronPoint[3];
    for (int i = 1 ; i < garfieldPhysics->GetElectronNums() ; i++) {
        electronPoint = garfieldPhysics->GetElectronStepPoint(i);
        if (electronPoint[3] < minimumPoint) { minimumPoint = electronPoint[3]; }
        if (electronPoint[3] > maximumPoint) { maximumPoint = electronPoint[3]; }
    }
    G4cout << "Minimum Time Signature: " << minimumPoint << G4endl;
    G4cout << "Maximum Time Signature: " << maximumPoint << G4endl;

    //Canvas settings and all that
    TCanvas * J = new TCanvas();
    J->Clear();

    int intervalofslice = 500;

    //Current time interval, separating sampled electrons into interval-long frames
    for (int i = (int) minimumPoint ; i < (int) maximumPoint ; i=i+intervalofslice ) {
        TView3D *newView = (TView3D*) TView::CreateView(1);
        newView->SetRange(-50,-50,-20,50,50,20);
        newView->ShowAxis();
        newView->Front();
        TPolyMarker3D * driftFrame = new TPolyMarker3D;

        for (int j = 0 ; j < garfieldPhysics->GetElectronNums() ; j++) {
            electronPoint = garfieldPhysics->GetElectronStepPoint(j);
            if ((electronPoint[3] >= i-(intervalofslice/2)) && (electronPoint[3] < i+(intervalofslice/2))) {
                driftFrame->SetNextPoint( electronPoint[0], electronPoint[1], electronPoint[2] ); 
            }
        }

        driftFrame->Draw();
        std::string fileIndexString = std::to_string(i);
        char* fileIndex = new char[fileIndexString.length() + 15];
        strcpy(fileIndex, "myDriftQ/timestep_");
        strcat(fileIndex, fileIndexString.c_str());
        strcat(fileIndex, ".png");
        J->SaveAs(fileIndex);

        delete [] fileIndex;
        delete driftFrame;
        J->Clear();
    } */
    /*
    // HISTOGRAM MAKER
    G4cout << "Number of Electrons Surveyed: " << garfieldPhysics->GetElectronMomNums() << G4endl;

    std::vector<double> electronMomPoint = garfieldPhysics->GetElectronMomPoint(0);
    double minimumRadial = (sqrt((electronMomPoint[0])*(electronMomPoint[0]) + (electronMomPoint[1])*(electronMomPoint[1])))/(electronMomPoint[3]);
    double maximumRadial = minimumRadial;
    double minimumVertical = (electronMomPoint[2])/(electronMomPoint[3]);
    double maximumVertical = minimumVertical;
    for (int j = 1 ; j < garfieldPhysics->GetElectronMomNums() ; j++) {
        electronMomPoint = garfieldPhysics->GetElectronMomPoint(j);
        if ((sqrt((electronMomPoint[0])*(electronMomPoint[0]) + (electronMomPoint[1])*(electronMomPoint[1])))/(electronMomPoint[3]) < minimumRadial) { minimumRadial = (sqrt((electronMomPoint[0])*(electronMomPoint[0]) + (electronMomPoint[1])*(electronMomPoint[1])))/(electronMomPoint[3]); }
        if ((sqrt((electronMomPoint[0])*(electronMomPoint[0]) + (electronMomPoint[1])*(electronMomPoint[1])))/(electronMomPoint[3]) > maximumRadial) { maximumRadial = (sqrt((electronMomPoint[0])*(electronMomPoint[0]) + (electronMomPoint[1])*(electronMomPoint[1])))/(electronMomPoint[3]); }
        if ((electronMomPoint[2])/(electronMomPoint[3]) < minimumVertical) { minimumVertical = (electronMomPoint[2])/(electronMomPoint[3]); }
        if ((electronMomPoint[2])/(electronMomPoint[3]) > maximumVertical) { maximumVertical = (electronMomPoint[2])/(electronMomPoint[3]); }    
    }
    G4cout << "Minimum Radial Velocity: " << minimumRadial << G4endl;
    G4cout << "Maximum Radial Velocity: " << maximumRadial << G4endl;
    G4cout << "Minimum Verti. Velocity: " << minimumVertical << G4endl;
    G4cout << "Maximum Verti. Velocity: " << maximumVertical << G4endl;

    TCanvas * K = new TCanvas();

    K->Clear();
    TH1* h1 = new TH1I("h1", "radial",100000,minimumRadial,maximumRadial);
    for (int j = 0 ; j < garfieldPhysics->GetElectronMomNums() ; j++) {
        electronMomPoint = garfieldPhysics->GetElectronMomPoint(j);
        h1->Fill((sqrt((electronMomPoint[0])*(electronMomPoint[0]) + (electronMomPoint[1])*(electronMomPoint[1])))/(electronMomPoint[3]));
    }
    h1->Draw();
    K->SaveAs("myDriftQ/radialvelocity_total.png");
    K->SaveAs("myDriftQ/radialvelocity_total.C");
    K->Clear();

    TH1* h2 = new TH1I("h2", "vertical",100000,minimumVertical,maximumVertical);
    for (int j = 0 ; j < garfieldPhysics->GetElectronMomNums() ; j++) {
        electronMomPoint = garfieldPhysics->GetElectronMomPoint(j);
        h2->Fill((electronMomPoint[2])/(electronMomPoint[3]));
    }
    h2->Draw();
    K->SaveAs("myDriftQ/verticalvelocity_total.png");
    K->SaveAs("myDriftQ/verticalvelocity_total.C");
    K->Clear();

    TStyle* a_gStyle;
    a_gStyle = new TStyle();
    a_gStyle->SetOptStat(111111111);
    TH1* h3 = new TH1I("h3", "radial",100,(h1->GetMean() -(2*(h1->GetStdDev()))),(h1->GetMean() +(2*(h1->GetStdDev()))));
    for (int j = 0 ; j < garfieldPhysics->GetElectronMomNums() ; j++) {
        electronMomPoint = garfieldPhysics->GetElectronMomPoint(j);
        h3->Fill((sqrt((electronMomPoint[0])*(electronMomPoint[0]) + (electronMomPoint[1])*(electronMomPoint[1])))/(electronMomPoint[3]));
    }
    h3->Draw();
    G4cout << "Radial Underflow: " << h3->GetBinContent(0) << "\n";
    G4cout << "Radial Overflow : " << h3->GetBinContent(101) << "\n";
    K->SaveAs("myDriftQ/radialvelocity_centered.png");
    K->Clear();

    TStyle* b_gStyle;
    b_gStyle = new TStyle();
    b_gStyle->SetOptStat(111111111);
    TH1* h4 = new TH1I("h4", "vertical",100,(h2->GetMean() -(2*(h2->GetStdDev()))),(h2->GetMean() +(2*(h2->GetStdDev()))));
    for (int j = 0 ; j < garfieldPhysics->GetElectronMomNums() ; j++) {
        electronMomPoint = garfieldPhysics->GetElectronMomPoint(j);
        h4->Fill((electronMomPoint[2])/(electronMomPoint[3]));
    }
    h4->Draw();
    G4cout << "Verti. Underflow: " << h4->GetBinContent(0) << "\n";
    G4cout << "Verti. Overflow : " << h4->GetBinContent(101) << "\n";
    K->SaveAs("myDriftQ/verticalvelocity_centered.png");
    K->Clear();

    delete h1;
    delete h2;
    delete h3;
    delete h4;
    garfieldPhysics->ClearElectronMomPoints();
    */
    if(fWriteEmptyEvents || h_anodewafer->GetEntries()> 0 || run->GetEdepVetoTop() >0 || run->GetEdepVetoBottom() > 0 || alphas_X1->size()>0   ){

        //Strawman pulse sim
        //TH1D * Strawman_pulsefitter(TH1D* h_in, bool donoise=true, TString noiselib = "/scratch/group/cdms/Software/XIA_sim/NoiseLibrary/noise.root", int noise_index=0){
        TString TSnoiselib = TString( fNoiseLib );  
        TH1D  *  h_anode_wafer_calib     =  (TH1D*)  Strawman_pulsefitter(  h_anodewafer  , fDoNoise, TSnoiselib, eventnum   );
        TH1D  *  h_anode_full_calib      =  (TH1D*)  Strawman_pulsefitter(  h_anodefull   , fDoNoise, TSnoiselib, eventnum  );
        TH1D  *  h_guard_wafer_calib     =  (TH1D*)  Strawman_pulsefitter(  h_guardwafer  , fDoNoise, TSnoiselib, eventnum  );
        TH1D  *  h_guard_full_calib      =  (TH1D*)  Strawman_pulsefitter(  h_guardfull   , fDoNoise, TSnoiselib, eventnum  );



        //FFT based pulse sim
        //  TH1D  *  h_anodewafershaped  =  (TH1D*)  FFTW_pulsefitter(  h_anodewafer  );
        //  TH1D  *  h_anodefullshaped   =  (TH1D*)  FFTW_pulsefitter(  h_anodefull   );
        //  TH1D  *  h_guardfullshaped   =  (TH1D*)  FFTW_pulsefitter(  h_guardfull   );
        //  TH1D  *  h_guardwafershaped  =  (TH1D*)  FFTW_pulsefitter(  h_guardwafer  );

        //RK4 based electronics pulse sim
        //  TH1D * h_anodewafershaped = (TH1D*) RK4Pulse( h_anodewafer );
        //  TH1D * h_anodefullshaped   = (TH1D*) RK4Pulse( h_anodefull  );
        //  TH1D * h_guardfullshaped   = (TH1D*) RK4Pulse( h_guardfull  );
        //  TH1D * h_guardwafershaped  = (TH1D*) RK4Pulse( h_guardwafer );






        for(int i=1; i< h_anodewafer->GetNbinsX() +1; ++i){
            Q_time->push_back(  h_anodewafer->GetBinCenter(i) );
            anode_wafer_Q->push_back( h_anodewafer->GetBinContent(i) );
            anode_full_Q->push_back( h_anodefull->GetBinContent(i) );
            guard_wafer_Q->push_back( h_guardwafer->GetBinContent(i) );
            guard_full_Q->push_back( h_guardfull->GetBinContent(i) );
        }

        for(int i=1; i< h_anode_wafer_calib->GetNbinsX() +1; ++i){
            pulse_time->push_back(  h_anode_wafer_calib->GetBinCenter(i) );
            anode_wafer_pulse->push_back( h_anode_wafer_calib->GetBinContent(i) );
            anode_full_pulse->push_back( h_anode_full_calib->GetBinContent(i) );
            guard_wafer_pulse->push_back( h_guard_wafer_calib->GetBinContent(i) );
            guard_full_pulse->push_back( h_guard_full_calib->GetBinContent(i) );
        }

        analysisManager->FillNtupleDColumn( 0, eventnum);

        //Fill veto 
        //  analysisManager->FillNtupleDColumn( 31, run->GetEdepVetoTop() );
        //  analysisManager->FillNtupleDColumn( 32, run->GetEdepVetoBottom() );
        analysisManager->FillNtupleDColumn( 31, fEdepVetoTop    );
        analysisManager->FillNtupleDColumn( 32, fEdepVetoBottom );



        analysisManager->AddNtupleRow();

        delete     h_anode_wafer_calib ;                                  
        delete     h_anode_full_calib  ;                                   
        delete     h_guard_wafer_calib ;                                   
        delete     h_guard_full_calib  ;                                   


    }


    delete     h_anodewafer;
    delete     h_anodefull;
    delete     h_guardwafer;
    delete     h_guardfull;

    }

    //....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
