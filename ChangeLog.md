


# Change Log
All notable changes to this project will be documented in this file.
 
The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project tries to adheres to [Semantic Versioning](http://semver.org/).
 
## [Unreleased] - 2024-07-12
Update to bring in line with Geant4 10.7 and later versions of Garfieldpp that changed the way drift electrons are accessed. Ions haven't been updated because in practice, they aren't that useful. Be aware
 
### Added
- ChangeLog.md - a new change log to track changes ! You're reading it right now
 
### Changed
- GNUmakefile - Updates to linking 
- src/GarfieldPhysics.cc -  Adapting to new Garfield
- README.md - updates for papers


 
### Fixed
 
## [v01-03-00] - 2018-08-13 
   - Last stable tag
