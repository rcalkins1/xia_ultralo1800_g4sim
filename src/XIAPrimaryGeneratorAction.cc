//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: XIAPrimaryGeneratorAction.cc 69565 2013-05-08 12:35:31Z gcosmo $
//
/// \file XIAPrimaryGeneratorAction.cc
/// \brief Implementation of the XIAPrimaryGeneratorAction class

#include "XIAPrimaryGeneratorAction.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4VPhysicalVolume.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "G4VoxelLimits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

    XIAPrimaryGeneratorAction::XIAPrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
    fParticleGun(0), 
    fEnvelopeBox(0)
{


    fParticleGun = new G4ParticleGun();
    fGPS = new G4GeneralParticleSource();
    //fParticleGun  = new G4GeneralParticleSource();


    fMessenger = new G4GenericMessenger(this, "/source/", "XIA source" );



    fDoSurface  = false;
    G4GenericMessenger::Command&  DoSurfaceCmd =  fMessenger->DeclareProperty("DoSurface",  fDoSurface,  "Bool to do surface source: Default : false " );
    DoSurfaceCmd.SetParameterName("DoSurface",  true , true);


    fDoVolume   = false;
    G4GenericMessenger::Command&  DoVolumeCmd =  fMessenger->DeclareProperty("DoVolume",  fDoVolume,  "Bool to do volume source: Default : false " );
    DoVolumeCmd.SetParameterName("DoVolume",  true , true);

    fVolumeName = "";
    G4GenericMessenger::Command&  VolumeNameCmd =  fMessenger->DeclareProperty("VolumeName",  fVolumeName,  "Volume name : Default : NULL " );
    VolumeNameCmd.SetParameterName("VolumeName",  true , true);


    /*
       G4int n_particle = 1;
    // default particle kinematic
    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
    G4String particleName;
    G4ParticleDefinition* particle
    = particleTable->FindParticle(particleName="alpha");
    fParticleGun->SetParticleDefinition(particle);
    //fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
    //fParticleGun->SetParticleEnergy(5.3*MeV);

    G4int i = (int)(5.*G4UniformRand());
    gun->SetParticleDefinition(particle);
    G4double pp = momentum + (G4UniformRand() - 0.5)*sigmaMomentum;
    G4double mass = particle->GetPDGMass();
    G4double Ekin = sqrt(pp*pp + mass*mass) – mass;
    gun->SetParticleEnergy(Ekin);

    G4double angle = (G4UniformRand() - 0.5)*sigmaAngle;
    gun->SetParticleMomentumDirection(G4ThreeVector(sin(angle), 0., cos(angle));
    gun->GeneratePrimaryVertex(anEvent);
    */


}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

XIAPrimaryGeneratorAction::~XIAPrimaryGeneratorAction()
{
    delete fParticleGun;
    delete fGPS;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void XIAPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
    //this function is called at the begining of ecah event
    //

    // In order to avoid dependence of PrimaryGeneratorAction
    // on DetectorConstruction class we get Envelope volume
    // from G4LogicalVolumeStore.

    G4double envSizeXY = 0;
    G4double envSizeZ = 0;

    if (!fEnvelopeBox)
    {
        G4LogicalVolume* envLV
            = G4LogicalVolumeStore::GetInstance()->GetVolume("XIA_Ar");
        //= G4LogicalVolumeStore::GetInstance()->GetVolume("Envelope");
        if ( envLV ) fEnvelopeBox = dynamic_cast<G4Box*>(envLV->GetSolid());
    }

    if ( fEnvelopeBox ) {
        envSizeXY = fEnvelopeBox->GetXHalfLength()*2.;
        envSizeZ = fEnvelopeBox->GetZHalfLength()*2.;
    }  
    else  {
        G4ExceptionDescription msg;
        msg << "Envelope volume of box shape not found.\n"; 
        msg << "Perhaps you have changed geometry.\n";
        msg << "The gun will be place at the center.";
        G4Exception("XIAPrimaryGeneratorAction::GeneratePrimaries()",
                "MyCode0002",JustWarning,msg);
    }

    G4double size = 0.8; 
    G4double x0 = size * envSizeXY * (G4UniformRand()-0.5);
    G4double y0 = size * envSizeXY * (G4UniformRand()-0.5);
    G4double z0 = -0.5 * envSizeZ;

    G4ThreeVector position =  G4ThreeVector(0,0,0) ;
    //G4ThreeVector position =  G4ThreeVector(x0,y0,z0) ;


    if( fDoVolume || fDoSurface ){ 
        G4VPhysicalVolume* sourcePV = 0;
        sourcePV  = G4PhysicalVolumeStore::GetInstance()->GetVolume( fVolumeName);
        G4LogicalVolume* sourceLV = 0;
        //sourceLV  = G4LogicalVolumeStore::GetInstance()->GetVolume( fVolumeName);
        sourceLV  = sourcePV ->GetLogicalVolume(); 

        /*
         *  Maybe someday we'll have a solid transformation to global coordinates but 
         *  Geant4 isn't going to make it easy....
         G4LogicalVolume* motherLV =  sourceLV-> GetMotherLogical();

        //myLogicalVol->GetPhysicalVolume()->GetTransfomration()
        //myLogicalVol->GetMother()->GetPhysicalVolume()->GetTransformation()

        G4ThreeVector org =   G4ThreeVector( 0, 0 , 0 );
        while(  motherLV->GetName() != "World"){
        org +=    motherLV->GetTranslation()    ;
        motherLV = motherLV->GetMother(); 
        }
        */



        if( sourceLV != 0){ 
            G4VSolid * sourceSolid =  0;
            sourceSolid = sourceLV->GetSolid();

        if( sourceSolid != 0){ 


            G4double minX,maxX,minY,maxY,minZ,maxZ;
            G4VoxelLimits limit;                // Unlimited
            G4AffineTransform origin;
            sourceSolid->CalculateExtent(kXAxis,limit,origin,minX,maxX);
            sourceSolid->CalculateExtent(kYAxis,limit,origin,minY,maxY);
            sourceSolid->CalculateExtent(kZAxis,limit,origin,minZ,maxZ);

            if(fDoSurface) position = sourceSolid ->GetPointOnSurface();
            if(fDoVolume){
                while( kOutside == sourceSolid->Inside(position) ){

                    x0 = minX+(maxX-minX)*G4UniformRand();
                    y0 = minY+(maxY-minY)*G4UniformRand();
                    z0 = minZ+(maxZ-minZ)*G4UniformRand();

                    position =  G4ThreeVector(x0,y0,z0) ;
                }
            }
            //position += sourcePV ->GetFrameTranslation()    ;
            //position +=  origin ;

            //If you are really subscribed to the tray Z=0, then this is correct
            position +=  G4ThreeVector(0,0,-minZ )  ;
        }else{
            G4ExceptionDescription msg;
            msg << "Volume not found, using GPS instead  .\n"; 
            G4Exception("XIAPrimaryGeneratorAction::GeneratePrimaries()",
                    "MyCode0003",JustWarning,msg);

            }
        }else{
            G4ExceptionDescription msg;
            msg << "Volume not found, using GPS instead  .\n"; 
            G4Exception("XIAPrimaryGeneratorAction::GeneratePrimaries()",
                    "MyCode0003",JustWarning,msg);
        }
 //       G4cout << "REC " << position << G4endl;
    fGPS->SetParticlePosition( position );
    fGPS->GetCurrentSource()->GetPosDist()->SetCentreCoords(position*mm); 
    }
/*
    //Now we are ready to generate stuff
    G4double mass = fGPS->  GetParticleDefinition()->GetPDGMass() ;
    fParticleGun ->   SetParticleDefinition         ( fGPS->   GetParticleDefinition() );
    //fParticleGun ->   SetParticleEnergy             ( fGPS->   GetParticleEnergy() );
   // fParticleGun ->   SetParticleMomentum           (  std::sqrt( std::pow( mass + fGPS->   GetParticleEnergy(), 2) -mass*mass) );       
  //  fParticleGun ->   SetParticleMomentumDirection  ( fGPS->   GetParticleMomentumDirection() );
 //   fParticleGun ->   SetParticleCharge             ( fGPS->   GetParticleCharge() );
    fParticleGun ->   SetParticlePolarization       ( fGPS->   GetParticlePolarization() );
    fParticleGun ->   SetNumberOfParticles          ( fGPS->   GetNumberOfParticles() );

    //G4cout <<  fGPS->   GetParticleDefinition() ->GetParticleType() <<G4endl;
    //G4cout <<  fGPS->   GetParticleEnergy() <<G4endl;


    fParticleGun->SetParticlePosition( position );
*/

    fGPS->GeneratePrimaryVertex(anEvent);
    //fParticleGun->GeneratePrimaryVertex(anEvent);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

