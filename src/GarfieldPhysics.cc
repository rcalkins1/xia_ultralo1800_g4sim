#include "GarfieldPhysics.hh"

#include "TGeoManager.h"
//#include "TGDMLParse.h"
#include "TGeoBBox.h"

//#include "GarfieldAnalysis.hh"
#include "g4root.hh"
#include "G4SystemOfUnits.hh"

#include "G4ThreeVector.hh"
#include "G4Electron.hh"

#include "TRandom3.h"
#include <G4INCLRandom.hh>

#include <iostream>

GarfieldPhysics* GarfieldPhysics::fGarfieldPhysics = 0;
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....


GarfieldPhysics* GarfieldPhysics::GetInstance() {
    if (!fGarfieldPhysics) {
        fGarfieldPhysics = new GarfieldPhysics();
    }
    return fGarfieldPhysics;
}

void GarfieldPhysics::Dispose() {
    delete fGarfieldPhysics;
    fGarfieldPhysics = 0;
}

GarfieldPhysics::GarfieldPhysics() {
    fMapParticlesEnergy = new MapParticlesEnergy();
    fSecondaryElectrons = new std::vector<GarfieldElectron*>();
    fMediumMagboltz = 0;
    fSensor = 0;
    fAvalanche = 0;
    fDrift = 0;
    driftline_i  =0;
    fComponentAnalyticField = 0;
    fComponentConstant = 0;
    fComponentVoxel_Efield = 0;
    fComponentVoxel_Wfield_anode_full = 0;
    fComponentVoxel_Wfield_anode_wafer = 0;;
    fComponentVoxel_Wfield_guard_full = 0;
    fComponentVoxel_Wfield_guard_wafer = 0;
    fTrackHeed = 0;
    fGeoManager = 0;
    fGeometryRoot = 0;
    fGeometrySimple = 0;
    fBox = 0;
    createSecondariesInGeant4 = false;
    fTimeOffset= 0.0;
    fFirstInstance=true; 

    const int nbins = 1200 ;

    h_pulse_anode_wafer  =  new  TH1F("pulse_anode_wafer",  "pulse_anode_wafer;Time  [us];  I  [Arb  Units]",  nbins,  0,  300  );
    h_pulse_guard_wafer  =  new  TH1F("pulse_guard_wafer",  "pulse_guard_wafer;Time  [us];  I  [Arb  Units]",  nbins,  0,  300  );
    h_pulse_anode_full   =  new  TH1F("pulse_anode_full",   "pulse_anode_full;Time   [us];  I  [Arb  Units]",  nbins,  0,  300  );
    h_pulse_guard_full   =  new  TH1F("pulse_guard_full",   "pulse_guard_full;Time   [us];  I  [Arb  Units]",  nbins,  0,  300  );

    g_drift = new  TPolyMarker3D();
    std::vector<std::vector<double>> electronStepPoints;
    std::vector<double> electronStepPoint(4);

    //These are hard coded defaults
    Efield_file                = "FieldMeshes/Empty_Efield_1100V.txt" ;
    Wfield_anode_full_file     = "FieldMeshes/WfieldAnodeFull_cm1.txt" ;
    Wfield_anode_wafer_file    = "FieldMeshes/WfieldAnodeWafer_cm1.txt" ;
    Wfield_guard_full_file     = "FieldMeshes/WfieldGuardFull_cm1.txt" ;
    Wfield_guard_wafer_file    = "FieldMeshes/WfieldGuardWafer_cm1.txt" ;
    fEnableElectronDrift       =  true ;                     
    fEnableIonDrift            =  false ;                     


    nXmesh=100; nYmesh=100; nZmesh=100;



    fMessenger = new G4GenericMessenger(this , "/GarfieldPhysics/", "Garfield Physics Control" );


    G4GenericMessenger::Command&  EfieldCmd            =  fMessenger->DeclareProperty("Efield_file",                  Efield_file              ,  "Location  of  input  E-field  file"        );
    G4GenericMessenger::Command&  WfieldAnodeFullCmd   =  fMessenger->DeclareProperty("WfieldAnodeFull_file",       Wfield_anode_full_file   ,  "Location  of  input  W-field  anode-full   file"  );
    G4GenericMessenger::Command&  WfieldAnodeWaferCmd  =  fMessenger->DeclareProperty("WfieldAnodeWafer_file",      Wfield_anode_wafer_file  ,  "Location  of  input  W-field  anode-wafer  file"  );
    G4GenericMessenger::Command&  WfieldGuardFullCmd   =  fMessenger->DeclareProperty("WfieldGuardFull_file",       Wfield_guard_full_file   ,  "Location  of  input  W-field  guard-full   file"  );
    G4GenericMessenger::Command&  WfieldGuardWaferCmd  =  fMessenger->DeclareProperty("WfieldGuardWafer_file",      Wfield_guard_wafer_file  ,  "Location  of  input  W-field  guard-wafer  file"  );



    EfieldCmd            .SetParameterName("Efield_file",              true ,  true );
    WfieldAnodeFullCmd   .SetParameterName("Wfield_anode_full_file",   true ,  true );
    WfieldAnodeWaferCmd  .SetParameterName("Wfield_anode_wafer_file",  true ,  true );
    WfieldGuardFullCmd   .SetParameterName("Wfield_guard_full_file",   true ,  true );
    WfieldGuardWaferCmd  .SetParameterName("Wfield_guard_wafer_file",  true ,  true );


    //  EfieldCmd            .SetDefaultValue( (G4String)"FieldMeshes/constant.dat");
    //  WfieldAnodeFullCmd   .SetDefaultValue( (G4String)"FieldMeshes/constant.dat");
    //  WfieldAnodeWaferCmd  .SetDefaultValue( (G4String)"FieldMeshes/constant.dat");
    //  WfieldGuardFullCmd   .SetDefaultValue( (G4String)"FieldMeshes/constant.dat");
    //  WfieldGuardWaferCmd  .SetDefaultValue( (G4String)"FieldMeshes/constant.dat");



    //    G4cout <<" Here REC  E-Field = " << Efield_file << "  : " << fMessenger->GetCurrentValue(EfieldCmd.command ) << ": "<< test << G4endl;

    G4GenericMessenger::Command&  nXmeshCmd            =  fMessenger->DeclareProperty("nXmesh",                  nXmesh,  "Number of mesh nodes along X direction : Default = 10"        );
    G4GenericMessenger::Command&  nYmeshCmd            =  fMessenger->DeclareProperty("nYmesh",                  nYmesh,  "Number of mesh nodes along Y direction : Default = 10"        );
    G4GenericMessenger::Command&  nZmeshCmd            =  fMessenger->DeclareProperty("nZmesh",                  nZmesh,  "Number of mesh nodes along Z direction : Default = 10"        );

    nXmeshCmd.SetParameterName("nXmesh",   true , true );
    nYmeshCmd.SetParameterName("nYmesh",   true , true );
    nZmeshCmd.SetParameterName("nZmesh",   true , true );



    Gasfile = "GasData/ar-100_20psi.gas"; //For normal cases
    //Gasfile = "GasData/Ar_40C_HV.gas"; // For high field strengths
    G4GenericMessenger::Command&  GasfileCmd            =  fMessenger->DeclareProperty("Gasfile",     Gasfile,  "Specify location of gas properties file : Default = GasData/ar-100_20psi.gas"        );
    GasfileCmd.SetParameterName("Gasfile",   true , true );

    Ionfile = "GasData/IonMobility_Ar+_Ar.txt";
    G4GenericMessenger::Command&  IonfileCmd            =  fMessenger->DeclareProperty("Ionfile",     Ionfile,  "Specify location of ion mobility file : Default = GasData/IonMobility_Ar+_Ar.txt"        );
    IonfileCmd.SetParameterName("Ionfile",   true , true );



    fEnableElectronDrift = true;
    G4GenericMessenger::Command&  EnableElectronDriftCmd            =  fMessenger->DeclareProperty("EnableElectronDrift",                  fEnableElectronDrift,  "Turn on electron drift"        );
    EnableElectronDriftCmd            .SetParameterName("EnableElectronDrift",              true , true );
    EnableElectronDriftCmd            .SetDefaultValue( "true" );


    fEnableIonDrift = false ;
    G4GenericMessenger::Command&  EnableIonDriftCmd            =  fMessenger->DeclareProperty("EnableIonDrift",                  fEnableIonDrift,  "Turn on ion drift"        );
    EnableIonDriftCmd            .SetParameterName("EnableIonDrift",              true , true );
    EnableIonDriftCmd            .SetDefaultValue( "false" );






}

GarfieldPhysics::~GarfieldPhysics() {
    delete fMapParticlesEnergy;
    DeleteSecondaryElectrons();
    delete fSecondaryElectrons;
    delete fMediumMagboltz;
    delete fSensor;
    delete fAvalanche;
    delete fDrift;
    delete driftline_i ;
    delete fComponentAnalyticField;
    delete fComponentConstant;
    delete fTrackHeed;
    delete fGeometryRoot;
    delete fGeometrySimple;
    delete h_pulse_anode_full ;
    delete h_pulse_anode_wafer ;
    delete h_pulse_guard_full ;
    delete h_pulse_guard_wafer ;
    delete fComponentVoxel_Efield;
    delete fComponentVoxel_Wfield_anode_full;
    delete fComponentVoxel_Wfield_anode_wafer;;
    delete fComponentVoxel_Wfield_guard_full;
    delete fComponentVoxel_Wfield_guard_wafer;
    delete g_drift;
    delete fMessenger;   

    std::cout << "Deconstructor GarfieldPhysics" << std::endl;
}

void GarfieldPhysics::AddParticleName(const std::string particleName,
        G4double ekin_min_keV, G4double ekin_max_keV) {
    if (ekin_min_keV >= ekin_max_keV) {
        std::cout << "Ekin_min=" << ekin_min_keV
            << " keV is larger than Ekin_max=" << ekin_max_keV << " keV"
            << std::endl;
        return;
    }
    std::cout << "Garfield model is applicable for G4Particle " << particleName
        << " between " << ekin_min_keV << " keV and " << ekin_max_keV
        << " keV" << std::endl;
    fMapParticlesEnergy->insert(
            std::make_pair(particleName,
                std::make_pair(ekin_min_keV, ekin_max_keV)));

}

bool GarfieldPhysics::FindParticleName(std::string name) {
    MapParticlesEnergy::iterator it;
    it = fMapParticlesEnergy->find(name);
    if (it != fMapParticlesEnergy->end()) {
        return true;
    }
    return false;
}

bool GarfieldPhysics::FindParticleNameEnergy(std::string name,
        G4double ekin_keV) {
    MapParticlesEnergy::iterator it;
    it = fMapParticlesEnergy->find(name);
    if (it != fMapParticlesEnergy->end()) {
        EnergyRange_keV range = it->second;
        if (range.first <= ekin_keV && range.second >= ekin_keV) {
            return true;
        }
    }
    return false;
}

void GarfieldPhysics::InitializePhysics() {

    fMediumMagboltz = new Garfield::MediumMagboltz();

    fMediumMagboltz->SetComposition("ar", 100);
    fMediumMagboltz->SetTemperature(293.15);
    fMediumMagboltz->SetPressure(760.);
    fMediumMagboltz->SetPressure(1034.);
    fMediumMagboltz->EnableDebugging();
    fMediumMagboltz->Initialise(true);
    fMediumMagboltz->DisableDebugging();


    ////fMediumMagboltz->SetFieldGrid(0. , 1.e3, 30, true );
    ////fMediumMagboltz->GenerateGasTable( 10, true );
    ////fMediumMagboltz->WriteGasFile("ar_100.gas");

    //fMediumMagboltz->SetFieldGrid(100 , 100000, 20 , true); 
    fMediumMagboltz->SetFieldGrid(73.0, 74.0 , 20 , false ); 

    //Load gas file
    fMediumMagboltz->LoadGasFile(Gasfile);


    // Set the Penning transfer efficiency.
    const G4double rPenning = 0.57;
    const G4double lambdaPenning = 0.;
    fMediumMagboltz->EnablePenningTransfer(rPenning, lambdaPenning, "ar");

    // Load the ion mobilities.
    fMediumMagboltz->LoadIonMobility(Ionfile);



    fSensor = new Garfield::Sensor();
    fDrift = new Garfield::AvalancheMC();
    fAvalanche = new Garfield::AvalancheMicroscopic();
    fComponentAnalyticField = new Garfield::ComponentAnalyticField();
    fComponentConstant = new Garfield::ComponentConstant();
    driftline_i = new Garfield::DriftLineRKF();
    fTrackHeed = new Garfield::TrackHeed();

    fComponentVoxel_Efield =             new Garfield::ComponentVoxel();
    fComponentVoxel_Wfield_anode_full =  new Garfield::ComponentVoxel();
    fComponentVoxel_Wfield_anode_wafer = new Garfield::ComponentVoxel();
    fComponentVoxel_Wfield_guard_full =  new Garfield::ComponentVoxel();
    fComponentVoxel_Wfield_guard_wafer = new Garfield::ComponentVoxel();



    fComponentVoxel_Efield              ->  SetMesh(  nXmesh,  nYmesh,  nZmesh,  -30.1625,  30.1625,  -30.1625,  30.1625  ,  -7.5,  7.5  );
    fComponentVoxel_Wfield_anode_full   ->  SetMesh(  nXmesh,  nYmesh,  nZmesh,  -30.1625,  30.1625,  -30.1625,  30.1625  ,  -7.5,  7.5  );
    fComponentVoxel_Wfield_anode_wafer  ->  SetMesh(  nXmesh,  nYmesh,  nZmesh,  -30.1625,  30.1625,  -30.1625,  30.1625  ,  -7.5,  7.5  );
    fComponentVoxel_Wfield_guard_full   ->  SetMesh(  nXmesh,  nYmesh,  nZmesh,  -30.1625,  30.1625,  -30.1625,  30.1625  ,  -7.5,  7.5  );
    fComponentVoxel_Wfield_guard_wafer  ->  SetMesh(  nXmesh,  nYmesh,  nZmesh,  -30.1625,  30.1625,  -30.1625,  30.1625  ,  -7.5,  7.5  );


    /*
    //This is here in case you need to work in inches
    fComponentVoxel_Efield              ->  SetMesh( nXmesh, nYmesh, nZmesh,  0,  23.75,  0, 23.75 , 0,  5.9  );
    fComponentVoxel_Wfield_anode_full   ->  SetMesh( nXmesh, nYmesh, nZmesh,  0,  23.75,  0, 23.75 , 0,  5.9  );
    fComponentVoxel_Wfield_anode_wafer  ->  SetMesh( nXmesh, nYmesh, nZmesh,  0,  23.75,  0, 23.75 , 0,  5.9  );
    fComponentVoxel_Wfield_guard_full   ->  SetMesh( nXmesh, nYmesh, nZmesh,  0,  23.75,  0, 23.75 , 0,  5.9  );
    fComponentVoxel_Wfield_guard_wafer  ->  SetMesh( nXmesh, nYmesh, nZmesh,  0,  23.75,  0, 23.75 , 0,  5.9  );
    */




    CreateGeometry();

    fDrift->SetTimeSteps( 50.0 );
    //fDrift->SetTimeSteps( .01 );
    //fDrift->SetTimeSteps( .001 );
    fDrift->SetTimeWindow( -500, 350*1000 ); //in ns
    fDrift->SetSensor(fSensor);
    fDrift->EnableDriftLines(true);

    fAvalanche->SetSensor(fSensor);

    fTrackHeed->SetSensor(fSensor);
    fTrackHeed->EnableDeltaElectronTransport();

    driftline_i->SetSensor(fSensor);


    //  fAvalanche -> AvalancheMicroscopic::EnableSignalCalculation();
    //  fDrift->AvalancheMC::EnableSignalCalculation();

    myCanvas = new TCanvas();
    //    cellView = new Garfield::ViewCell();

    //  fComponentAnalyticField ->AddPlaneY( 15, 1000, "anode");
    //  fComponentAnalyticField ->AddPlaneY( 0, 0, "ground");

    //  cellView->SetComponent(fComponentAnalyticField);
    // cellView->SetArea(....);
    // cellView->SetCanvas(myCanvas);
    // cellView->Plot2d();
    // myCanvas->Update();
    // myCanvas->SaveAs("field_2d.png");

    //  vd = new Garfield::ViewDrift();
    //  vd->SetCanvas(myCanvas);
    //  // vd->SetArea(...);




}

void GarfieldPhysics::CreateGeometry() {


    fGeometrySimple = new Garfield::GeometrySimple();

    float boxXY= 23.75*2.54, boxZ = 15; //cm

    fBox = new Garfield::SolidBox(0., 0.,  0 , 0.5*boxXY, 0.5*boxXY , 0.5*boxZ  );


    // Add the solid to the geometry, together with the medium inside.
    fGeometrySimple->AddSolid(fBox, fMediumMagboltz);

    //  G4double thin = 1e-6;
    //  fBoxPlane = new Garfield::SolidBox(0., 0.,  0.5*boxZ+thin , 0.5*boxXY, 0.5*boxXY , thin ,"AnodeSignal" );
    //  fGeometrySimple->AddSolid(fBoxPlane, fMediumMagboltz);

    //    G4cout <<" ============== REC ================================\n";

    fGeometrySimple->PrintSolids() ;
    fComponentConstant->SetGeometry(fGeometrySimple);
    fComponentVoxel_Efield->SetGeometry(fGeometrySimple);
    fComponentVoxel_Wfield_anode_full->SetGeometry(fGeometrySimple);
    fComponentVoxel_Wfield_anode_wafer->SetGeometry(fGeometrySimple);
    fComponentVoxel_Wfield_guard_full->SetGeometry(fGeometrySimple);
    fComponentVoxel_Wfield_guard_wafer->SetGeometry(fGeometrySimple);
    fComponentVoxel_Efield->SetMedium(0, fMediumMagboltz );
    fComponentVoxel_Wfield_anode_full->SetMedium(0, fMediumMagboltz );
    fComponentVoxel_Wfield_anode_wafer->SetMedium(0, fMediumMagboltz );
    fComponentVoxel_Wfield_guard_full->SetMedium(0, fMediumMagboltz );
    fComponentVoxel_Wfield_guard_wafer->SetMedium(0, fMediumMagboltz );

    fComponentConstant ->SetElectricField(0,0, -1000.0/15.0 );
    fComponentConstant ->SetWeightingField(0,0, -1.0/15.0 , "anode" );

    //G4cout <<"REC: This is my efield file : " << GetEfield() <<G4endl;

    //This is where we'll set the fields 
    //if(    !fComponentVoxel_Efield              -> LoadData( Efield_file                .data()   , "XYZ", false, false ) ) G4cerr<<"WARNING: Failed to set field \n";
    //if(    !fComponentVoxel_Wfield_anode_full   -> LoadData( Wfield_anode_full_file     .data()   , "XYZ", false, false ) ) G4cerr<<"WARNING: Failed to set field \n";
    //if(    !fComponentVoxel_Wfield_anode_wafer  -> LoadData( Wfield_anode_wafer_file    .data()   , "XYZ", false, false ) ) G4cerr<<"WARNING: Failed to set field \n";
    //if(    !fComponentVoxel_Wfield_guard_full   -> LoadData( Wfield_guard_full_file     .data()   , "XYZ", false, false ) ) G4cerr<<"WARNING: Failed to set field \n";
    //if(    !fComponentVoxel_Wfield_guard_wafer  -> LoadData( Wfield_guard_wafer_file    .data()   , "XYZ", false, false ) ) G4cerr<<"WARNING: Failed to set field \n";

    if(    !fComponentVoxel_Efield              -> LoadElectricField( Efield_file                .data()   , "XYZ", false, false  , 1 , 1 , 1 ) ) G4cerr<<"WARNING: Failed to set field \n";
    if(    !fComponentVoxel_Wfield_anode_full   -> LoadElectricField( Wfield_anode_full_file     .data()   , "XYZ", false, false  , 1 , 1 , 1 ) ) G4cerr<<"WARNING: Failed to set field \n";
    if(    !fComponentVoxel_Wfield_anode_wafer  -> LoadElectricField( Wfield_anode_wafer_file    .data()   , "XYZ", false, false  , 1 , 1 , 1 ) ) G4cerr<<"WARNING: Failed to set field \n";
    if(    !fComponentVoxel_Wfield_guard_full   -> LoadElectricField( Wfield_guard_full_file     .data()   , "XYZ", false, false  , 1 , 1 , 1 ) ) G4cerr<<"WARNING: Failed to set field \n";
    if(    !fComponentVoxel_Wfield_guard_wafer  -> LoadElectricField( Wfield_guard_wafer_file    .data()   , "XYZ", false, false  , 1 , 1 , 1 ) ) G4cerr<<"WARNING: Failed to set field \n";


    fSensor->AddComponent(fComponentVoxel_Efield);
    //fSensor->AddComponent(fComponentConstant);


    fSensor->SetArea( );



    //Create a read-out of ionization - This seems to only work for analytical fields
    //    fComponentConstant ->AddReadout("AnodeSignal");
    //fSensor->AddElectrode(fComponentConstant, "AnodeSignal");
    //fSensor->SetTimeWindow( 0 , 300*1000  , 600 ); //Argument is in ms
    //fSensor->SetTimeWindow( 0 , 300*1000  , 1200 ); //Argument is in ms
                          //start, stepsize, nstes
    //fSensor->SetTimeWindow( 0 , 500  , 2*2400*1000 ); //Argument is in ms  // The number of steps is huge to capture all of the physics
    //fSensor->SetTimeWindow( 0 , 100 ,  100*300*1000 );

    const unsigned int nTimeBins = 3500;
    const double tmin = 0.;
    const double tmax = 350*1000  ;
    const double tstep = (tmax - tmin) / nTimeBins;
    fSensor->SetTimeWindow( tmin, tstep ,nTimeBins );




    //fDrift->EnableSignalCalculation();
    fAvalanche->EnableSignalCalculation();

  //double tstart=0, tstep=0; unsigned int nsteps=0;
  //fSensor->GetTimeWindow(tstart,  tstep,  nsteps);
  //G4cout << " Sensor window start: " <<  tstart <<"  tsteps: " <<   tstep << " nsteps: " <<   nsteps 
  //        <<" endtime : " << (tstart + tstep*nsteps)/1000 <<" us" << G4endl;


}

void GarfieldPhysics::DoIt(std::string particleName, G4double ekin_keV,
        G4double time, G4double x_cm, G4double y_cm, G4double z_cm, G4double dx,
        G4double dy, G4double dz,
        const G4FastTrack& fastTrack,
        G4FastStep& fastStep) {

    bool debugdrift = 0;
    //debugdrift = 1;
    float triggeroffset =  1.45;
    float oldtriggeroffset = 87*1e3 ; //in ns
    // This might not be correct since DoIt is called for tracks so multiple tracks will have this randomness to it
   // oldtriggeroffset += 1e3*G4INCL::Random::gauss(2 );
    
    //triggeroffset = 120 ; //testing something crazy

    DeleteSecondaryElectrons();

    if(!fEnableElectronDrift  && !fEnableIonDrift ) return;


    TRandom3 * rand = new TRandom3(); 


    G4double eKin_eV = ekin_keV * 1000;

    //    G4double xc = 0., yc = 0., zc = 0., tc = 0.;
    // Number of electrons produced in a collision
    int nc = 0;



    fastStep.KillPrimaryTrack();

    //fTrackHeed->SetParticle(particleName);
    //fTrackHeed->SetKineticEnergy(eKin_eV);
    //fTrackHeed->NewTrack(x_cm, y_cm, z_cm, time, dx, dy, dz);
    //fTrackHeed->TransportDeltaElectron(x_cm, y_cm, z_cm, time - fTimeOffset + triggeroffset, eKin_eV, dx, dy, dz, nc);
    fTrackHeed->TransportDeltaElectron(x_cm, y_cm, z_cm, time - fTimeOffset + oldtriggeroffset, eKin_eV, dx, dy, dz, nc);
    //std::cout << "Alpha Radius: " << sqrt((x_cm)*(x_cm) + (y_cm)*(y_cm) + (7.5+z_cm)*(7.5+z_cm)) << std::endl;
    //if (sqrt((x_cm)*(x_cm) + (y_cm)*(y_cm) + (7.5+z_cm)*(7.5+z_cm)) > 4) std::cout << "LOOK ABVOE IT MAY BE OUT OF BOUNDS" << std::endl;

    if(debugdrift) G4cout<< " cluster  (" << x_cm <<" , "<< y_cm <<" , "<< z_cm <<" )\n";

    G4double globaltime = fastTrack.GetPrimaryTrack()->GetGlobalTime() - fTimeOffset ;
    if(fFirstInstance){
        fFirstInstance = false;
        fTimeOffset  = fastTrack.GetPrimaryTrack()->GetGlobalTime() ;
        globaltime = 0;
    }
    globaltime/=1000;

    G4double xe, ye, ze, te;
    G4double ee, dxe, dye, dze;
    G4double xe1, ye1, ze1, te1;
    G4double xe2, ye2, ze2, te2;
    G4double xe3, ye3, ze3, te3;
    G4double xe4, ye4, ze4, te4;
    G4double ex, ey, ez;
    G4double dt =0;
    Garfield::Medium * medium = fMediumMagboltz ;
    //Garfield::Medium * medium =0;
    G4double induced ;
    int status;


//    globaltime += triggeroffset;

    for (int cl = 0; cl < nc; cl++) {
        fTrackHeed->GetElectron(cl, xe, ye, ze, te, ee, dxe, dye, dze);

//        te+= globaltime;

        if(debugdrift) G4cout<< " Electron Cl "<< cl <<" (" << xe <<" , "<< ye <<" , "<< ze <<" )\n";
        fComponentVoxel_Efield->ElectricField( xe, ye, ze , ex, ey, ez, medium , status );               
        if(debugdrift) G4cout <<"( "<< xe <<" , "<< ye <<" , "<< ze <<" )  E=( "<< ex <<" , "<<ey <<" , "<< ez <<" )\n";

        if(fEnableElectronDrift){
            fDrift->DriftElectron(xe, ye, ze, te);
            //fDrift->AvalancheElectron(xe, ye, ze, te);
            fDrift->GetElectronEndpoint(0, xe1, ye1, ze1, te1, xe2, ye2, ze2, te2, status);
            if(debugdrift) G4cout<< " Electron Cl Drift end "<< cl <<" (" << xe1 <<" , "<< ye1 <<" , "<< ze1 <<" )\n";


            const std::vector<Garfield::AvalancheMC::EndPoint> electrons = fDrift-> GetElectrons() ;


            //for(unsigned int i=0;i< fDrift->GetNumberOfDriftLinePoints(); i++  ){//Older garfieldpp function
            for(size_t el_n =0; el_n< electrons.size() ;++el_n){
            std::vector<Garfield::AvalancheMC::Point> path = electrons.at(el_n).path ;
            
            for(unsigned int i=0;i< path.size() ; i++  ){
                //fDrift->GetDriftLinePoint(i, xe4, ye4, ze4, te4 );  //For older garfield when this was done by hand
                  xe4 = path.at(i).x ; 
                  ye4 = path.at(i).y ; 
                  ze4 = path.at(i).z ; 
                  te4 = path.at(i).t ; 
                //if(debugdrift) G4cout<< " Electron Cl Drift point "<< i <<" (" << xe4 <<" , "<< ye4 <<" , "<< ze4 <<" , " << te4/1000    <<" )\n";

                //We are going to randomly sample with a down factor of 1000
                if(rand->Uniform(0, 10000 ) < 1 ) { 
                    g_drift->SetNextPoint( xe4, ye4, ze4 ); 
                }
              //if(((int)te4 % 500) == 0) {
              //    electronStepPoint = {xe4, ye4, ze4, te4};
              //    electronStepPoints.push_back( electronStepPoint );
              //}

                //Now we calculate the ionization signal based on the Ramo-Shockley theorem :
                if( int( i )  >0){ // set to 0 if using older garfield
                   // fDrift->GetDriftLinePoint(i-1, xe3, ye3, ze3, te3 ); //For older garfield when this was done by hand
                  xe3 = path.at(i-1).x ; 
                  ye3 = path.at(i-1).y ; 
                  ze3 = path.at(i-1).z ; 
                  te3 = path.at(i-1).t ; 
                    
                 //   G4cout<<"( " << xe3 <<" , "<< ye3<< " , "<< ze3<< " ) -> (" << xe4 <<" , "<< ye4<< " , "<< ze4<< " ) \n";
                    //if (i == 1) {
                   //   electronMomPoint = {xe4-xe3, ye4-ye3, ze4-ze3, te4-te3};
                   //   electronMomPoints.push_back( electronMomPoint );
                    //}

                    dt = te4 - te3 ;
                    if(dt ==0 ) continue;

                    //            continue;
                    //if(debugdrift) G4cout<<"( " << xe4 <<" , "<< ye4<< " , "<< ze4<< " ) -> (";

                    //We have to convert to COMSOL coordinates 
                    //This is a translation of (0,0,0) to the bottom corner and a conversion to inches
                    /*
                       xe3 = 0.5*23.75 + xe3/2.54;
                       ye3 = 0.5*23.75 + ye3/2.54;
                       ze3 = 0.5*5.9   + ze3/2.54;
                       xe4 = 0.5*23.75 + xe4/2.54;
                       ye4 = 0.5*23.75 + ye4/2.54;
                       ze4 = 0.5*5.9   + ze4/2.54;
                       dt*=2.54; // Hacky way to convert
                       */
                    if(debugdrift) G4cout<<"( " << xe4 <<" , "<< ye4<< " , "<< ze4<< "  , "<< te4<< " ) \n";

                    if(debugdrift) G4cout<<"dt = " << dt << G4endl;

                    //Obsolete 
                    //              fComponentConstant->WeightingField( xe4, ye4, ze4 , ex, ey, ez, "anode" );               
                    //              induced = 1.60217662e-19 ;// e
                    //              induced *= ex*(xe4-xe3) 
                    //                  +ey*(ye4-ye3) 
                    //                  +ez*(ze4-ze3) ;
                    //              induced/=dt;


                    //fComponentVoxel_Efield->ElectricField( xe4, ye4, ze4 , ex, ey, ez, medium , status );               
                    if(debugdrift) G4cout <<"( "<< xe3 <<" , "<< ye3 <<" , "<< ze3 <<"  , "<< te3 <<" )  E=( "<< ex <<" , "<<ey <<" , "<< ez <<" )\n";
                    if(debugdrift) G4cout <<"velocity =  "<< sqrt( (xe4-xe3)*(xe4-xe3) + (ye4-ye3)*(ye4-ye3)+(ze4-ze3)*(ze4-ze3))/dt* CLHEP::m/CLHEP::s << G4endl;
                    if(debugdrift) G4cout <<"Z velocity =  "<< (ze4-ze3)/dt* CLHEP::m/CLHEP::s << G4endl;

                    fComponentVoxel_Wfield_anode_wafer->ElectricField( xe4, ye4, ze4 , ex, ey, ez, medium , status );               
                    induced = 1.60217662e-19 ;// e
                    induced *= ex*(xe4-xe3) 
                        +ey*(ye4-ye3) 
                        +ez*(ze4-ze3) ;
                    induced/=dt;
                    h_pulse_anode_wafer->Fill( te4/1000 + globaltime + triggeroffset, -induced );
                    //if (induced <= 0 ) h_pulse_anode_wafer->Fill( te4/1000 + globaltime + triggeroffset, -induced );


                    fComponentVoxel_Wfield_anode_full ->ElectricField( xe4, ye4, ze4 , ex, ey, ez, medium , status );               
                    induced = 1.60217662e-19 ;// e
                    induced *= ex*(xe4-xe3) 
                        +ey*(ye4-ye3) 
                        +ez*(ze4-ze3) ;
                    induced/=dt;
                    h_pulse_anode_full->Fill( te4/1000 + globaltime + triggeroffset, -induced );
                    //if (induced <= 0 ) h_pulse_anode_full->Fill( te4/1000 + globaltime + triggeroffset, -induced );

                    fComponentVoxel_Wfield_guard_wafer->ElectricField( xe4, ye4, ze4 , ex, ey, ez, medium , status );               
                    induced = 1.60217662e-19 ;// e
                    induced *= ex*(xe4-xe3) 
                        +ey*(ye4-ye3) 
                        +ez*(ze4-ze3) ;
                    induced/=dt;
                    h_pulse_guard_wafer->Fill( te4/1000 + globaltime + triggeroffset, -induced );
                    //if (induced <= 0 ) h_pulse_guard_wafer->Fill( te4/1000 + globaltime + triggeroffset, -induced );

                    fComponentVoxel_Wfield_guard_full ->ElectricField( xe4, ye4, ze4 , ex, ey, ez, medium , status );               
                    induced = 1.60217662e-19 ;// e
                    induced *= ex*(xe4-xe3) 
                        +ey*(ye4-ye3) 
                        +ez*(ze4-ze3) ;
                    induced/=dt;
                    h_pulse_guard_full->Fill( te4/1000 + globaltime + triggeroffset, -induced );
                    //if (induced <= 0 ) h_pulse_guard_full->Fill( te4/1000 + globaltime + triggeroffset, -induced );

                    if(debugdrift) G4cout<< " Electron Cl Drift point "<< i <<" (" << xe3 <<" , "<< ye3 <<" , "<< ze3 <<" , "<< te3 <<  " )  E-field("<< ex <<" , "<< ey << " , " << ez << " )  q= " << induced << G4endl    ;


                }


            }
        }

        }


        //Drift the ions

        if(fEnableIonDrift){
            //    fTrackHeed->GetElectron(cl, xe, ye, ze, te, ee, dxe, dye, dze);
            //  fComponentVoxel_Efield->ElectricField( xe, ye, ze , ex, ey, ez, medium , status );               
            //  G4double vx=0,vy=0,vz=0;
            //  fMediumMagboltz->IonVelocity( ex, ey, ez, 0, 0, 0, vx, vy, vz ); 

            fDrift->DriftIon(xe, ye, ze, te);
            fDrift->GetIonEndpoint(0, xe1, ye1, ze1, te1, xe2, ye2, ze2, te2, status);
            if(debugdrift) G4cout<< " Ion  Cl Drift end "<< cl <<" (" << xe1 <<" , "<< ye1 <<" , "<< ze1 <<" )\n";



            //for(unsigned int i=0;i< fDrift->GetNumberOfDriftLinePoints(); i++  ){
            for(unsigned int i=0;i< fDrift->GetNumberOfIonEndpoints(); i++  ){
                //fDrift->GetDriftLinePoint(i, xe4, ye4, ze4, te4 ); //For older garfield
                fDrift->GetIonEndpoint( i, xe3, ye3, ze3, te3 , xe4, ye4, ze4, te4, status );
                if(te4/1000 > 400 ) break;
                if(debugdrift) G4cout<< " Ion Cl Drift point "<< i <<" (" << xe3 <<" , "<< ye3 <<" , "<< ze3 <<" , " << te4/1000    <<"  )\n";

                //We are going to randomly sample with a down factor of 1000

                if(rand->Uniform(0, 10000 ) < 1 ) g_drift->SetNextPoint( xe4, ye4, ze4 );

                //Now we calculate the ionization signal based on the Ramo-Shockley theorem :
                if(int(i)>-1){ //set to 0 for older garfield when doing by hand
                   // fDrift->GetDriftLinePoint(i-1, xe3, ye3, ze3, te3 ); //for older garfield
                    dt = te4 - te3 ;
                    if(dt < 1e-9) continue;

                    if(debugdrift) G4cout<<"( " << xe4 <<" , "<< ye4<< " , "<< ze4<< " ) -> (";


                    //fComponentVoxel_Efield->ElectricField( xe4, ye4, ze4 , ex, ey, ez, medium , status );               
                    if(debugdrift) G4cout <<"( "<< xe4 <<" , "<< ye4 <<" , "<< ze4 <<" )  E=( "<< ex <<" , "<<ey <<" , "<< ez <<" )\n";


                    fComponentVoxel_Wfield_anode_wafer->ElectricField( xe4, ye4, ze4 , ex, ey, ez, medium , status );               
                    induced = 1.60217662e-19 ;// e
                    induced *= ex*(xe4-xe3) 
                        +ey*(ye4-ye3) 
                        +ez*(ze4-ze3) ;
                    induced/=dt;
                    h_pulse_anode_wafer->Fill( te4/1000 + globaltime + triggeroffset, induced );

                    fComponentVoxel_Wfield_anode_full ->ElectricField( xe4, ye4, ze4 , ex, ey, ez, medium , status );               
                    induced = 1.60217662e-19 ;// e
                    induced *= ex*(xe4-xe3) 
                        +ey*(ye4-ye3) 
                        +ez*(ze4-ze3) ;
                    induced/=dt;
                    h_pulse_anode_full->Fill( te4/1000 + globaltime + triggeroffset, induced );

                    fComponentVoxel_Wfield_guard_wafer->ElectricField( xe4, ye4, ze4 , ex, ey, ez, medium , status );               
                    induced = 1.60217662e-19 ;// e
                    induced *= ex*(xe4-xe3) 
                        +ey*(ye4-ye3) 
                        +ez*(ze4-ze3) ;
                    induced/=dt;
                    h_pulse_guard_wafer->Fill( te4/1000 + globaltime + triggeroffset, induced );

                    fComponentVoxel_Wfield_guard_full ->ElectricField( xe4, ye4, ze4 , ex, ey, ez, medium , status );               
                    induced = 1.60217662e-19 ;// e
                    induced *= ex*(xe4-xe3) 
                        +ey*(ye4-ye3) 
                        +ez*(ze4-ze3) ;
                    induced/=dt;
                    h_pulse_guard_full->Fill( te4/1000 + globaltime + triggeroffset, induced );

                    if(debugdrift) G4cout<< " Electron Cl Drift point "<< i <<" (" << xe3 <<" , "<< ye3 <<" , "<< ze3 <<" , "<< te4 <<  " )  E-field("<< ex <<" , "<< ey << " , " << ez << " )  q= " << induced << G4endl    ;
                }


            }

        }







    }


    delete rand;

}

std::vector<GarfieldElectron*>* GarfieldPhysics::GetSecondaryElectrons() {
    return fSecondaryElectrons;
}

void GarfieldPhysics::DeleteSecondaryElectrons() {
    if (!fSecondaryElectrons->empty()) {
        fSecondaryElectrons->erase(fSecondaryElectrons->begin(),
                fSecondaryElectrons->end());
    }
}



void GarfieldPhysics::UpdateGarfield(){
    G4cout <<"GarfieldPhysics::UpdateGarfield : updating field configutation \n";

    //Load gas file
    fMediumMagboltz->LoadGasFile(Gasfile);


    // Set the Penning transfer efficiency.
    const G4double rPenning = 0.57;
    const G4double lambdaPenning = 0.;
    fMediumMagboltz->EnablePenningTransfer(rPenning, lambdaPenning, "ar");

    // Load the ion mobilities.
    fMediumMagboltz->LoadIonMobility(Ionfile);


    fComponentVoxel_Efield              ->  SetMesh(  nXmesh,  nYmesh,  nZmesh,  -30.1625,  30.1625,  -30.1625,  30.1625  ,  -7.5,  7.5  );
    fComponentVoxel_Wfield_anode_full   ->  SetMesh(  nXmesh,  nYmesh,  nZmesh,  -30.1625,  30.1625,  -30.1625,  30.1625  ,  -7.5,  7.5  );
    fComponentVoxel_Wfield_anode_wafer  ->  SetMesh(  nXmesh,  nYmesh,  nZmesh,  -30.1625,  30.1625,  -30.1625,  30.1625  ,  -7.5,  7.5  );
    fComponentVoxel_Wfield_guard_full   ->  SetMesh(  nXmesh,  nYmesh,  nZmesh,  -30.1625,  30.1625,  -30.1625,  30.1625  ,  -7.5,  7.5  );
    fComponentVoxel_Wfield_guard_wafer  ->  SetMesh(  nXmesh,  nYmesh,  nZmesh,  -30.1625,  30.1625,  -30.1625,  30.1625  ,  -7.5,  7.5  );


    //if(    !fComponentVoxel_Efield              -> LoadData( Efield_file                .data()   , "XYZ", false, false  ) ) G4cerr<<"WARNING: Failed to set field \n";
    //if(    !fComponentVoxel_Wfield_anode_full   -> LoadData( Wfield_anode_full_file     .data()   , "XYZ", false, false  ) ) G4cerr<<"WARNING: Failed to set field \n";
    //if(    !fComponentVoxel_Wfield_anode_wafer  -> LoadData( Wfield_anode_wafer_file    .data()   , "XYZ", false, false  ) ) G4cerr<<"WARNING: Failed to set field \n";
    //if(    !fComponentVoxel_Wfield_guard_full   -> LoadData( Wfield_guard_full_file     .data()   , "XYZ", false, false  ) ) G4cerr<<"WARNING: Failed to set field \n";
    //if(    !fComponentVoxel_Wfield_guard_wafer  -> LoadData( Wfield_guard_wafer_file    .data()   , "XYZ", false, false  ) ) G4cerr<<"WARNING: Failed to set field \n";

    if(    !fComponentVoxel_Efield              -> LoadElectricField( Efield_file                .data()   , "XYZ", false, false , 1 , 1 , 1 ) ) G4cerr<<"WARNING: Failed to set field \n";
    if(    !fComponentVoxel_Wfield_anode_full   -> LoadElectricField( Wfield_anode_full_file     .data()   , "XYZ", false, false , 1 , 1 , 1 ) ) G4cerr<<"WARNING: Failed to set field \n";
    if(    !fComponentVoxel_Wfield_anode_wafer  -> LoadElectricField( Wfield_anode_wafer_file    .data()   , "XYZ", false, false , 1 , 1 , 1 ) ) G4cerr<<"WARNING: Failed to set field \n";
    if(    !fComponentVoxel_Wfield_guard_full   -> LoadElectricField( Wfield_guard_full_file     .data()   , "XYZ", false, false , 1 , 1 , 1 ) ) G4cerr<<"WARNING: Failed to set field \n";
    if(    !fComponentVoxel_Wfield_guard_wafer  -> LoadElectricField( Wfield_guard_wafer_file    .data()   , "XYZ", false, false , 1 , 1 , 1 ) ) G4cerr<<"WARNING: Failed to set field \n";

    G4cout <<"GarfieldPhysics:Electron drift enabled = " << fEnableElectronDrift <<G4endl;
    G4cout <<"GarfieldPhysics:Ion drift enabled = " << fEnableIonDrift <<G4endl;


    //Dump out the e-field info
    fComponentVoxel_Efield ->PrintRegions() ;

}
