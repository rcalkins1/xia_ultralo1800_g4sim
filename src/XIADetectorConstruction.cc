//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: XIADetectorConstruction.cc 75117 2013-10-28 09:38:37Z gcosmo $
//
/// \file XIADetectorConstruction.cc
/// \brief Implementation of the XIADetectorConstruction class

#include "XIADetectorConstruction.hh"
#include "GarfieldG4FastSimulationModel.hh"

#include "G4RunManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4PVPlacement.hh"

#include "G4EqMagElectricField.hh"
#include "G4UniformElectricField.hh"
#include "G4MagIntegratorStepper.hh"

//#include "G4ElectricField.hh"
#include "G4FieldManager.hh"
//#include "G4MagInt_Driver.hh"
#include "G4MagIntegratorDriver.hh"
#include "G4ChordFinder.hh"
#include "G4ClassicalRK4.hh"
#include "G4TransportationManager.hh"

#include "TRandom3.h"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

    XIADetectorConstruction::XIADetectorConstruction()
: G4VUserDetectorConstruction(),
    fScoringVolume(0)
{

    fMessenger = new G4GenericMessenger(this, "/XIA/", "XIA Geometry Control" );

    fBuildFondren = false;
    G4GenericMessenger::Command&  BuildFondrenCmd =  fMessenger->DeclareProperty("BuildFondren",  fBuildFondren,  "Bool to build Fondren lab: Default : false " );
    BuildFondrenCmd.SetParameterName("BuildFondren",  true , true);

    fBuildVeto = false;
    G4GenericMessenger::Command&  BuildVetoCmd =  fMessenger->DeclareProperty("BuildVeto",  fBuildVeto,  "Bool to build veto : Default : false " );
    BuildVetoCmd.SetParameterName("BuildVeto",  true , true);

    fBuildTrayLiner = false;
    G4GenericMessenger::Command&  BuildTrayLinerCmd =  fMessenger->DeclareProperty("BuildTrayLiner",  fBuildTrayLiner ,  "Bool to build teflon tray liner: Default : false " );
    BuildTrayLinerCmd.SetParameterName("BuildTrayLiner",  true , true);

    G4GenericMessenger::Command& UpdateGeomCmd =  fMessenger->DeclareMethod("UpdateGeometry", &XIADetectorConstruction::UpdateGeometry  ,  "Update geometry if options are changed after initial construction " );


}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

XIADetectorConstruction::~XIADetectorConstruction()
{ }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void XIADetectorConstruction::BuildMaterials(){
    // Get nist material manager
    nist = G4NistManager::Instance();


    vacuum_mat = nist->FindOrBuildMaterial("G4_Galactic");
    air_mat = nist->FindOrBuildMaterial("G4_AIR");
    H2O_mat  = nist->FindOrBuildMaterial("G4_WATER");
    Ar_mat = nist->FindOrBuildMaterial("G4_Ar");
    Si_mat = nist->FindOrBuildMaterial("G4_Si");
    Al_mat = nist->FindOrBuildMaterial("G4_Al");
    Cu_mat = nist->FindOrBuildMaterial("G4_Cu");
    Pb_mat = nist->FindOrBuildMaterial("G4_Pb");
    scint_mat  = nist->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");
    concrete_mat = nist->FindOrBuildMaterial("G4_CONCRETE");
    plexy_mat = nist->FindOrBuildMaterial("G4_PLEXIGLASS");
    poly_mat = nist->FindOrBuildMaterial("G4_POLYETHYLENE");


    G4Element* elFe  = nist->FindOrBuildElement("Fe");
    G4Element* elC  = nist->FindOrBuildElement("C");
    G4Element* elMg  = nist->FindOrBuildElement("Mg");
    G4Element* elP  = nist->FindOrBuildElement("P");
    G4Element* elCr  = nist->FindOrBuildElement("Cr");
    G4Element* elNi  = nist->FindOrBuildElement("Ni");
    G4Element* elSi  = nist->FindOrBuildElement("Si");
    G4Element* elN  = nist->FindOrBuildElement("N");
    G4Element* elO  = nist->FindOrBuildElement("O");
    G4Element* elCa = nist->FindOrBuildElement("Ca");
    G4Element* elAl = nist->FindOrBuildElement("Al");
    G4Element* elTi = nist->FindOrBuildElement("Ti");
    G4Element* elS  = nist->FindOrBuildElement("S");

    norite_mat = new G4Material("Norite", 2.87*g/cm3, 14);
    norite_mat->AddElement(nist->FindOrBuildElement("H"), 0.0015);
    norite_mat->AddElement(nist->FindOrBuildElement("C"), 0.0004);
    norite_mat->AddElement(nist->FindOrBuildElement("O"), 0.4582);
    norite_mat->AddElement(nist->FindOrBuildElement("Na"),0.0222);
    norite_mat->AddElement(nist->FindOrBuildElement("Mg"),0.0328);
    norite_mat->AddElement(nist->FindOrBuildElement("Al"),0.0892);
    norite_mat->AddElement(nist->FindOrBuildElement("Si"),0.2610);
    norite_mat->AddElement(nist->FindOrBuildElement("P"), 0.0012);
    norite_mat->AddElement(nist->FindOrBuildElement("S"), 0.0020);
    norite_mat->AddElement(nist->FindOrBuildElement("K"), 0.0115);
    norite_mat->AddElement(nist->FindOrBuildElement("Ca"),0.0520);
    norite_mat->AddElement(nist->FindOrBuildElement("Mn"),0.0013);
    norite_mat->AddElement(nist->FindOrBuildElement("Fe"),0.0619);
    norite_mat->AddElement(nist->FindOrBuildElement("Ti"),0.0050);



    //Creating stainless steel 301
    //http://www.aksteel.com/pdf/markets_products/stainless/austenitic/AK%20301%20Stainless%20Steel%20PDB.pdf
    steel_mat = new G4Material("Steel_301", 7.88*g/cm3 , 9);
    steel_mat ->AddElement( elC , 0.15*perCent );
    steel_mat ->AddElement( elMg , 2.00*perCent );
    steel_mat ->AddElement( elP , 0.045*perCent );
    steel_mat ->AddElement( elS , 0.03*perCent );
    steel_mat ->AddElement( elSi , 0.75*perCent );
    steel_mat ->AddElement( elCr , 17*perCent );
    steel_mat ->AddElement( elNi , 7*perCent );
    steel_mat ->AddElement( elN , 0.1*perCent );
    steel_mat ->AddElement( elFe , 72.925*perCent );


    //    G4Material* Ar_H2O_20ppm =  new G4Material(name="Ar_H2O_20ppm", 0.00166201*0.99998 +  1*2e-5   , ncomponents = 2 );

    //In case you want to check the vacuum case
    //    Ar_mat = vacuum_mat;

    //This a guess at what is in DeWAL DW-105  http://www.dewal.com/dw-105/
    // this is ball park material, let's hope we aren't that sensitive to it
    G4Material * pure_teflon_mat = nist->FindOrBuildMaterial("G4_TEFLON");
    teflon_mat = new G4Material("ConductiveTeflon", .75*pure_teflon_mat->GetDensity() + .25*2.0*g/cm3     , 2);
    teflon_mat->AddMaterial(pure_teflon_mat, 75*perCent);
    teflon_mat->AddElement(elC , 25*perCent);


    //Create fiber glass using http://www.compositesworld.com/articles/the-making-of-glass-fiber, Column 2, why would there be boron? 
    //We'll skip the <1% elements

    G4double  density = 2.196*g/cm3; //  Quartz   59%
    G4double fractionmass; 
    G4int natoms, ncomponents;

    G4Material* SiO2 = new G4Material("SiO2", density, ncomponents = 2);
    SiO2->AddElement(elSi, natoms=1);
    SiO2->AddElement(elO , natoms= 2);


    density = 3.34*g/cm3; //  Quicklime     23%
    G4Material* CaO= new G4Material("CaO", density, ncomponents = 2);
    CaO->AddElement(elCa, natoms=1);
    CaO->AddElement(elO , natoms=1);

    density = 4.00*g/cm3; //         13%
    G4Material* Al2O3= new G4Material("Al2O3", density, ncomponents = 2);
    Al2O3->AddElement(elAl, natoms= 2);
    Al2O3->AddElement(elO , natoms=3);


    density = 3.58*g/cm3; //  4%
    G4Material* MgO= new G4Material("MgO", density, ncomponents = 2);
    MgO->AddElement(elMg, natoms=1);
    MgO->AddElement(elO , natoms=1);

    density = 4.95*g/cm3; //  4%
    G4Material* TiO= new G4Material("TiO", density, ncomponents = 2);
    TiO->AddElement(elTi, natoms=1);
    TiO->AddElement(elO , natoms=1);


    float Fiberglass_density = .59*2.196 + .23*3.34 + .13*4.00 + .04*3.58 +0.01*4.95 ;




    filter_mat = new G4Material("HEPAFilter",  0.5*Fiberglass_density + 0.5*air_mat->GetDensity()  , 6);
    filter_mat->AddMaterial(air_mat, fractionmass=50*perCent); // Let's assume it has about a 50% fill efficiency 
    //We'll take the 
    filter_mat->AddMaterial(SiO2, fractionmass=  59*.5*perCent);
    filter_mat->AddMaterial(CaO, fractionmass=   23*.5*perCent);
    filter_mat->AddMaterial(Al2O3, fractionmass= 13*.5*perCent);
    filter_mat->AddMaterial(MgO, fractionmass=   4*.5*perCent);
    filter_mat->AddMaterial(TiO, fractionmass=   1*.5*perCent);


    human_mat = new G4Material("SoylentGreen", 1.062*g/cm3, 11    );
    human_mat  ->AddElement(  nist->FindOrBuildElement("O")   ,  .65    )  ;
    human_mat  ->AddElement(  nist->FindOrBuildElement("C")   ,  .1850   )  ;
    human_mat  ->AddElement(  nist->FindOrBuildElement("H")   ,  .0950   )  ;
    human_mat  ->AddElement(  nist->FindOrBuildElement("N")   ,  .0320   )  ;
    human_mat  ->AddElement(  nist->FindOrBuildElement("Ca")  ,  .0150   )  ;
    human_mat  ->AddElement(  nist->FindOrBuildElement("P")   ,  .0100    )  ;
    human_mat  ->AddElement(  nist->FindOrBuildElement("K")   ,  .0040   )  ;
    human_mat  ->AddElement(  nist->FindOrBuildElement("S")   ,  .0030   )  ;
    human_mat  ->AddElement(  nist->FindOrBuildElement("Na")  ,  .0020   )  ;
    human_mat  ->AddElement(  nist->FindOrBuildElement("Cl")  ,  .0020   )  ;
    human_mat  ->AddElement(  nist->FindOrBuildElement("Mg")  ,  .0020   )  ; //This is fudged but should be small






}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* XIADetectorConstruction::Construct()
{  

    BuildMaterials();

    //Coloring for XIA
    G4VisAttributes* whitecolor = new G4VisAttributes(G4Colour::White() );

    invis = new G4VisAttributes(G4Colour(1.0,1.0,1.0,0.2));
    invis->SetForceWireframe(false);
    invis->SetVisibility(false);

    blue = new G4VisAttributes(G4Colour::Blue());
    blue-> SetForceLineSegmentsPerCircle(15);

    // Option to switch on/off checking of volumes overlaps
    //
    G4bool  checkOverlaps = false;

    //     
    // World
    //
    // We are going to create a world large enough to hold Fondren Science building for all cases

    //G4double world_sizeXY = 2*CLHEP::m;
    //G4double world_sizeZ  = 2*CLHEP::m;
    //if(fBuildFondren){
    G4double  world_sizeXY = 30*CLHEP::m;
    G4double  world_sizeZ  = 2*(fFondrenH+5*CLHEP::m);
    //  }


    G4Box* solidWorld =    
        new G4Box("World",                       //its name
                0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);     //its size

    logicWorld =                         
        new G4LogicalVolume(solidWorld,          //its solid
                air_mat,           //its material
                "World");            //its name

    physWorld = 
        new G4PVPlacement(0,                     //no rotation
                G4ThreeVector(),       //at (0,0,0)
                logicWorld,            //its logical volume
                "World",               //its name
                0,                     //its mother  volume
                false,                 //no boolean operation
                0,                     //copy number
                checkOverlaps);        //overlaps checking

    G4double XIA_sizeXY = env_sizeXY+2*Cu_thickness  ;
    G4double XIA_sizeZ  = env_sizeZ+2*Cu_thickness ;

    G4Box* XIA=  new G4Box("XIA", 0.5*XIA_sizeXY, 0.5*XIA_sizeXY, 0.5*XIA_sizeZ); //its size
    G4LogicalVolume * XIAEnv = new G4LogicalVolume(XIA,  Cu_mat,  "XIA_Cu");         //its name
    XIAEnv ->SetVisAttributes( whitecolor );

    //segment the XIA tray

    //Start with the guard
    G4Box* XIA_guard=  new G4Box("XIA_guard",   0.5*17.7*inches, 0.5*17.7*inches, 0.5*Cu_thickness ); 

    G4LogicalVolume* logicXIA_guard = new G4LogicalVolume(XIA_guard,  Cu_mat,  "logicXIA_Guard");   
    G4LogicalVolume* logicXIA_guard_steel = new G4LogicalVolume(XIA_guard,  steel_mat,  "logicXIA_Guard_steel");   

    //now the full tray
    G4Box* XIA_full=  new G4Box("XIA_full",  0.5*16.7*inches, 0.5*16.7*inches, 0.5*Cu_thickness ); 

    G4LogicalVolume* logicXIA_full =  new G4LogicalVolume(XIA_full,  Cu_mat,   "logicXIA_full"); 
    G4LogicalVolume* logicXIA_full_steel =  new G4LogicalVolume(XIA_full,  steel_mat,   "logicXIA_full_steel"); 



    //now the wafer
    G4Tubs * XIA_wafer= new G4Tubs ("XIA_wafer",                    //its name
            0,// inner radius
            5.9*inches, //outer radius
            0.5*Cu_thickness, //height
            0, //starting angle
            360.*deg ); // rotation angle

    G4LogicalVolume* logicXIA_wafer = new G4LogicalVolume(XIA_wafer,   Cu_mat,   "logicXIA_wafer");       
    G4LogicalVolume* logicXIA_wafer_steel = new G4LogicalVolume(XIA_wafer,   steel_mat,   "logicXIA_wafer_steel");       



    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(),         //at (0,0,0)
            logicXIA_wafer,                //its logical volume
            "XIA_wafer",                   //its name
            logicXIA_full,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(),         //at (0,0,0)
            logicXIA_full,                //its logical volume
            "XIA_full",                   //its name
            logicXIA_guard,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking


    //place the guards on the top and bottom
    G4ThreeVector guard_top(0, 0, 0.5*env_sizeZ+0.5*Cu_thickness);

    new G4PVPlacement(0,                       //no rotation
            guard_top, //G4ThreeVector(),         //at (0,0,0)
            logicXIA_guard,                //its logical volume
            "XIA_guard_top",                   //its name
            XIAEnv,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    //Bottom is steel
    G4Box* XIAtray=  new G4Box("XIA_Tray", 0.5*env_sizeXY, 0.5*env_sizeXY, 0.5*Cu_thickness); //its size
    G4LogicalVolume* LVXIAtray = new G4LogicalVolume(XIAtray,  steel_mat,  "XIA_tray");         //its name


    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(),         //at (0,0,0)
            logicXIA_wafer_steel,                //its logical volume
            "XIA_wafer_tray",                   //its name
            logicXIA_full_steel,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(),         //at (0,0,0)
            logicXIA_full_steel,                //its logical volume
            "XIA_full_tray",                   //its name
            logicXIA_guard_steel,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(),         //at (0,0,0)
            logicXIA_guard_steel,                //its logical volume
            "XIA_full_tray",                   //its name
            LVXIAtray,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking



    //place the guards on the top and bottom

    G4ThreeVector guard_bot(0, 0, -0.5*env_sizeZ-0.5*Cu_thickness);
    new G4PVPlacement(0,                       //no rotation
            guard_bot, //G4ThreeVector(),         //at (0,0,0)
            LVXIAtray,                //its logical volume
            "XIA_tray",                   //its name
            XIAEnv,              //its mother  volume
            false,                   //no boolean operation
            1,                       //copy number
            checkOverlaps);          //overlaps checking




    //  Add in the counting gas in the center
    G4Box* XIA_Ar =    
        new G4Box("XIA_Ar",                    //its name
                0.5*env_sizeXY, 0.5*env_sizeXY, 0.5*env_sizeZ); //its size





    G4ElectricField * fEMfield = new G4UniformElectricField( G4ThreeVector(0., 0.,  -(1./15.)*kilovolt/CLHEP::cm) ); 
    G4EqMagElectricField * fEquation = new G4EqMagElectricField( fEMfield ); 
    G4MagIntegratorStepper * fStepper = new G4ClassicalRK4( fEquation , 8 ); 
    G4FieldManager* fFieldManager = G4TransportationManager::GetTransportationManager()->GetFieldManager(); 
    fFieldManager -> SetDetectorField( fEMfield ); 
    G4MagInt_Driver * fIntgrDriver = new G4MagInt_Driver( 0.001*CLHEP::mm    /*fMinStep*/ , fStepper , fStepper -> GetNumberOfVariables () ); 
    G4ChordFinder * fChordFinder = new G4ChordFinder( fIntgrDriver ); 
    fFieldManager->SetChordFinder( fChordFinder );

    logicEnv =                         
        new G4LogicalVolume(XIA_Ar,            //its solid
                Ar_mat,             //its material
                "XIA_Ar",         //its name
                fFieldManager // field manager
                );
    logicEnv ->SetFieldManager( fFieldManager, true );

    /*
    //REC Adding in a 600nm later of Al and a 525 um silicon wafer
    G4Box* XIA_Si = new G4Box("XIA_Si",  10*CLHEP::cm , 10*CLHEP::cm , 0.5*525*CLHEP::um ); 
    G4LogicalVolume* LVXIA_Si=  new G4LogicalVolume(XIA_Si,  Si_mat,   "LVXIA_Si");      
    new G4PVPlacement(0,  G4ThreeVector(0, 0, -7.5*CLHEP::cm+0.5*525*CLHEP::um ),  LVXIA_Si,  "XIA_Si",  logicEnv,  false,  0,  checkOverlaps); 

    G4Box* XIA_Al = new G4Box("XIA_Al",  10*CLHEP::cm , 10*CLHEP::cm , 300*CLHEP::nm ); 
    G4LogicalVolume* LVXIA_Al=  new G4LogicalVolume(XIA_Al,  Al_mat,   "LVXIA_Al");      
    new G4PVPlacement(0,  G4ThreeVector(0, 0, -7.5*CLHEP::cm+300*CLHEP::nm+525*CLHEP::um ),  LVXIA_Al,  "XIA_Al",  logicEnv,  false,  0,  checkOverlaps); 

    //Add a 1cm cube of some material
    G4Box* XIA_Al = new G4Box("XIA_Al",  1*CLHEP::cm , 1*CLHEP::cm , 0.5*CLHEP::cm ); 
    G4LogicalVolume* LVXIA_Al=  new G4LogicalVolume(XIA_Al,  Pb_mat,   "LVXIA_Al");      
    new G4PVPlacement(0,  G4ThreeVector(0, 0, -7.5*CLHEP::cm+0.5*CLHEP::cm),  LVXIA_Al,  "XIA_Al",  logicEnv,  false,  0,  checkOverlaps); 

    //Adding in a small piece of rough Cu to be our contaminated source
    G4double scale = 1*CLHEP::um ;
    //scale *= 10;
    //scale *= 10;
    //  scale = 1*CLHEP::mm ;

    //G4LogicalVolume * roughCu = BuildRoughCu( scale );
    //new G4PVPlacement(0,  G4ThreeVector(0, 0, -7.5*CLHEP::cm+0.5*scale    ), roughCu   ,  "RoughCu",  logicEnv,  false,  0,  checkOverlaps); 
    G4LogicalVolume * roughCu = BuildRoughCuWave( scale );


    new G4PVPlacement(0,  G4ThreeVector(0, 0, -7.5*CLHEP::cm+0.5*scale    ), roughCu   ,  "RoughCu",  logicEnv,  false,  0,  checkOverlaps); 
    G4cout << "RoughCu volume = " << roughCu->GetSolid()->GetCubicVolume() * CLHEP::cm << " cm^3" <<G4endl;
    //G4cout << "RoughCu SurfaceArea = " << roughCu->GetSolid()->GetSurfaceArea() * CLHEP::cm << " cm^2" <<G4endl;

*/


    //Create the steel frame
    G4Box * XIAFrameBox = new G4Box("XIAFrameBox", 0.5*XIAsteellength ,  0.5*XIAsteellength , 0.5*XIAsteelheight );
    G4LogicalVolume* LVXIAFrameBox =  new G4LogicalVolume(XIAFrameBox ,  steel_mat,   "LVXIAFrameBox");      

    G4Box * XIAFrameBoxAr = new G4Box("XIAFrameBoxAr", 0.5*XIAsteellength-steel_thickness ,  0.5*XIAsteellength-steel_thickness , 0.5*XIAsteelheight-steel_thickness);
    G4LogicalVolume* LVXIAFrameBoxAr =  new G4LogicalVolume(XIAFrameBoxAr ,  Ar_mat,   "LVXIAFrameBoxAr");      
    new G4PVPlacement(0, G4ThreeVector(),  LVXIAFrameBoxAr, "FrameBoxAr",  LVXIAFrameBox,  false, 0,   checkOverlaps); 


    //Put the Ar gas in the XIA Al shell
    fScoringPVolume = new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(),         //at (0,0,0)
            logicEnv,                //its logical volume
            "XIA_Ar",                   //its name
            XIAEnv,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    fScoringVolume = logicEnv;





    //Put the XIA in the world so that the bottom of the tray is at Z=0
    //G4ThreeVector XIA_pos(0,0, 0.5*env_sizeZ );

    //Put the Ar in the fram so that the bottom of the tray is 8" above the bottom
    G4ThreeVector XIA_pos(0,0, 0.5*env_sizeZ -0.5*XIAsteelheight +8*inches         );
    new G4PVPlacement(0,                       //no rotation
            XIA_pos, //G4ThreeVector(),         //at (0,0,0)
            XIAEnv,                //its logical volume
            "XIA_Detector",                   //its name
            LVXIAFrameBoxAr,              //its mother  volume
            //logicWorld,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    //There is a slab of acrylic on top of the anodes to isolate the HV board from them
    //Let's add that stuff to the top of it. 

    G4Box * XIAAcrylicSlab = new G4Box("XIAAcrylicSlab", 0.5*env_sizeXY+steel_thickness+1*inches ,  0.5*env_sizeXY+steel_thickness+1*inches , 0.5*acrylicslab_thickness );
    G4LogicalVolume* LVXIAAcrylicSlab =  new G4LogicalVolume(XIAAcrylicSlab ,  plexy_mat,   "LVXIAAcrylicSlabAr");      
    new G4PVPlacement(0, G4ThreeVector(0,0, 0.5*acrylicslab_thickness  -0.5*XIAsteelheight +8*inches + env_sizeZ +steel_thickness ),  LVXIAAcrylicSlab, "AcrylicSlab",  LVXIAFrameBoxAr,  false, 0,   checkOverlaps); 






    //Put the steel frame in the world so that the bottom of the tray is at (0,0,0)
    G4ThreeVector frame_pos(0,0, 0.5*XIAsteelheight -8*inches         );
    new G4PVPlacement(0,                       //no rotation
            frame_pos, //G4ThreeVector(),         //at (0,0,0)
            LVXIAFrameBox,                //its logical volume
            "XIA_Frame",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking




    //Create XIA base

    G4Box* XIABaseBox=    
        new G4Box("XIABaseBox",                       //its name
                0.5*XIA_sizeXY, 0.5*XIA_sizeXY, 0.5*XIA_baseZ); //its size

    G4LogicalVolume* LVXIABaseBox =                         
        new G4LogicalVolume(XIABaseBox ,          //its solid
                Al_mat,           //its material
                "XIABaseBox");            //its name

    G4Box* XIABaseBoxAr=    
        new G4Box("XIABaseBoxAr",                       //its name
                0.5*XIA_sizeXY -Al_thickness, 0.5*XIA_sizeXY-Al_thickness, 0.5*XIA_baseZ-0.5*Al_thickness); //its size

    G4LogicalVolume* LVXIABaseBoxAr =                         
        new G4LogicalVolume(XIABaseBoxAr ,          //its solid
                Ar_mat,           //its material
                "XIABaseBoxAr");            //its name

    new G4PVPlacement(0,                     //no rotation
            G4ThreeVector(0, 0, 0.5*Al_thickness ),       //Move up so that there isn't a layer between the tray
            LVXIABaseBoxAr,            //its logical volume
            "XIABaseBoxAr",               //its name
            LVXIABaseBox,                     //its mother  volume
            false,                 //no boolean operation
            0,                     //copy number
            checkOverlaps);        //overlaps checking

    //new G4PVPlacement(0,                     //no rotXIA_baseZation
    //        G4ThreeVector(0, 0,  -Al_thickness - 0.5*XIA_baseZ ),       //
    //        LVXIABaseBox,            //its logical volume
    //        "XIABaseBox",               //its name
    //        logicWorld,                     //its mother  volume
    //        false,                 //no boolean operation
    //        0,                     //copy number
    //        checkOverlaps);        //overlaps checking






    //Create electronics box
    G4Box* ElecBox=    
        new G4Box("ElecBox",                       //its name
                0.5*ElecBoxX, 0.5*ElecBoxY, 0.5*ElecBoxZ );     //its size

    G4LogicalVolume* LVElecBox =                         
        new G4LogicalVolume(ElecBox ,          //its solid
                steel_mat,           //its material
                "ElecBox");            //its name

    G4Box* ElecBoxAir=    
        new G4Box("ElecBoxAir",                       //its name
                0.5*ElecBoxX - steel_thickness, 0.5*ElecBoxY- steel_thickness, 0.5*ElecBoxZ - steel_thickness);     //its size

    G4LogicalVolume* LVElecBoxAir =                         
        new G4LogicalVolume(ElecBoxAir ,          //its solid
                air_mat,           //its material
                "ElecBoxAir");            //its name

    G4Box* XIAmotherboard=    
        new G4Box("XIAmotherboard",                       //its name
                0.5*ElecBoxX - 3*steel_thickness, 0.5*ElecBoxY- 3*steel_thickness, 2*CLHEP::mm );     //its size

    G4LogicalVolume* LVXIAmotherboard =                         
        new G4LogicalVolume(XIAmotherboard ,          //its solid
                Si_mat,           //its material
                "XIAmotherboard");            //its name

    //Place the card about 1/3 the way up 
    new G4PVPlacement(0,                     //no rotation
            G4ThreeVector(0, 0, -(0.5*ElecBoxZ - steel_thickness)/3 ),       //
            LVXIAmotherboard,            //its logical volume
            "XIAmotherboard",               //its name
            LVElecBoxAir,                     //its mother  volume
            false,                 //no boolean operation
            0,                     //copy number
            checkOverlaps);        //overlaps checking


    new G4PVPlacement(0,                     //no rotation
            G4ThreeVector( ),       //at (0,0,0)
            LVElecBoxAir,            //its logical volume
            "ElecBoxAir",               //its name
            LVElecBox,                     //its mother  volume
            false,                 //no boolean operation
            0,                     //copy number
            checkOverlaps);        //overlaps checking



    new G4PVPlacement(0,                     //no rotation
            //G4ThreeVector( (env_sizeXY + steel_thickness -32*CLHEP::cm )-0.5*ElecBoxX   , 0 , env_sizeZ + steel_thickness + 0.5* ElecBoxZ  ),       //at (0,0,0)
            G4ThreeVector( (env_sizeXY + Al_thickness -32*CLHEP::cm )-0.5*ElecBoxX   , 0 ,  0.5* ElecBoxZ +XIAsteelheight-8*inches  ),       //at (0,0,0)
            LVElecBox,            //its logical volume
            "ElecBox",               //its name
            logicWorld,                     //its mother  volume
            false,                 //no boolean operation
            0,                     //copy number
            checkOverlaps);        //overlaps checking




    //Create a Garfield Region
    G4Region* regionGarfield = new G4Region("RegionGarfield");
    regionGarfield->AddRootLogicalVolume(fScoringVolume );
    //regionGarfield->AddRootLogicalVolume(LVXIA_Al);
    //    GarfieldModel* garfieldModel = new GarfieldModel("GarfieldModelIonisationChamber", regionGarfield);
    fGarfieldG4FastSimulationModel = new GarfieldG4FastSimulationModel(	"GarfieldG4FastSimulationModel", regionGarfield);



    //    if(fBuildFondren)BuildFondren( );
    UpdateGeometry();

    //
    //always return the physical World
    //
    return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void XIADetectorConstruction::BuildFondren( ) {
    G4bool  checkOverlaps = false;


    BuildCleanroom();
    BuildBuilding();

    G4double personheight = 6*feet;
    G4LogicalVolume * person = BuildPerson(personheight );


    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(  0,  -4*CLHEP::m , -(XIAoffset- 0.5*personheight)  ),         //
            person,                //its logical volume
            "Rob",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(  1*CLHEP::m,  -4*CLHEP::m , -(XIAoffset- 0.5*personheight)  ),         //
            person,                //its logical volume
            "Andrew",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            1,                       //copy number
            checkOverlaps);          //overlaps checking


    G4RotationMatrix * zRot = new G4RotationMatrix;
    zRot->rotateZ( 90.*deg );   


    new G4PVPlacement(zRot,                       //no rotation
            G4ThreeVector(  0.5*CLHEP::m,  -3*CLHEP::m , -(XIAoffset- 0.5*personheight)  ),         //
            person,                //its logical volume
            "Jodi",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            2,                       //copy number
            checkOverlaps);          //overlaps checking





    return; 
}



void XIADetectorConstruction::BuildVeto(){
    G4bool  checkOverlaps = false;

    //In case we want to build PMTs
    fTopVeto = BuildVetoPanel();
    fBottomVeto = BuildVetoPanel();

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(  6*inches , -XIACleanWalldist +0.5*.5*CLHEP::m +cleanroom_thickness  , 30*CLHEP::cm ),  // moved over in the lab
            fTopVeto,               //its logical volume
            "VetoPanel",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(  6*inches , -XIACleanWalldist +1.5*.5*CLHEP::m +cleanroom_thickness , 30*CLHEP::cm ),  // moved over in the lab
            fTopVeto,               //its logical volume
            "VetoPanel",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            1,                       //copy number
            checkOverlaps);          //overlaps checking


    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(  6*inches , -XIACleanWalldist +0.5*.5*CLHEP::m +cleanroom_thickness  , -30*CLHEP::cm ),  // moved over in the lab
            fBottomVeto,               //its logical volume
            "VetoPanel",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(  6*inches , -XIACleanWalldist +1.5*.5*CLHEP::m +cleanroom_thickness  , -30*CLHEP::cm ),  // moved over in the lab
            fBottomVeto,                //its logical volume
            "VetoPanel",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            1,                       //copy number
            checkOverlaps);          //overlaps checking




}


void XIADetectorConstruction::BuildTrayLiner(){
    G4bool  checkOverlaps = false;

    //Creating a carbon filled teflon for hte tray liner

    //I measured it to be 0.050-0.052 mm so lets go with 0.051 mm or 2 mil
    G4Box* TrayLinerBox =  new G4Box("TrayLinerBox",  0.5*env_sizeXY, 0.5*env_sizeXY, 0.5*.051*mm ); 

    G4LogicalVolume * logicTrayLiner =  new G4LogicalVolume( TrayLinerBox, teflon_mat, "LV_TrayLiner" );

    G4ThreeVector trayliner(0, 0, -0.5*env_sizeZ+0.5*.051*mm ) ;
    new G4PVPlacement(0,  trayliner ,  logicTrayLiner, "XIA_Teflon_Tray_Liner",  logicEnv, false,  0,  checkOverlaps);   
}




G4LogicalVolume * XIADetectorConstruction::BuildHEPAFilter(){

    // Option to switch on/off checking of volumes overlaps
    //
    G4bool  checkOverlaps = false;

    G4VisAttributes* beige = new G4VisAttributes(G4Colour( 245.0/255.0, 245.0/255.0, 220.0/255.0, 1 ));



    //We are going to base this off of measurements (rounded) and http://www.modularcleanrooms.com/assets/graphics/mac10-org5-RSRE-2.jpg

    // 2'x 4' x 9.5"
    // 2'x 16" x 3"
    // 13.5'x 6" x 3"
    G4double Al_wall = Al_thickness; // No reason to change but let's leave the option open

    G4Box* HEPAMotherBox=    
        new G4Box("HEPAMotherBox",                       //its name
                HEPALength*0.5, HEPAWidth*0.5,HEPAHeight*0.5 );     //its size

    G4LogicalVolume* LVHEPA=                         
        new G4LogicalVolume(HEPAMotherBox,          //its solid
                air_mat,           //its material
                "LVHEPAFilter");            //its name

    LVHEPA->SetVisAttributes(invis);


    G4Box* HEPABox=    
        new G4Box("HEPABox",                       //its name
                HEPALength*0.5, HEPAWidth*0.5,9.5*inches*0.5 );     //its size

    G4LogicalVolume* LVHEPABox=                         
        new G4LogicalVolume(HEPABox,          //its solid
                Al_mat,           //its material
                "LVHEPABox");            //its name

    //Main filter box details
    // 4" thick is a guess 

    G4Box* HEPAMainFilter=    
        new G4Box("HEPAMainFilter",                       //its name
                0.5*HEPALength-Al_wall, HEPAWidth*0.5-Al_wall,  4*inches*0.5);     //its size

    G4LogicalVolume* LVHEPAMainFilter=                         
        new G4LogicalVolume(HEPAMainFilter,          //its solid
                filter_mat,           //its material
                "LVHEPAMainFilter");            //its name
    LVHEPAMainFilter->SetVisAttributes( beige) ;

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(0,0, -9.5*inches*0.5 +0.5*4*inches  +Al_wall ),   
            LVHEPAMainFilter,                //its logical volume
            "HEPAMainFilter",                   //its name
            LVHEPABox,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    //Fan area
    // THere is a thick plate that I guess is ~1" thick in between the fan and the filter
    //I'm going to guess this space is about 4" deep

    G4Box* HEPAMainFanBox=    
        new G4Box("HEPAMainFanBox",                       //its name
                0.5*HEPALength-Al_wall, HEPAWidth*0.5-Al_wall,  4*inches*0.5);     //its size

    G4LogicalVolume* LVHEPAMainFanBox=                         
        new G4LogicalVolume(HEPAMainFanBox,          //its solid
                air_mat,           //its material
                "LVHEPAMainFanBox");            //its name

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(0,0, 9.5*inches*0.5 -0.5*4*inches - Al_wall   ),   
            LVHEPAMainFanBox,                //its logical volume
            "HEPAMainFanBox",                   //its name
            LVHEPABox,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking




    //Controller box
    G4Box* HEPAContBox=    
        new G4Box("HEPAContBox",                       //its name
                6*inches*0.5, 13.5*inches*0.5,3*inches*0.5 );     //its size

    G4LogicalVolume* LVHEPAContBox=                         
        new G4LogicalVolume(HEPAContBox,          //its solid
                Al_mat,           //its material
                "LVHEPAContBox");            //its name

    G4Box* HEPAContBoxAir=    
        new G4Box("HEPAContBoxAir",                       //its name
                6*inches*0.5-Al_wall, 13.5*inches*0.5-Al_wall,3*inches*0.5-Al_wall);     //its size

    G4LogicalVolume* LVHEPAContBoxAir=                         
        new G4LogicalVolume(HEPAContBoxAir,          //its solid
                air_mat,           //its material
                "LVHEPAContBoxAir");            //its name

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(0,0, 0),   
            LVHEPAContBoxAir,                //its logical volume
            "HEPAContBoxAir",                   //its name
            LVHEPAContBox,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking



    //Input filter box
    G4Box* HEPAInputBox=    
        new G4Box("HEPAInputBox",                       //its name
                16*inches*0.5, HEPAWidth*0.5 , 3*inches*0.5 );     //its size

    G4LogicalVolume* LVHEPAInputBox=                         
        new G4LogicalVolume(HEPAInputBox,          //its solid
                Al_mat,           //its material
                "LVHEPAInputBox");            //its name

    G4Box* HEPAInputBoxFilter=    
        new G4Box("HEPAInputBoxFilter",                       //its name
                16*inches*0.5-Al_wall, HEPAWidth*0.5-Al_wall,3*inches*0.5-Al_wall);     //its size

    G4LogicalVolume* LVHEPAInputBoxFilter=                         
        new G4LogicalVolume(HEPAInputBoxFilter,          //its solid
                filter_mat,           //its material
                "LVHEPAInputBoxFilter");            //its name
    LVHEPAInputBoxFilter->SetVisAttributes( beige) ;

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(0,0, 0),   
            LVHEPAInputBoxFilter,                //its logical volume
            "HEPAInputBoxFilter",                   //its name
            LVHEPAInputBox,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking



    //Put the components into the mother volume


    //main filter
    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(0,0, -3*inches*0.5),   
            LVHEPABox,                //its logical volume
            "HEPABox",                   //its name
            LVHEPA,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    //controller box
    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector( -(8*inches+ 0.5*6*inches) , 0.5*HEPAWidth-0.5*13.5*inches     , 0.5*HEPAHeight - 0.5*3*inches   ),   
            LVHEPAContBox,                //its logical volume
            "HEPAContBox",                   //its name
            LVHEPA,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    //input filter box
    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector( 0, 0, 0.5*HEPAHeight - 0.5*3*inches   ),   
            LVHEPAInputBox,                //its logical volume
            "HEPAInputBox",                   //its name
            LVHEPA,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking



    return LVHEPA;
}


G4LogicalVolume *  XIADetectorConstruction::BuildVetoPanel(){
    G4bool  checkOverlaps = false;


    G4Box* VetoMother=    
        new G4Box("VetoMother",                       //its name
                (panellength + 6*inches  )*0.5, panelwidth*0.5,panelheight*0.5 );     //its size

    G4LogicalVolume* LVVeto=                         
        new G4LogicalVolume(VetoMother,          //its solid
                air_mat,           //its material
                "LVVetoMother");            //its name

    LVVeto->SetVisAttributes(invis);

    G4Box* VetoPanel=    
        new G4Box("VetoPanel",                       //its name
                (panellength )*0.5, panelwidth*0.5,panelheight*0.5 );     //its size

    G4LogicalVolume* LVVetoPanel=                         
        new G4LogicalVolume(VetoPanel,          //its solid
                scint_mat,           //its material
                "LVVetoPanel");            //its name

    LVVetoPanel->SetVisAttributes(blue);

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector( -3*inches   , 0, 0 ),   
            LVVetoPanel,                //its logical volume
            "Scintillator",                   //its name
            LVVeto,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    //build the two PMTs per panel
    G4Tubs* PMT
        = new G4Tubs("PMT",
                0 , //Inner radius, we'll fill it with G4_GALACTIC
                0.5*panelheight,
                0.5*6*inches,
                0, 
                360.*deg);

    G4Tubs* PMTVac
        = new G4Tubs("PMTVac",
                0 , //Inner radius, we'll fill it with G4_GALACTIC
                0.5*panelheight- 0.2*CLHEP::cm,
                0.5*6*inches- 0.2*CLHEP::cm,
                0, 
                360.*deg);


    G4LogicalVolume* LVPMT=                         
        new G4LogicalVolume(PMT,          //its solid
                Al_mat,           //its material
                "LVPMT");            //its name

    G4LogicalVolume* LVPMTVac=                         
        new G4LogicalVolume(PMTVac,          //its solid
                vacuum_mat,           //its material
                "LVPMTVac");            //its name


    //  G4VisAttributes * AuxEdgeOn = new G4VisAttributes(G4Color::Blue() );
    //  AuxEdgeOn-> G4VisAttributes::SetForceAuxEdgeVisible(true);
    //  //    AuxEdgeOn-> G4VisAttributes::SetForceWireframe(true);
    //  AuxEdgeOn-> SetForceLineSegmentsPerCircle(15);
    //  LVPMT->SetVisAttributes( AuxEdgeOn );
    //  LVPMTVac->SetVisAttributes( AuxEdgeOn );
    LVPMT->SetVisAttributes( blue );
    LVPMTVac->SetVisAttributes( blue );


    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector( 0  , 0, 0 ),   
            LVPMTVac,                //its logical volume
            "PMTVac",                   //its name
            LVPMT,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    //Put the PMT in the motehr volume
    G4RotationMatrix * zRot = new G4RotationMatrix;
    zRot->rotateY( 90.*deg );   
    new G4PVPlacement( zRot,
            G4ThreeVector( 0.5*panellength      , 4*inches , 0 ),   
            LVPMT,                //its logical volume
            "PMT",                   //its name
            LVVeto,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(zRot,
            G4ThreeVector( 0.5*panellength       , -4*inches , 0 ),   
            LVPMT,                //its logical volume
            "PMT",                   //its name
            LVVeto,              //its mother  volume
            false,                   //no boolean operation
            1,                       //copy number
            checkOverlaps);          //overlaps checking


    return LVVeto;
}



void XIADetectorConstruction::BuildCleanroom( ){
    G4bool  checkOverlaps = false;

    //Parts of the cleanroom

    //Cleanroom walls will be clear
    G4VisAttributes* transparent = new G4VisAttributes(G4Colour(1.0,1.0,1.0,0.2));
    transparent->SetForceWireframe(true);



    G4Box* CleanLongWall =    
        new G4Box("CleanLongWall",                       //its name
                0.5*cleanroom_length , 0.5*cleanroom_thickness , 0.5*cleanroom_height );     //its size


    G4Box* CleanWestWall =    
        new G4Box("CleanWestWall",                       //its name
                0.5*cleanroom_thickness , 0.5*cleanroom_width  - 2*0.5*cleanroom_thickness, 0.5*cleanroom_height );     //its size


    G4Box* CleanCelling=    
        new G4Box("CleanCelling",                       //its name
                0.5*cleanroom_length, 0.5*cleanroom_width  ,   0.5*cleanroom_thickness );     //its size



    G4LogicalVolume* logic_CleanLongWall=                         
        new G4LogicalVolume(CleanLongWall,            //its solid
                plexy_mat,             //its material
                "LVCleanLongWall");         //its name

    G4LogicalVolume* logic_CleanWestWall=                         
        new G4LogicalVolume(CleanWestWall,            //its solid
                plexy_mat,             //its material
                "LVCleanWestWall");         //its name


    G4LogicalVolume* logic_CleanCelling=                         
        new G4LogicalVolume(CleanCelling,            //its solid
                plexy_mat,             //its material
                "LVCleanCelling");         //its name

    logic_CleanLongWall->SetVisAttributes(transparent);
    logic_CleanWestWall->SetVisAttributes(transparent);
    logic_CleanCelling->SetVisAttributes(transparent);


    //Place the cleanroom parts
    // The XIA tray center is (0,0,0), which is 46" off the ground

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector( -( 0.5*cleanroom_length - XIAEastWalldist - XIAXY  )  , -XIACleanWalldist , -(XIAoffset- 0.5*cleanroom_height)  ),         //
            logic_CleanLongWall,                //its logical volume
            "CleanSouthWall",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector( -( 0.5*cleanroom_length -  XIAEastWalldist - XIAXY   )  , -XIACleanWalldist + 8*12*inches, -(XIAoffset- 0.5*cleanroom_height)  ),         //
            logic_CleanLongWall,                //its logical volume
            "CleanNorthhWall",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            1,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector( -(cleanroom_length -  XIAEastWalldist - XIAXY   )  , -XIACleanWalldist + 0.5*cleanroom_width + 0.5*cleanroom_thickness, -(XIAoffset- 0.5*cleanroom_height ) ),         //
            logic_CleanWestWall,                //its logical volume
            "CleanWesthWall",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector( -(17*feet - XIAEastWalldist - XIAXY  )  , -XIACleanWalldist + 0.5*cleanroom_width  + 0.5*cleanroom_thickness, -(XIAoffset- 0.5*cleanroom_height ) ),         //
            logic_CleanWestWall,                //its logical volume
            "CleanAnteWall",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            1,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(  -( 0.5*cleanroom_length - XIAEastWalldist - XIAXY  )   , -XIACleanWalldist + 0.5*cleanroom_width ,   cleanroom_height -XIAoffset  + 0.5*cleanroom_thickness ),         //
            //G4ThreeVector(  -( 0.5*cleanroom_length - (64.0+23.75)*inches -.2*CLHEP::cm  )   , -XIACleanWalldist + 0.5*cleanroom_width ,   XIAoffset- 0.5*cleanroom_height + 0.5*cleanroom_height  + 0.5*cleanroom_thickness ),         //
            logic_CleanCelling,                //its logical volume
            "CleanCelling",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    //Build and place HEPA filters
    G4LogicalVolume * HEPAFilter = BuildHEPAFilter();
    for(int i=0;i<2;++i){ //These are the ones in the main cleanroom
        new G4PVPlacement(0,                       //no rotation
                G4ThreeVector( XIAEastWalldist + XIAXY -(2*i+1.5)*HEPALength , -XIACleanWalldist +0.5*HEPAWidth , cleanroom_height -XIAoffset  + cleanroom_thickness  +0.5*HEPAHeight ),         //
                HEPAFilter,                //its logical volume
                "HEPAFilter",                   //its name
                logicWorld,              //its mother  volume
                false,                   //no boolean operation
                4*i+0,                       //copy number
                checkOverlaps);          //overlaps checking

        new G4PVPlacement(0,                       //no rotation
                G4ThreeVector( XIAEastWalldist + XIAXY -(2*i+0.5)*HEPALength , -XIACleanWalldist +1.5*HEPAWidth , cleanroom_height -XIAoffset  + cleanroom_thickness  +0.5*HEPAHeight ),         //
                HEPAFilter,                //its logical volume
                "HEPAFilter",                   //its name
                logicWorld,              //its mother  volume
                false,                   //no boolean operation
                4*i+1,                       //copy number
                checkOverlaps);          //overlaps checking

        new G4PVPlacement(0,                       //no rotation
                G4ThreeVector( XIAEastWalldist + XIAXY -(2*i+1.5)*HEPALength , -XIACleanWalldist +2.5*HEPAWidth , cleanroom_height -XIAoffset  + cleanroom_thickness  +0.5*HEPAHeight ),         //
                HEPAFilter,                //its logical volume
                "HEPAFilter",                   //its name
                logicWorld,              //its mother  volume
                false,                   //no boolean operation
                4*i+2,                       //copy number
                checkOverlaps);          //overlaps checking

        new G4PVPlacement(0,                       //no rotation
                G4ThreeVector( XIAEastWalldist + XIAXY -(2*i+0.5)*HEPALength , -XIACleanWalldist +3.5*HEPAWidth , cleanroom_height -XIAoffset  + cleanroom_thickness  +0.5*HEPAHeight ),         //
                HEPAFilter,                //its logical volume
                "HEPAFilter",                   //its name
                logicWorld,              //its mother  volume
                false,                   //no boolean operation
                4*i+3,                       //copy number
                checkOverlaps);          //overlaps checking


    }

    //Only 1 in the Ante room
    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector( XIAEastWalldist + XIAXY -(5.5)*HEPALength , -XIACleanWalldist +1.5*HEPAWidth , cleanroom_height -XIAoffset  + cleanroom_thickness  +0.5*HEPAHeight ),         //
            HEPAFilter,                //its logical volume
            "HEPAFilter",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            9,                       //copy number
            checkOverlaps);          //overlaps checking




    return;
}

void XIADetectorConstruction::BuildBuilding(){
    G4bool  checkOverlaps = false;

    G4VisAttributes* beige = new G4VisAttributes(G4Colour( 245.0/255.0, 245.0/255.0, 220.0/255.0, 1 ));

    //Walls of Fondren 
    LongWallLength = 15*CLHEP::m + XIANorthWalldist;  //This should always be ok

    G4Box* LongWall =    
        new G4Box("LongWall",                       //its name
                0.5*WallThickness, 0.5*LongWallLength , 0.5*fFondrenH);     //its size

    G4Box* NorthWall =    
        new G4Box("NorthWall",                       //its name
                0.5*(LabWidth +3*WallThickness+HallWidth) ,   0.5*WallThickness,  0.5*fFondrenH);     //its size

    G4LogicalVolume* logic_LongWall=                         
        new G4LogicalVolume(LongWall,            //its solid
                concrete_mat,             //its material
                "LVLongWall");         //its name

    G4LogicalVolume* logic_NorthWall=                         
        new G4LogicalVolume(NorthWall,            //its solid
                concrete_mat,             //its material
                "LVNorthWall");         //its name
    logic_LongWall->SetVisAttributes( beige );
    logic_NorthWall->SetVisAttributes( beige );



    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector( XIAEastWalldist + XIAXY+ 0.5*WallThickness ,   -(0.5*LongWallLength - XIANorthWalldist)  , (0.5*fFondrenH -XIAoffset)   ),         //
            logic_LongWall,                //its logical volume
            "LongWallCenter",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector( XIAEastWalldist + XIAXY+ WallThickness+ HallWidth +  0.5*WallThickness ,   -(0.5*LongWallLength - XIANorthWalldist)  , (0.5*fFondrenH -XIAoffset)   ),         //
            logic_LongWall,                //its logical volume
            "LongWallHall",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            1,                       //copy number
            checkOverlaps);          //overlaps checking


    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector( -(LabWidth-XIAEastWalldist - XIAXY) - 0.5*WallThickness ,   -(0.5*LongWallLength - XIANorthWalldist)  , (0.5*fFondrenH -XIAoffset)   ),         //
            logic_LongWall,                //its logical volume
            "LongWallWest",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            2,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(   -(LabWidth -XIAEastWalldist - XIAXY-0.5*(LabWidth +3*WallThickness+HallWidth)+WallThickness ) ,   XIANorthWalldist+0.5*WallThickness  , (0.5*fFondrenH -XIAoffset)   ),         //
            logic_NorthWall,                //its logical volume
            "NorthWallNorth",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking


    //Floors of Fondren 
    G4Box* LabFloor=    
        new G4Box("LabFloor",                       //its name
                0.5*LabWidth, 0.5*LongWallLength , 0.5*FloorThickness);     //its size


    G4Box* HallFloor=    
        new G4Box("HallFloor",                       //its name
                0.5*HallWidth, 0.5*LongWallLength , 0.5*FloorThickness);     //its size


    G4LogicalVolume* logic_LabFloor=                         
        new G4LogicalVolume(LabFloor,            //its solid
                concrete_mat,             //its material
                "LVLabFloor");         //its name

    G4LogicalVolume* logic_HallFloor=                         
        new G4LogicalVolume(HallFloor,            //its solid
                concrete_mat,             //its material
                "LVHallFloor");         //its name

    logic_LabFloor->SetVisAttributes( beige );
    logic_HallFloor->SetVisAttributes( beige );

    //Ground level
    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(    -(0.5*LabWidth -XIAEastWalldist - XIAXY)         , -(0.5*LongWallLength - XIANorthWalldist) , -(XIAoffset + 0.5*FloorThickness) ),
            logic_LabFloor,                //its logical volume
            "LabFloor_0",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(  XIAEastWalldist + XIAXY+WallThickness + 0.5*HallWidth     , -(0.5*LongWallLength - XIANorthWalldist) , -(XIAoffset + 0.5*FloorThickness) ),
            logic_HallFloor,                //its logical volume
            "HallFloor_0",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    //First floor
    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(    -(0.5*LabWidth -XIAEastWalldist - XIAXY)         , -(0.5*LongWallLength - XIANorthWalldist) , -XIAoffset  + FloorHeight -0.5*FloorThickness  ),
            logic_LabFloor,                //its logical volume
            "LabFloor_1",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            1,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(  XIAEastWalldist + XIAXY+WallThickness + 0.5*HallWidth     , -(0.5*LongWallLength - XIANorthWalldist) , -XIAoffset + FloorHeight -0.5*FloorThickness ),
            logic_HallFloor,                //its logical volume
            "HallFloor_1",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            1,                       //copy number
            checkOverlaps);          //overlaps checking

    //Secondfloor
    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(    -(0.5*LabWidth -XIAEastWalldist - XIAXY)         , -(0.5*LongWallLength - XIANorthWalldist) , -XIAoffset  + 2*FloorHeight -0.5*FloorThickness  ),
            logic_LabFloor,                //its logical volume
            "LabFloor_2",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            2,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(  XIAEastWalldist + XIAXY+WallThickness + 0.5*HallWidth     , -(0.5*LongWallLength - XIANorthWalldist) , -XIAoffset + 2*FloorHeight -0.5*FloorThickness ),
            logic_HallFloor,                //its logical volume
            "HallFloor_2",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            2,                       //copy number
            checkOverlaps);          //overlaps checking

    //Third floor Celling
    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(    -(0.5*LabWidth -XIAEastWalldist - XIAXY)         , -(0.5*LongWallLength - XIANorthWalldist) , -XIAoffset  + 3*FloorHeight -0.5*FloorThickness  ),
            logic_LabFloor,                //its logical volume
            "LabFloor_3",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            3,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(  XIAEastWalldist + XIAXY+WallThickness + 0.5*HallWidth     , -(0.5*LongWallLength - XIANorthWalldist) , -XIAoffset + 3*FloorHeight -0.5*FloorThickness ),
            logic_HallFloor,                //its logical volume
            "HallFloor_3",                   //its name
            logicWorld,              //its mother  volume
            false,                   //no boolean operation
            3,                       //copy number
            checkOverlaps);          //overlaps checking

    return;
}





G4LogicalVolume * XIADetectorConstruction::BuildPerson(G4double height ){
    G4bool  checkOverlaps = false;

    G4double width = 25*CLHEP::cm;



    G4Box* Mother=  new G4Box("PersonMother", 0.5*width, 0.5*1*CLHEP::m  , 0.5*height);  
    G4LogicalVolume* LVMother = new G4LogicalVolume(Mother, air_mat, "LVMother");        
    LVMother->SetVisAttributes(invis);


    G4Orb * head = new G4Orb("Head", 0.5*width );
    G4LogicalVolume* LVHead= new G4LogicalVolume(head,  human_mat,   "LVHead");         


    new G4PVPlacement(0,                       //no rotation
            //G4ThreeVector(0,0, height - 0.5*width ),   
            G4ThreeVector(0,0, 0.5*(height - width) ),   
            LVHead,                //its logical volume
            "LVHead",                   //its name
            LVMother,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    G4VisAttributes * AuxEdgeOn = new G4VisAttributes(G4Color::Yellow() );
    AuxEdgeOn-> G4VisAttributes::SetForceAuxEdgeVisible(true);
    //    AuxEdgeOn-> G4VisAttributes::SetForceWireframe(true);
    AuxEdgeOn-> SetForceLineSegmentsPerCircle(15);
    LVHead->SetVisAttributes( AuxEdgeOn );

    G4double torsolength = (3.2/7.0)*height; //70*CLHEP::cm;
    G4double torsowidth  = height/5.0;

    G4Box* torso= new G4Box("Torso", 0.5*width, 0.5*torsowidth  , 0.5*torsolength );     //its size
    G4LogicalVolume* LVtorso= new G4LogicalVolume(torso,  human_mat,   "LVtorso");         
    LVtorso->SetVisAttributes( AuxEdgeOn );

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(0,0, 0.5*height - width-0.5*torsolength ),   
            LVtorso,                //its logical volume
            "LVtorso",                   //its name
            LVMother,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    G4Tubs * arm = new G4Tubs("arm", 0, 0.3*width, 0.5*torsolength, 0, 360*CLHEP::deg );
    G4LogicalVolume * LVarm  = new G4LogicalVolume(arm , human_mat, "LVarm" );
    LVarm ->SetVisAttributes( AuxEdgeOn );


    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(0,0.5*torsowidth+0.3*width, 0.5*height - width-0.5*torsolength ),   
            LVarm,                //its logical volume
            "LVarm",                   //its name
            LVMother,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(0,-(0.5*torsowidth+0.3*width), 0.5*height - width-0.5*torsolength ),   
            LVarm,                //its logical volume
            "LVarm",                   //its name
            LVMother,              //its mother  volume
            false,                   //no boolean operation
            1,                       //copy number
            checkOverlaps);          //overlaps checking

    G4double leglength = height - width - torsolength;
    G4Tubs * leg = new G4Tubs("leg", 0, 0.3*width, 0.5*leglength, 0, 360*CLHEP::deg );
    G4LogicalVolume * LVleg  = new G4LogicalVolume(leg , human_mat, "LVleg" );
    LVleg ->SetVisAttributes( AuxEdgeOn );


    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(0,0.5*torsowidth-0.3*width, -0.5*height + 0.5*leglength   ),   
            LVleg,                //its logical volume
            "LVleg",                   //its name
            LVMother,              //its mother  volume
            false,                   //no boolean operation
            0,                       //copy number
            checkOverlaps);          //overlaps checking

    new G4PVPlacement(0,                       //no rotation
            G4ThreeVector(0,-(0.5*torsowidth-0.3*width), -0.5*height + 0.5*leglength   ),   
            LVleg,                //its logical volume
            "LVleg",                   //its name
            LVMother,              //its mother  volume
            false,                   //no boolean operation
            1,                       //copy number
            checkOverlaps);          //overlaps checking





    return LVMother;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4LogicalVolume * XIADetectorConstruction::BuildRoughCu(G4double scale){
    G4bool  checkOverlaps = false;
    G4double fudge = 1e-3 * scale;

    //We want roughly 500 bumps so we'll do 30x30 scale units so 50% bump coverage
    int nUnits = 30;
    G4double len = nUnits*scale;

    //We are going to make substrate that is 'scale' high and put stuff on top of it so
    G4VSolid * substrate=  new G4Box("substrate", 0.5*len, 0.5*len , 0.5*scale);  

    //Make a bump
    G4Tubs * bump = new G4Tubs("bump", 0, 0.5*scale, 0.5*(scale+fudge), 0, 360*CLHEP::deg );

    //TRandom3 * rand3 = new TRandom3( (int)scale) ; 
    TRandom3 * rand3 = new TRandom3() ; 

    //Now it gets tricky. Let's make N bumps and add them on top of hte substrate
    int Nbumps = rand3->Poisson( 0.5*nUnits*nUnits );
    //Nbumps = 3;

    //Let's do the first one by hand to create the G4UnionSolid and hte rest will be 
    // done by the loop
    G4double X=rand3->Uniform( -0.5*len, 0.5*len  ), Y= rand3->Uniform( -0.5*len, 0.5*len  ) ;
    G4VSolid  * roughCu  = new G4UnionSolid("roughCu", substrate, bump, 0, G4ThreeVector(X, Y, scale )) ; 

    for( int i =1; i<Nbumps; ++i){
        X=rand3->Uniform( -0.5*len, 0.5*len  ); Y= rand3->Uniform( -0.5*len, 0.5*len  ) ;
        G4VSolid * temp = new G4UnionSolid("roughCu",  roughCu , bump, 0, G4ThreeVector(X, Y, scale )) ;  
        roughCu = temp->Clone();
        //delete temp;
    }

    //We only need to add in the sides of the cylinders....think about it.                  
    G4cout << "RoughCu SurfaceArea = " << len*len  + Nbumps*(scale*CLHEP::pi*scale)  <<G4endl;

    G4LogicalVolume * LVCu= new G4LogicalVolume(  roughCu , Cu_mat, "LVRoughCu" );

    delete rand3;
    return LVCu ;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4LogicalVolume * XIADetectorConstruction::BuildRoughCuWave(G4double scale){
    G4double fudge = 1e-3 * scale;
    fudge = 0; 

    //We want roughly 500 bumps so we'll do 30x30 scale units so 50% bump coverage
    int nUnits = 30;
    G4double len = nUnits*scale;

    //We are going to make substrate that is 'scale' high and put stuff on top of it so
    G4VSolid * substrate=  new G4Box("substrate", 0.5*len, 0.5*len , 0.5*scale);  

    //Make a bump
    G4Tubs * bump = new G4Tubs("bump", 0, 0.5*scale-fudge , 0.5*len-fudge , 0, 360*CLHEP::deg );

    G4RotationMatrix* myRotation = new G4RotationMatrix();
    myRotation->rotateX(90.*deg);
    myRotation->rotateY(0.*deg);
    myRotation->rotateZ(0.*rad);


    //Place it on the edge                                                                                 start on the edge                 
    G4VSolid  * roughCu  = new G4UnionSolid("roughCu", substrate, bump, myRotation , G4ThreeVector( -0.5*len+(0.5+0)*scale,  0  , 0.5*scale )) ; 

    G4VSolid * temp = 0;
    for( int i =1; i<nUnits ; i=i+2){
        temp = new G4SubtractionSolid("roughCu",  roughCu , bump, myRotation , G4ThreeVector( -0.5*len+(0.5+i)*scale  , 0, 0.5*scale )) ; 
        roughCu = temp->Clone();
        if(i==nUnits-1) break;
        temp = new G4UnionSolid("roughCu", roughCu , bump, myRotation , G4ThreeVector( -0.5*len+(1.5+i)*scale  , 0, 0.5*scale )) ; 
        roughCu = temp->Clone();
        //delete temp;
    }

    //We only need to add in the sides of the cylinders....think about it.                  
    //    G4cout << "RoughCu SurfaceArea = " << len*len  + Nbumps*(scale*CLHEP::pi*scale)  <<G4endl;

    G4LogicalVolume * LVCu= new G4LogicalVolume(  roughCu , Cu_mat, "LVRoughCu" );

    return LVCu ;
}




//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


void XIADetectorConstruction::UpdateGeometry(){

    if(fBuildFondren) BuildFondren();
    if(fBuildVeto) BuildVeto();
    if(fBuildTrayLiner) BuildTrayLiner();


    if( fBuildFondren ||  fBuildVeto || fBuildTrayLiner    )   G4RunManager::GetRunManager()->GeometryHasBeenModified();


}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
