//#include "../include/Strawman_pulsefitter.h"
#include "./RobStrawman_pulsefitter.h"
#include "TFile.h"








int main(int argc, char *argv[]){

    if( argc != 3){ 
        std::cout <<" Usage : RerunPulseSim <input file> <output file> \n";
        return 1;
    }

    Double_t EventNum = 0;

    std::vector<Double_t> *          pulse_time =  new   std::vector<Double_t> ;
    std::vector<Double_t> *          Q_time =  new   std::vector<Double_t> ;
    std::vector<Double_t> *          anode_full_Q =  new   std::vector<Double_t> ;
    std::vector<Double_t> *          anode_full_pulse =  new   std::vector<Double_t> ;
    std::vector<Double_t> *          guard_full_Q =  new   std::vector<Double_t> ;
    std::vector<Double_t> *          guard_full_pulse =  new   std::vector<Double_t> ;
    std::vector<Double_t> *          anode_wafer_Q =  new   std::vector<Double_t> ;
    std::vector<Double_t> *          anode_wafer_pulse =  new   std::vector<Double_t> ;
    std::vector<Double_t> *          guard_wafer_Q =  new   std::vector<Double_t> ;
    std::vector<Double_t> *          guard_wafer_pulse =  new   std::vector<Double_t> ;

    std::vector<Double_t> *            X1 = 0;
    std::vector<Double_t> *            Y1 = 0;
    std::vector<Double_t> *            Z1 = 0;
    std::vector<Double_t> *            T1 = 0;
    std::vector<Double_t> *            X3 = 0;
    std::vector<Double_t> *            Y3 = 0;
    std::vector<Double_t> *            Z3 = 0;
    std::vector<Double_t> *            T3 = 0;
    std::vector<Double_t> *            PX1 = 0;
    std::vector<Double_t> *            PY1 = 0;
    std::vector<Double_t> *               PZ1 = 0;
    std::vector<Double_t> *               KE1 = 0;
    std::vector<Double_t> *               Edep = 0;
    std::vector<Double_t> *               Ptype = 0;
    std::vector<Double_t> *               PX3 = 0;
    std::vector<Double_t> *               PY3 = 0;
    std::vector<Double_t> *               PZ3 = 0;
    std::vector<Double_t> *               KE3 = 0;
    std::vector<Double_t> *               TrkID = 0;
    std::vector<Double_t> *               StepN = 0;

    Double_t VetoTop = 0;
    Double_t VetoBottom = 0;


    std::vector<double> *         primary_Ptype =  new   std::vector<Double_t> ;
    std::vector<double> *         primary_X =  new   std::vector<Double_t> ;
    std::vector<double> *         primary_Y =  new   std::vector<Double_t> ;
    std::vector<double> *         primary_Z =  new   std::vector<Double_t> ;
    std::vector<double> *         primary_PX =  new   std::vector<Double_t> ;
    std::vector<double> *         primary_PY =  new   std::vector<Double_t> ;
    std::vector<double> *         primary_PZ =  new   std::vector<Double_t> ;
    std::vector<double> *         primary_KE =  new   std::vector<Double_t> ;
    std::vector<double> *         primary_T =  new   std::vector<Double_t> ;



    TChain * input = new TChain("output");
    input->Add( argv[1] );



    input->SetBranchAddress("EventNum", &EventNum );
    input->SetBranchAddress("pulse_time", &pulse_time );

    input->SetBranchAddress("Q_time", & Q_time );
    input->SetBranchAddress("anode_full_Q", & anode_full_Q );
    input->SetBranchAddress("anode_full_pulse", & anode_full_pulse );
    input->SetBranchAddress("guard_full_Q", & guard_full_Q );
    input->SetBranchAddress("guard_full_pulse", & guard_full_pulse );
    input->SetBranchAddress("anode_wafer_Q", & anode_wafer_Q );
    input->SetBranchAddress("anode_wafer_pulse", & anode_wafer_pulse );
    input->SetBranchAddress("guard_wafer_Q", & guard_wafer_Q );
    input->SetBranchAddress("guard_wafer_pulse", & guard_wafer_pulse );

    input->SetBranchAddress("X1", &X1 );
    input->SetBranchAddress("Y1", &Y1 );
    input->SetBranchAddress("Z1", &Z1 );
    input->SetBranchAddress("T1", &T1 );
    input->SetBranchAddress("X3", &X3 );
    input->SetBranchAddress("Y3", &Y3 );
    input->SetBranchAddress("Z3", &Z3 );
    input->SetBranchAddress("T3", &T3 );
    input->SetBranchAddress("PX1", &PX1 );
    input->SetBranchAddress("PY1", &PY1 );
    input->SetBranchAddress("PZ1", &PZ1 );
    input->SetBranchAddress("KE1", &KE1 );
    input->SetBranchAddress("Edep", &Edep );
    input->SetBranchAddress("Ptype", &Ptype );
    input->SetBranchAddress("PX3", &PX3 );
    input->SetBranchAddress("PY3", &PY3 );
    input->SetBranchAddress("PZ3", &PZ3 );
    input->SetBranchAddress("KE3", &KE3 );
    input->SetBranchAddress("TrkID", &TrkID );
    input->SetBranchAddress("StepN", &StepN );

    input->SetBranchAddress("VetoTop", &VetoTop );
    input->SetBranchAddress("VetoBottom", &VetoBottom );


    input->SetBranchAddress("primary_Ptype", &primary_Ptype );
    input->SetBranchAddress("primary_X", &primary_X );
    input->SetBranchAddress("primary_Y", &primary_Y );
    input->SetBranchAddress("primary_Z", &primary_Z );
    input->SetBranchAddress("primary_PX", &primary_PX );
    input->SetBranchAddress("primary_PY", &primary_PY );
    input->SetBranchAddress("primary_PZ", &primary_PZ );
    input->SetBranchAddress("primary_KE", &primary_KE );
    input->SetBranchAddress("primary_T", &primary_T );



    TFile * outputf = new TFile( argv[2], "RECREATE");
    outputf->cd();
    TTree * output = new TTree("output", "output"); 


    output->Branch("EventNum", &EventNum );
    output->Branch("pulse_time", &pulse_time );

    output->Branch("Q_time", & Q_time );
    output->Branch("anode_full_Q", & anode_full_Q );
    output->Branch("anode_full_pulse", & anode_full_pulse );
    output->Branch("guard_full_Q", & guard_full_Q );
    output->Branch("guard_full_pulse", & guard_full_pulse );
    output->Branch("anode_wafer_Q", & anode_wafer_Q );
    output->Branch("anode_wafer_pulse", & anode_wafer_pulse );
    output->Branch("guard_wafer_Q", & guard_wafer_Q );
    output->Branch("guard_wafer_pulse", & guard_wafer_pulse );

    output->Branch("X1", &X1 );
    output->Branch("Y1", &Y1 );
    output->Branch("Z1", &Z1 );
    output->Branch("T1", &T1 );
    output->Branch("X3", &X3 );
    output->Branch("Y3", &Y3 );
    output->Branch("Z3", &Z3 );
    output->Branch("T3", &T3 );
    output->Branch("PX1", &PX1 );
    output->Branch("PY1", &PY1 );
    output->Branch("PZ1", &PZ1 );
    output->Branch("KE1", &KE1 );
    output->Branch("Edep", &Edep );
    output->Branch("Ptype", &Ptype );
    output->Branch("PX3", &PX3 );
    output->Branch("PY3", &PY3 );
    output->Branch("PZ3", &PZ3 );
    output->Branch("KE3", &KE3 );
    output->Branch("TrkID", &TrkID );
    output->Branch("StepN", &StepN );

    output->Branch("VetoTop", &VetoTop );
    output->Branch("VetoBottom", &VetoBottom );


    output->Branch("primary_Ptype", &primary_Ptype );
    output->Branch("primary_X", &primary_X );
    output->Branch("primary_Y", &primary_Y );
    output->Branch("primary_Z", &primary_Z );
    output->Branch("primary_PX", &primary_PX );
    output->Branch("primary_PY", &primary_PY );
    output->Branch("primary_PZ", &primary_PZ );
    output->Branch("primary_KE", &primary_KE );
    output->Branch("primary_T", &primary_T );



    input->GetEntry(0);
    int nsamples= Q_time->size(), npulse= pulse_time->size();
    TH1D * pulse_Q = new TH1D("Q", "Q", nsamples, 0, 300 );
    TH1D * pulse = 0;
    TString noisefile=" /hpc/group/cdms2/xia_ultralo1800_g4sim/NoiseLibrary/noise.root"; 

    for( int entry=0; entry<input->GetEntries(); ++entry){
        input->GetEntry(entry);

        pulse_Q->Reset();
        for( int i = 0; i< Q_time->size() ; ++i) pulse_Q->SetBinContent( i+1, anode_full_Q->at(i) );  
        //pulse_Q ->SetBinContent( 180, 1000*  pulse_Q ->GetBinContent( 180) );
        pulse=Strawman_pulsefitter(pulse_Q  ,true ,  noisefile,  EventNum );
        for(int i =0; i<pulse->GetNbinsX();++i ) anode_full_pulse->at(i) = pulse->GetBinContent(i+1);
        delete pulse; pulse = 0;


        pulse_Q->Reset();
      //for(int i = 0; i < Q_time->size() ; ++i) {
      //    if (anode_wafer_Q->at(i) < 0.0) {
      //        anode_wafer_Q->at(i) = 0.0;
      //    }
      //}
        for( int i = 0; i< Q_time->size() ; ++i) pulse_Q->SetBinContent( i+1, anode_wafer_Q->at(i));
        /*for(int i = 0; i < Q_time->size() ; ++i) {
            if (pulse_Q->GetBinContent(i) < 0) {
                pulse_Q->SetBinContent(i,0);
            }
        }*/
        /*pulse_Q ->SetBinContent( 354,  -0.0256294027*pulse_Q->GetBinContent(354)); // 1.46552e-19/-5.71812e-18
        pulse_Q ->SetBinContent( 355,  -0.197598412*pulse_Q->GetBinContent(355)); // 7.38447e-19/-3.73711e-18
        pulse_Q ->SetBinContent( 356,  -0.859944799*pulse_Q->GetBinContent(356)); // 7.44959e-19/-8.66287e-19
        pulse_Q ->SetBinContent( 357,  -4.91491578*pulse_Q->GetBinContent(357)); // 7.45519e-19/-1.51685e-19
        pulse_Q ->SetBinContent( 358,  4.33690587*pulse_Q->GetBinContent(358)); // 7.4456e-19/1.7168e-19
        pulse_Q ->SetBinContent( 359,  2.31865688*pulse_Q->GetBinContent(359)); // 7.44319e-19/3.21013e-19
        pulse_Q ->SetBinContent( 360,  1.90272981*pulse_Q->GetBinContent(360)); // 7.44835e-19/3.91456e-19
        pulse_Q ->SetBinContent( 361,  1.68629486*pulse_Q->GetBinContent(361)); // 7.45629e-19/4.4217e-19
        pulse_Q ->SetBinContent( 362,  1.60415005*pulse_Q->GetBinContent(362)); // 7.46249e-19/4.65199e-19
        pulse_Q ->SetBinContent( 363,  1.53214117*pulse_Q->GetBinContent(363)); // 7.46689e-19/4.8735e-19
        pulse_Q ->SetBinContent( 364,  1.38*pulse_Q->GetBinContent(364)); // 7.47217e-19/5.01465e-19
        pulse_Q ->SetBinContent( 365,  1.2*pulse_Q->GetBinContent(365)); // 7.47654e-19/5.12198e-19
        pulse_Q ->SetBinContent( 366,  1.075*pulse_Q->GetBinContent(366)); // 7.48138e-19/5.17131e-19
        pulse_Q ->SetBinContent( 367,  1.015*pulse_Q->GetBinContent(367)); // 7.4868e-19/5.28491e-19*/
        pulse=Strawman_pulsefitter(pulse_Q ,true ,  noisefile,  EventNum );
        for(int i =0; i<pulse->GetNbinsX();++i ) anode_wafer_pulse->at(i) = pulse->GetBinContent(i+1);
        delete pulse;pulse = 0;

        pulse_Q->Reset();
        for( int i = 0; i< Q_time->size() ; ++i) pulse_Q->SetBinContent( i+1, guard_full_Q->at(i) );
        pulse=Strawman_pulsefitter(pulse_Q ,true ,  noisefile,  EventNum );
        for(int i =0; i<pulse->GetNbinsX();++i ) guard_full_pulse->at(i) = pulse->GetBinContent(i+1);
        delete pulse;pulse = 0;

        pulse_Q->Reset();
        for( int i = 0; i< Q_time->size() ; ++i) pulse_Q->SetBinContent( i+1, guard_wafer_Q->at(i) );
        pulse=Strawman_pulsefitter(pulse_Q ,true ,  noisefile,  EventNum );
        for(int i =0; i<pulse->GetNbinsX();++i ) guard_wafer_pulse->at(i) = pulse->GetBinContent(i+1);
        delete pulse;pulse = 0;



        output->Fill();

    }

    outputf->Write();
    outputf->Close();



    return 0;
}


