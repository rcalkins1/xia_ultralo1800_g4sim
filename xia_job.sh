#!/bin/bash
#SBATCH -p htc

#cd ~rcalkins/xia_ultralo1800_g4sim/
cd /hpc/group/cdms2/xia_ultralo1800_g4sim

. setup.sh

echo $HOSTNAME
#export GARFIELD_INSTALL=/hpc/group/cdms2/garfield/

/usr/bin/time -v XIA_sim $1 $RANDOM $RANDOM 
#valgrind  --leak-check=full  --show-reachable=yes ./XIA_sim $1


