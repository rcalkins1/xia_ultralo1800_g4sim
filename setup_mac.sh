#!/bin/bash

#This is your working directory by finding out where this script lives
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"


#Latest Geant4 version
#. ~/geant4build/share/Geant4-10.4.1/geant4make/geant4make.sh
. ~/geant4install/share/Geant4-10.4.1/geant4make/geant4make.sh 

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$QTLIBPATH

#We are going to set G4WORKDIR to our directory to keep track of our binaries
G4WORKDIR=${DIR}

#add it to the path
PATH=$G4WORKDIR/bin/Linux-g++/:$PATH
export GARFIELD_HOME=~/garfieldpp/   #you'll need to set this to whatever your setup is 
export CRYHOME=~/cry_v1.7/    # Maybe we'll use this someday
export HEED_DATABASE=$GARFIELD_HOME/Heed/heed++/database
export LD_LIBRARY_PATH=$GARFIELD_HOME/Library/:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$CRYHOME/lib/:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$G4LIB:$LD_LIBRARY_PATH

