//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: XIARunAction.cc 87359 2014-12-01 16:04:27Z gcosmo $
//
/// \file XIARunAction.cc
/// \brief Implementation of the XIARunAction class

#include "XIARunAction.hh"
#include "XIAPrimaryGeneratorAction.hh"
#include "XIADetectorConstruction.hh"
#include "XIARun.hh"

#include "GarfieldPhysics.hh"

#include "G4RunManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

#include <vector>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

    XIARunAction::XIARunAction()
: G4UserRunAction()
{ 
    // add new units for dose
    // 
    const G4double milligray = 1.e-3*gray;
    const G4double microgray = 1.e-6*gray;
    const G4double nanogray  = 1.e-9*gray;  
    const G4double picogray  = 1.e-12*gray;

    new G4UnitDefinition("milligray", "milliGy" , "Dose", milligray);
    new G4UnitDefinition("microgray", "microGy" , "Dose", microgray);
    new G4UnitDefinition("nanogray" , "nanoGy"  , "Dose", nanogray);
    new G4UnitDefinition("picogray" , "picoGy"  , "Dose", picogray);        

    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    G4cout << "Using " << analysisManager->GetType() << G4endl;
    analysisManager->SetFileName("output");

    //GetXIA run for event info
    // XIARun* run 
    //     = static_cast<XIARun*>(
    //             G4RunManager::GetRunManager()->GetNonConstCurrentRun());



}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

XIARunAction::~XIARunAction()
{
    delete G4AnalysisManager::Instance();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Run* XIARunAction::GenerateRun()
{
    return new XIARun; 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void XIARunAction::BeginOfRunAction(const G4Run*)
{ 
    //inform the runManager to save random number seed
    G4RunManager::GetRunManager()->SetRandomNumberStore(false);


    GarfieldPhysics* garfieldPhysics = GarfieldPhysics::GetInstance();
    garfieldPhysics->UpdateGarfield();


    XIARun* run 
        = static_cast<XIARun*>(
                G4RunManager::GetRunManager()->GetNonConstCurrentRun());

    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

    // Open an output file
    //
    analysisManager->OpenFile();

    analysisManager->CreateH1("meta","meta;number of events;events", 1,-0.5,1.5 );


    if ( run->GetRunID() == 0  ) {

        // Creating ntuple
        //
        analysisManager->CreateNtuple("output", "output");


        analysisManager->CreateNtupleDColumn("EventNum"                                          );  //   000
        analysisManager->CreateNtupleDColumn("pulse_time",         run->Get_pulse_time()         );  //   001
        analysisManager->CreateNtupleDColumn("Q_time",             run->Get_Q_time()             );  //   002
        analysisManager->CreateNtupleDColumn("anode_full_Q",       run->Get_anode_full_Q()       );  //   003
        analysisManager->CreateNtupleDColumn("anode_full_pulse",   run->Get_anode_full_pulse()   );  //   004
        analysisManager->CreateNtupleDColumn("guard_full_Q",       run->Get_guard_full_Q()       );  //   005
        analysisManager->CreateNtupleDColumn("guard_full_pulse",   run->Get_guard_full_pulse()   );  //   006
        analysisManager->CreateNtupleDColumn("anode_wafer_Q",      run->Get_anode_wafer_Q()      );  //   007
        analysisManager->CreateNtupleDColumn("anode_wafer_pulse",  run->Get_anode_wafer_pulse()  );  //   008
        analysisManager->CreateNtupleDColumn("guard_wafer_Q",      run->Get_guard_wafer_Q()      );  //   009
        analysisManager->CreateNtupleDColumn("guard_wafer_pulse",  run->Get_guard_wafer_pulse()  );  //   010

        analysisManager->CreateNtupleDColumn("X1",                 run->Get_X1()                 );  //   011
        analysisManager->CreateNtupleDColumn("Y1",                 run->Get_Y1()                 );  //   012
        analysisManager->CreateNtupleDColumn("Z1",                 run->Get_Z1()                 );  //   013
        analysisManager->CreateNtupleDColumn("T1",                 run->Get_T1()                 );  //   014
        analysisManager->CreateNtupleDColumn("X3",                 run->Get_X3()                 );  //   015
        analysisManager->CreateNtupleDColumn("Y3",                 run->Get_Y3()                 );  //   016
        analysisManager->CreateNtupleDColumn("Z3",                 run->Get_Z3()                 );  //   017
        analysisManager->CreateNtupleDColumn("T3",                 run->Get_T3()                 );  //   018
        analysisManager->CreateNtupleDColumn("PX1",                run->Get_PX1()                );  //   019
        analysisManager->CreateNtupleDColumn("PY1",                run->Get_PY1()                );  //   020
        analysisManager->CreateNtupleDColumn("PZ1",                run->Get_PZ1()                );  //   021  
        analysisManager->CreateNtupleDColumn("KE1",                run->Get_KE1()                );  //   022  
        analysisManager->CreateNtupleDColumn("Edep",               run->Get_Edep()               );  //   023  
        analysisManager->CreateNtupleDColumn("Ptype",              run->Get_Ptype()              );  //   024  
        analysisManager->CreateNtupleDColumn("PX3",                run->Get_PX3()                );  //   025  
        analysisManager->CreateNtupleDColumn("PY3",                run->Get_PY3()                );  //   026  
        analysisManager->CreateNtupleDColumn("PZ3",                run->Get_PZ3()                );  //   027  
        analysisManager->CreateNtupleDColumn("KE3",                run->Get_KE3()                );  //   028  
        analysisManager->CreateNtupleDColumn("TrkID",              run->Get_TrkID()              );  //   029  
        analysisManager->CreateNtupleDColumn("StepN",              run->Get_StepN()              );  //   030   

        analysisManager->CreateNtupleDColumn("VetoTop"                                           );  //   031   
        analysisManager->CreateNtupleDColumn("VetoBottom"                                        );  //   032   


         analysisManager->CreateNtupleDColumn("primary_Ptype"     ,  run->Get_primary_Ptype()    );  //   033   
         analysisManager->CreateNtupleDColumn("primary_X"         ,  run->Get_primary_X()        );  //   034
         analysisManager->CreateNtupleDColumn("primary_Y"         ,  run->Get_primary_Y()        );  //   035
         analysisManager->CreateNtupleDColumn("primary_Z"         ,  run->Get_primary_Z()        );  //   036
         analysisManager->CreateNtupleDColumn("primary_PX"        ,  run->Get_primary_PX()       );  //   037
         analysisManager->CreateNtupleDColumn("primary_PY"        ,  run->Get_primary_PY()       );  //   038
         analysisManager->CreateNtupleDColumn("primary_PZ"        ,  run->Get_primary_PZ()       );  //   039
         analysisManager->CreateNtupleDColumn("primary_KE"        ,  run->Get_primary_KE()       );  //   040
         analysisManager->CreateNtupleDColumn("primary_T"         ,  run->Get_primary_T()        );  //   041





        //This is how we used to do it, one entry per step
        /*

           analysisManager->CreateNtupleDColumn("X1");        //  000
           analysisManager->CreateNtupleDColumn("Y1");        //  001
           analysisManager->CreateNtupleDColumn("Z1");        //  002
           analysisManager->CreateNtupleDColumn("T1");        //  003
           analysisManager->CreateNtupleDColumn("X3");        //  004
           analysisManager->CreateNtupleDColumn("Y3");        //  005
           analysisManager->CreateNtupleDColumn("Z3");        //  006
           analysisManager->CreateNtupleDColumn("T3");        //  007
           analysisManager->CreateNtupleDColumn("PX1");       //  008
           analysisManager->CreateNtupleDColumn("PY1");       //  009
           analysisManager->CreateNtupleDColumn("PZ1");       //  010
           analysisManager->CreateNtupleDColumn("KE1");       //  011
           analysisManager->CreateNtupleDColumn("EventNum");  //  012
           analysisManager->CreateNtupleDColumn("Edep");      //  013
           analysisManager->CreateNtupleDColumn("Ptype");     //  014
           analysisManager->CreateNtupleDColumn("PX3");       //  015
           analysisManager->CreateNtupleDColumn("PY3");       //  016
           analysisManager->CreateNtupleDColumn("PZ3");       //  017
           analysisManager->CreateNtupleDColumn("KE3");       //  018
           analysisManager->CreateNtupleDColumn("TrkID");     //  019
           analysisManager->CreateNtupleDColumn("StepN");     //  020
           */

        analysisManager->FinishNtuple();

    }

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void XIARunAction::EndOfRunAction(const G4Run* run)
{
    G4int nofEvents = run->GetNumberOfEvent();
    if (nofEvents == 0) return;

    //const XIARun* Run = static_cast<const XIARun*>(run);
    G4cout <<"Produced " << nofEvents << " events \n";    


    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    // save histograms & ntuple
    //
    analysisManager->Write();
    analysisManager->CloseFile();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
