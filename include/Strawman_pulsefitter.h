#include <iostream>
#include <vector>

#include "TH1D.h"
#include "TRandom3.h"
#include "TChain.h"
#include "TMath.h"

//   
//                              R2
//                        +----/\/\/--+
//                        |           |
//                        |     C2    |
//                        +----| |----+
//                        |           |
//                        |           |
//          C1            |   |\      |     +_______+
//     +-----||---------------|-\ ____|_____| pulse |
//     |                      |  /          | V_o   |
//     |                  +---|+/           +-------+
//     |                  |   |/
//  +------+            -----
//  |charge|             ---
//  | V_in |
//  +------+ 
//     |
//   -----
//    ---
// 
// If we look at this circuit, it is basically a low pass filter or an integrator
// that decays with some time constant. We can measure this time constant from 
// the data and make a simple model of the transfer function


TH1D * ImpulseFilter(TH1D * h_in){
    float RC = 1./( 2*TMath::Pi() *200);
    float dt= 0.5 ;
    float alpha = dt/( RC+dt) ;
    TH1D * h_out = (TH1D*) h_in->Clone();
    h_out->Reset();

    h_out->SetBinContent(1, alpha* h_in->GetBinContent(1) );
    for(int i=2;i< h_in->GetNbinsX() +1 ; ++i ){
        h_out->SetBinContent(i, alpha* h_in->GetBinContent(i) +(1-alpha)*h_out->GetBinContent( i-1 ) );
    }

    return h_out;
}



TH1D * Strawman_pulsefitter(TH1D* h_in, bool donoise, TString noiselib , int noise_index){
    int N = 600; //h_in->GetNbinsX();
    TH1D* h_out = 0;
    TH1D* h_noise= 0;
    TRandom3 * r = new TRandom3();
    r->SetSeed();


    h_in = ImpulseFilter(h_in);

    //h_out = (TH1D*)h_in->Clone((TString)h_in->GetName()+"_out" );
    //let's make this generic so it is independent of induction binning
    h_out = new TH1D((TString)h_in->GetName()+"_out","output", 600,0,300 );
    //Maybe do trigger time smearing but why
    //int offset = 0;
    //float sigma=27;
    //if(!iswafer) sigma= 33; 
    //offset= r->Gaus( 0, sigma);
    //for(int i=1;i<N+1;++i) h_out->Fill(h_in->GetBinCenter(i) , h_in->GetBinContent(i) );
    for(int i=1;i<h_in->GetNbinsX()   +1;++i) h_out->Fill(h_in->GetBinCenter(i) , h_in->GetBinContent(i) );


    //Why the factor of 1.04444? I don't know but it makes the energies line up better. This calibration from ADC->MeV was always going to be a bit of a fudge
    double joshfactor = 0.897225;
    h_out->Scale( joshfactor*1.106023*1.0955374494674022e+19 ); 
    // anode : 1 G1 : 0.951324 G2: 0.940763 G3 : 0.694431 G4 : 0.958793  full: 1  full1: 0.958528 full guard: 0.916922

    //Rescale the other channels which is relative to the calibration of the wafer channel
    TString name = (TString)h_in->GetName();
    if(name.TString::Contains("guard_wafer") ) h_out->Scale( 0.958793   );
    if(name.TString::Contains("anode_full")  ) h_out->Scale( 0.964202   );
    if(name.TString::Contains("guard_full")  ) h_out->Scale( 0.884098   );



    //Simple decaying intergrater                    integrate                and decay  
    for(int i=1;i< N+1;++i) h_out->SetBinContent(i, h_out->GetBinContent(i) + (1.0-0.00249)*h_out->GetBinContent(i-1) );


    if(donoise){
        h_noise= (TH1D*)h_out->Clone(name+"_noise" );
        h_noise->Reset();

        TChain * data =0;

        if(  name.TString::Contains(  "anode_wafer"  )  )  data  =  new  TChain(  "anode_wafer"  );
        if(  name.TString::Contains(  "guard_wafer"  )  )  data  =  new  TChain(  "guard_wafer"  );
        if(  name.TString::Contains(  "anode_full"   )  )  data  =  new  TChain(  "anode_full"   );
        if(  name.TString::Contains(  "guard_full"   )  )  data  =  new  TChain(  "guard_full"   );

        //If you want to use the older noise for your events. It was the same channel applied to all so it is not really proper
       delete data;
        data = new TChain("data");

        //std::cout << noiselib <<std::endl;
        data->Add(noiselib);

        std::vector<double> *Pulse_Anode = 0;
        //std::vector<double> *Pulse_SampleTime = 0;
        //Int_t id =0;

        data->SetBranchAddress("Pulse_Anode", &Pulse_Anode );
        //data->SetBranchAddress("Pulse_SampleTime", &Pulse_SampleTime );
        //data->SetBranchAddress("id", &id );


        if(noise_index>= data->GetEntries() ) noise_index = noise_index%data->GetEntries();
        data->GetEntry(noise_index);

        int nsamp=Pulse_Anode->size() ;
        int rand_shift = floor(r->Uniform(0,nsamp) );

        //we are going to find the slope/bias and slide along it so that our pulse is continuous
        float bias = Pulse_Anode->at(0);
        float slope = (Pulse_Anode->at(nsamp -1 ) - bias)/nsamp;

        int j=0;
        for(int i=1;i<nsamp+1;++i) {
            j= (i+rand_shift-1)%nsamp;
            h_noise->SetBinContent( i, bias +slope*(i-1) + ( Pulse_Anode->at(j) - slope*j-bias) );
        }


        h_out->Add(h_noise);
        delete data;
        delete h_noise;
    }


    //We rail at 16383 ADC counts
    for(int i=1;i<N+1;++i)  if( h_out->GetBinContent(i) >16383){    h_out->SetBinContent(i, 16383 ); }

    delete r;
    return h_out;
}






