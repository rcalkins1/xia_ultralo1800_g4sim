#ifndef GarfieldPhysicsList_h
#define GarfieldPhysicsList_h 1


#include "G4VModularPhysicsList.hh"
//#include "G4VUserPhysicsList.hh"
#include "globals.hh"


class GarfieldPhysicsList: public G4VModularPhysicsList {
public:
	GarfieldPhysicsList(bool DoRadDecay=true);
	virtual ~GarfieldPhysicsList();
	virtual void SetCuts();
	virtual void ConstructParticle();
	virtual void ConstructProcess();
protected:
	// these methods Construct physics processes and register them
	void AddParameterisation();
    bool doraddecay;

};


#endif /* GarfieldPhysicsList_h */
