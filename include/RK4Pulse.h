#include "TH1D.h"
#include "TVirtualFFT.h"
#include "TF1.h"
#include "TMath.h"
#include "TCanvas.h"

#include <iostream>

//   
//   
//                              R2
//                        +----/\/\/--+
//                        |           |
//                        |     C2    |
//                        +----| |----+
//                        |           |
//                        |           |
//          C1            |   |\      |     +_______+
//     +-----||---------------|-\ ____|_____| pulse |
//     |                      |  /          | V_o   |
//     |                  +---|+/           +-------+
//     |                  |   |/
//  +------+            -----
//  |charge|             ---
//  | V_in |
//  +------+ 
//     |
//   -----
//    ---
//   
//      dV_o(t)  - V_o(t) -  C1 dV_in(t)
//      ----   =  ----     ------------  
//      dt          C2R2       C2 dt




TH1D * RK4Pulse(TH1D * charge, double V0=0 , float R1C = pow(10,-12), float R2C = pow(10,-3) , float width = 300){
    //float h = (  charge->GetBinLowEdge(nsteps +1)   - charge->GetBinLowEdge(1) )/nsteps ; 

    //Set up the time constants
    R1C*=-1, R2C*= -1; //flip the signs here, doesn't really matter

    //Convert from s to bins
    R1C /= 1e-6;  // conversion for us
    R1C *= charge->GetBinWidth(1); // conversion to bins
    R2C /= 1e-6;  // conversion for us
    R2C *= charge->GetBinWidth(1); // conversion to bins

    //We are going to invert them. THe reason is simply that it is faster to multiply than divide so we'll divide once here and multiply for each of the steps
    R1C = 1.0/R1C;
    R2C = 1.0/R2C;



    TH1D * Precharge = (TH1D*)charge->Clone();
    //    Precharge->Reset();

    //float width = 300.0;

    // for(int bin=1; bin < Precharge->GetNbinsX() +1; ++bin){

    // //    std::cout <<  charge->GetBinContent(bin) << std::endl;
    //     //        std::cout <<  gaus->Eval(Precharge->GetBinCenter(bin) ) << std::endl;
    //     for(int bin1=bin; bin1 < Precharge->GetNbinsX() +1; ++bin1) Precharge->SetBinContent(bin1, Precharge->GetBinContent(bin1)+charge->GetBinContent(bin)*exp( -pow(charge->GetBinCenter(bin)-charge->GetBinCenter(bin1), 2)/width )     );
    // }

    //TCanvas * C  = new TCanvas();
    //Precharge->Draw();
    //C->SaveAs("mytest.C");



    TH1D * pulse = (TH1D*)charge->Clone();
    pulse->Reset();
    pulse->Rebin(2);// We have factors of .5 in  here 
    pulse->SetName("XIA_pulse");
    pulse->SetTitle("XIA pulse;Time [us]; Arb. Units");


    int nsteps = pulse ->GetNbinsX();
    float h =  pulse ->GetBinWidth(1); // 

    double k1,k2,k3,k4, yn,tn ;

    //y0, t0;
    pulse->SetBinContent(1, V0 );

    //yn, tn
    for(int n=4; n< 2*nsteps+1; n+=2){
        //        cout <<  n  <<" : "<< n/2-1 <<endl;
        //    std::cout << yn*R2C <<" " <<  (Precharge->GetBinContent(  n/2      ) - Precharge->GetBinContent(  n/2     -1 ))* R1C/*Precharge->GetBinWidth(  n/2      )*/ <<std::endl;
        yn = pulse->GetBinContent(n/2-1);
        k1 = yn*R2C            + R1C*(Precharge->GetBinContent(  n/2      ) )/*- Precharge->GetBinContent(  n/2     -1 ))  /Precharge->GetBinWidth(  n/2      )*/; 
        k2 = (yn+0.5*k1*h)*R2C + R1C*(Precharge->GetBinContent(  n/2 + 1  ) )/*- Precharge->GetBinContent(  n/2 + 1 -1 ))  /Precharge->GetBinWidth(  n/2 + 1  )*/; 
        k3 = (yn+0.5*k2*h)*R2C + R1C*(Precharge->GetBinContent(  n/2 + 1  ) )/*- Precharge->GetBinContent(  n/2 + 1 -1 ))  /Precharge->GetBinWidth(  n/2 + 1  )*/; 
        k4 = (yn+k3*h)*R2C     + R1C*(Precharge->GetBinContent(  n/2 + 2  ) )/*- Precharge->GetBinContent(  n/2 + 2 -1 ))  /Precharge->GetBinWidth(  n/2 + 2  )*/; 
        yn += (h/6)*(k1+2*k2+2*k3+k4);
        pulse->SetBinContent( n/2 , yn ); 

    }
    pulse->Scale(-1);






    //Gaussian smearing
    // for(int bin=1; bin < pulse->GetNbinsX() +1; ++bin){
    //      for(int bin1=bin; bin1 < pulse->GetNbinsX() +1; ++bin1) pulse->SetBinContent(bin1, pulse->GetBinContent(bin1)+charge->GetBinContent(bin)*exp( -pow(charge->GetBinCenter(bin)-charge->GetBinCenter(bin1), 2)/width )     );
    //  }

 // TH1D * pulse_org = (TH1D*) pulse->Clone(); 
 // pulse->Reset(); 
 // for(int bin=1; bin < pulse->GetNbinsX() +1; ++bin){
 //     for(int bin1=bin; bin1 < pulse->GetNbinsX() +1; ++bin1) pulse->SetBinContent(bin1, pulse_org->GetBinContent(bin1)*exp( -(pulse->GetBinCenter(bin)-pulse->GetBinCenter(bin))/4) + pulse->GetBinContent(bin1)     );
 // }


    delete Precharge;

    return pulse;
}


TH1D * CalibratePulse(TH1D * rawpulse ){


    //now let's apply some calibration
    float  Calibration = 1.0; // 2*1.005190 * 2.3e9 ;
    float   ADC_offset  = 0;// 5.69812e+03    ;
    float   time_offset = 0; //-4.89159e-01 ;

    TH1D* testpulse = (TH1D*)rawpulse->Clone();
    testpulse ->Reset();

    //Shift the pulse around before doing anything
    for(int i=1;i<testpulse->GetNbinsX() +1; ++i){
        int bin = i + (int) time_offset;
        testpulse->SetBinContent(i, rawpulse->GetBinContent( bin) );
    }

    testpulse->Scale( Calibration );


    for(int i=1; i< testpulse->GetNbinsX() +1; ++i ){
        testpulse->SetBinContent( i , ADC_offset + testpulse->GetBinContent(i)  );
    }

    //  TCanvas * C = new TCanvas();
    //  testpulse->Draw();
    //  C->SaveAs("testpulse.C");
    //  delete C;

    return testpulse;
}






