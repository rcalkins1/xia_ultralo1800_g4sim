//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: GarfieldPhysicsList.cc,v 1.19 2010-10-23 19:14:03 gum Exp $
// GEANT4 tag $Name: not supported by cvs2svn $
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
#include "GarfieldPhysicsList.hh"
#include "GarfieldPhysics.hh"
#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"
#include "QGSP_BERT_HP.hh"
#include "G4FastSimulationManagerProcess.hh"
#include "G4PAIModel.hh"
#include "G4PAIPhotModel.hh"
#include "G4LossTableManager.hh"
#include "G4EmConfigurator.hh"
#include "G4Region.hh"
#include "G4RegionStore.hh"
#include "G4ProductionCuts.hh"
#include "G4RadioactiveDecay.hh"
#include "G4PhysListFactory.hh"
#include "G4GenericIon.hh"




//REC Stuff I added to incorporate sCDMS low ion physics
#include "G4BraggIonGasModel.hh"
#include "G4BetheBlochIonGasModel.hh"
#include "G4IonFluctuations.hh"
#include "G4UniversalFluctuation.hh"
#include "G4PhysicalConstants.hh"

#include "G4MuMultipleScattering.hh"
#include "G4MuIonisation.hh"
#include "G4MuBremsstrahlung.hh"
#include "G4MuPairProduction.hh"






//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GarfieldPhysicsList::GarfieldPhysicsList(bool DoRadDecay) :
    G4VModularPhysicsList() {
        G4int verb = 0;
        SetVerboseLevel(verb);
        defaultCutValue = 1 * CLHEP::mm;
//        QGSP_BERT_HP *physicsList = new QGSP_BERT_HP;
        G4PhysListFactory factory;
        G4VModularPhysicsList * physicsList  = factory.GetReferencePhysList ("QGSP_BERT_HP_EMZ");

        doraddecay=DoRadDecay;
        for (G4int i = 0;; ++i) {
            G4VPhysicsConstructor* elem =
                const_cast<G4VPhysicsConstructor*>(physicsList->GetPhysics(i));
            if (elem == NULL)
                break;
            G4cout << "RegisterPhysics: " << elem->GetPhysicsName() << G4endl;
            RegisterPhysics(elem);
        }

    }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

GarfieldPhysicsList::~GarfieldPhysicsList() {

}

void GarfieldPhysicsList::AddParameterisation() {

    GarfieldPhysics* garfieldPhysics = GarfieldPhysics::GetInstance();

    G4FastSimulationManagerProcess* fastSimProcess_garfield =
        new G4FastSimulationManagerProcess("G4FSMP_garfield");

    //for versions of Geant before 10.3
    // theParticleIterator->reset();

    //for versions of Geant4 10.3+
    //auto theParticleIterator=GetParticleIterator();


    auto theParticleIterator = G4ParticleTable::GetParticleTable()->GetIterator();
    theParticleIterator->reset();


    while ((*theParticleIterator)()) {

        G4ParticleDefinition* particle = theParticleIterator->value();
        G4ProcessManager* pmanager = particle->GetProcessManager();
        G4EmConfigurator* config = G4LossTableManager::Instance()->EmConfigurator();
        G4LossTableManager::Instance()->SetVerbose(1);

        //G4PAIPhotModel* paiPhot = new G4PAIPhotModel(particle, "G4PAIPhotModel");
        G4PAIModel* paiPhot = new G4PAIModel(particle, "G4PAIModel");

        //G4cout << " REC: Particle " <<  particle->GetParticleName() <<G4endl;

        if (garfieldPhysics->FindParticleName(particle->GetParticleName())) {
            pmanager->AddDiscreteProcess(fastSimProcess_garfield);
        }

        if (particle->GetParticleName() == "e-"
                || particle->GetParticleName() == "e+") {

            config->SetExtraEmModel(particle->GetParticleName(), "eIoni", paiPhot, "RegionGarfield", 0.0 * keV, 100. * TeV, paiPhot);

        } else if (particle->GetParticleName() == "mu-"
                || particle->GetParticleName() == "mu+") {

            pmanager->AddProcess(new G4MuMultipleScattering, -1, 1, 1);
            pmanager->AddProcess(new G4MuIonisation,         -1, 2, 2);
            pmanager->AddProcess(new G4MuBremsstrahlung,     -1, 3, 3);
            pmanager->AddProcess(new G4MuPairProduction,     -1, 4, 4);

            //This gives funny results....
            //          config->SetExtraEmModel(particle->GetParticleName(), "muIoni",
            //                  paiPhot, "RegionGarfield", 0.01 * keV, 100. * TeV, paiPhot);

        } else if (particle->GetParticleName() == "proton"
                || particle->GetParticleName() == "pi+"
                || particle->GetParticleName() == "pi-") {

            //            config->SetExtraEmModel(particle->GetParticleName(), "hIoni",
            //                    paiPhot, "RegionGarfield", 0.01 * keV, 100. * TeV, paiPhot);

            //17/03/15 REC I have added this.
            G4BraggIonGasModel* mod1 = new G4BraggIonGasModel();
            G4BetheBlochIonGasModel* mod2 = new G4BetheBlochIonGasModel();
            G4double eth = 2.*CLHEP::MeV*particle->GetPDGMass()/proton_mass_c2;
            config->SetExtraEmModel(particle->GetParticleName() ,"ionIoni",mod1,"",0.0,eth,
                    new G4IonFluctuations());
            config->SetExtraEmModel(particle->GetParticleName() ,"ionIoni",mod2,"",eth,100*TeV,
                    new G4UniversalFluctuation());




        } else if (particle->GetParticleName() == "alpha"
                || particle->GetParticleName() == "He3"
                || particle->GetParticleName() == "GenericIon") {

            // This is what was used before
            ////////	config->SetExtraEmModel(particle->GetParticleName(), "ionIoni", paiPhot,
            ////////			"RegionGarfield", 0.01 * keV, 100. * TeV, paiPhot);

            G4BraggIonGasModel* mod1 = new G4BraggIonGasModel();
            G4BetheBlochIonGasModel* mod2 = new G4BetheBlochIonGasModel();
            G4double eth = 2.*CLHEP::MeV*particle->GetPDGMass()/proton_mass_c2;
            config->SetExtraEmModel(particle->GetParticleName() ,"ionIoni",mod1,"",0.0,eth,
                    new G4IonFluctuations());
            config->SetExtraEmModel(particle->GetParticleName() ,"ionIoni",mod2,"",eth,100*TeV,
                    new G4UniversalFluctuation());



        }

    }

    if(doraddecay){
        //Let's turn on radioactive decays
        G4RadioactiveDecay*  theRadioactiveDecay = new G4RadioactiveDecay();
        G4GenericIon* ion = G4GenericIon::GenericIon();
        G4ProcessManager* pmanager = ion->GetProcessManager();
        pmanager->AddProcess(theRadioactiveDecay, 0, -1, 3 );

    }
}

void GarfieldPhysicsList::SetCuts() {
    //G4ProductionCutsTable::GetProductionCutsTable()->SetEnergyRange(100. * eV,
    //G4ProductionCutsTable::GetProductionCutsTable()->SetEnergyRange(1. * eV, 100. * TeV);
    G4ProductionCutsTable::GetProductionCutsTable()->SetEnergyRange(18.3 * eV,   100. * TeV);

    SetCutsWithDefault();

    G4Region *region = G4RegionStore::GetInstance()->GetRegion(
            "RegionGarfield");
    G4ProductionCuts * cuts = new G4ProductionCuts();
    cuts->SetProductionCut(1 * um, G4ProductionCuts::GetIndex("gamma"));
    cuts->SetProductionCut(1 * um, G4ProductionCuts::GetIndex("e-"));
    cuts->SetProductionCut(1 * um, G4ProductionCuts::GetIndex("e+"));

    G4EmParameters* emParams = G4EmParameters::Instance ();
    emParams ->SetLowestElectronEnergy(30*eV);

    //REC -why not?
    //	cuts->SetProductionCut(1 * um, G4ProductionCuts::GetIndex("alpha"));

    if (region) {
        region->SetProductionCuts(cuts);
    }

    DumpCutValuesTable();
}

void GarfieldPhysicsList::ConstructParticle() {
    G4VModularPhysicsList::ConstructParticle();
}

void GarfieldPhysicsList::ConstructProcess() {
    G4VModularPhysicsList::ConstructProcess();
    AddParameterisation();
}

