//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: XIADetectorConstruction.hh 69565 2013-05-08 12:35:31Z gcosmo $
//
/// \file XIADetectorConstruction.hh
/// \brief Definition of the XIADetectorConstruction class

#ifndef XIADetectorConstruction_h
#define XIADetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "G4GenericMessenger.hh"
#include "G4NistManager.hh"
#include "globals.hh"
#include "G4SystemOfUnits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

class G4VPhysicalVolume;
class G4LogicalVolume;
class GarfieldG4FastSimulationModel;

/// Detector construction class to define materials and geometry.

class XIADetectorConstruction : public G4VUserDetectorConstruction
{
    public:
        XIADetectorConstruction();
        virtual ~XIADetectorConstruction();

        virtual G4VPhysicalVolume* Construct();

        G4LogicalVolume* GetScoringVolume() const { return fScoringVolume; }
        G4VPhysicalVolume* GetScoringPVolume() const { return fScoringPVolume; }

        inline  G4LogicalVolume* GetTopVeto() const { return fTopVeto; }
        inline  G4LogicalVolume* GetBottomVeto() const { return fBottomVeto; }


        GarfieldG4FastSimulationModel* fGarfieldG4FastSimulationModel;

        void  BuildMaterials();

        G4LogicalVolume * BuildHEPAFilter();
        G4LogicalVolume * BuildVetoPanel();
        G4LogicalVolume * BuildPerson(G4double height = 2*CLHEP::m );
        G4LogicalVolume * BuildRoughCu(G4double scale= 10*CLHEP::um );
        G4LogicalVolume * BuildRoughCuWave(G4double scale= 10*CLHEP::um );


        inline void SetBuildFondren(bool flag=false){ fBuildFondren = flag; }
        inline void SetBuildVeto(bool flag=false){ fBuildVeto = flag; }
        inline float GetFondrenHeight(){return fFondrenH; }

        void BuildFondren();
        void BuildCleanroom();
        void BuildBuilding();
        void BuildVeto();
        void BuildTrayLiner();
        
        void UpdateGeometry();



    protected:
        G4LogicalVolume*  fScoringVolume;
        G4VPhysicalVolume*  fScoringPVolume;
        G4LogicalVolume*  fTopVeto;
        G4LogicalVolume*  fBottomVeto;
        G4LogicalVolume* logicWorld ;
        G4VPhysicalVolume* physWorld ;
        G4LogicalVolume* logicEnv ;



        G4GenericMessenger * fMessenger;

        bool  fBuildFondren  =  false;
        bool  fBuildVeto     =  false;
        bool  fBuildTrayLiner  =  false;


        G4NistManager* nist ;
        G4Material* air_mat ;
        G4Material* H2O_mat ;
        G4Material* Ar_mat ;
        G4Material* Si_mat ;
        G4Material* Al_mat ;
        G4Material* Cu_mat ;
        G4Material* Pb_mat ;
        G4Material* vacuum_mat ;
        G4Material* concrete_mat ;
        G4Material* plexy_mat ; 
        G4Material* scint_mat ;
        G4Material* teflon_mat;
        G4Material* steel_mat;
        G4Material * filter_mat ;
        G4Material * human_mat ;
        G4Material * poly_mat ; 
        G4Material * norite_mat ; 


        G4VisAttributes* invis;
        G4VisAttributes* blue;

        //Constants for construction

        const  G4double  inches               =  .0254*CLHEP::m;
        const  G4double  feet                 =  0.3048*CLHEP::m;

        // Dimension parameters
        const  G4double  env_sizeXY           =  23.75*inches,     env_sizeZ    =      15*CLHEP::cm;
        const  G4double  Al_thickness         =  0.2               *CLHEP::cm;  //cm
        const  G4double  Cu_thickness         =  0.2               *CLHEP::cm;  //cm

        const  G4double  fFondrenH            =  11*CLHEP::m;

        const  G4double  ElecBoxX             =  22*CLHEP::cm,            ElecBoxY     =      40*CLHEP::cm,         ElecBoxZ  =  4.5*CLHEP::cm;
        const  G4double  HEPAWidth            =  2*feet;
        const  G4double  HEPALength           =  4*feet;
        const  G4double  HEPAHeight           =  12.5*inches;

        const  G4double  XIA_baseZ            =  11*CLHEP::cm;
        const  G4double  XIAoffset            =  46*inches         ;            //     1.1684*CLHEP::m;
        const  G4double  XIAXY                =  0.5*23.75*inches  +.2*CLHEP::cm       ;

        const G4double XIAsteellength         = 31*inches;
        G4double XIAsteelheight               = 12*inches + XIA_baseZ; //this shouldn't change either but just in case...
        const  G4double  steel_thickness         =  0.2               *CLHEP::cm;  //cm


        const  G4double  XIAEastWalldist      =  64*inches         ;
        const  G4double  XIANorthWalldist     =  28*feet           -            XIAXY  ;
        const  G4double  XIACleanWalldist     =  20*inches         ;

        const  G4double  WallThickness        =  .3*CLHEP::m;
        const  G4double  FloorThickness       =  .3*CLHEP::m;
        G4double  LongWallLength       =  3*34*feet         ; //This is here for reference, we need to truncate it at the end of the world
        const  G4double  LabWidth             =  30*feet;
        const  G4double  LabLength            =  50*feet;
        const  G4double  HallWidth            =  8*feet;
        const  G4double  FloorHeight          =  fFondrenH/3.0     ;

        const  G4double  cleanroom_thickness  =  .01*CLHEP::m;
        const  G4double  cleanroom_height     =  1.9812*CLHEP::m;
        const  G4double  cleanroom_width      =  2.4384*CLHEP::m;
        const  G4double  cleanroom_length     =  24*feet;


        const  G4double  panellength          =  1*CLHEP::m;
        const  G4double  panelwidth           =  .5*CLHEP::m;
        const  G4double  panelheight          =  50*CLHEP::mm;

        const G4double acrylicslab_thickness = 1*inches;

};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

