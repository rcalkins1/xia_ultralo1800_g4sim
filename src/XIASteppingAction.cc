//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: XIASteppingAction.cc 74483 2013-10-09 13:37:06Z gcosmo $
//
/// \file XIASteppingAction.cc
/// \brief Implementation of the XIASteppingAction class

#include <iostream>

#include "XIASteppingAction.hh"
#include "XIAEventAction.hh"
#include "XIADetectorConstruction.hh"
#include "XIARun.hh"

#include "G4Step.hh"
#include "G4Track.hh"
#include "G4DynamicParticle.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolume.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

    XIASteppingAction::XIASteppingAction(XIAEventAction* eventAction)
: G4UserSteppingAction(),
    fEventAction(eventAction),
    fScoringVolume(0)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

XIASteppingAction::~XIASteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void XIASteppingAction::UserSteppingAction(const G4Step* step)
{
    if (!fScoringVolume) { 
        const XIADetectorConstruction* detectorConstruction
            = static_cast<const XIADetectorConstruction*>
            (G4RunManager::GetRunManager()->GetUserDetectorConstruction());
        fScoringVolume = detectorConstruction->GetScoringVolume();   
    }

    XIARun* run 
        = static_cast<XIARun*>(
                G4RunManager::GetRunManager()->GetNonConstCurrentRun());

    std::vector<G4double> * X1 = &(run->Get_X1() );
    std::vector<G4double> * Y1 = &(run->Get_Y1() );
    std::vector<G4double> * Z1 = &(run->Get_Z1() );
    std::vector<G4double> * T1 = &(run->Get_T1() );
    std::vector<G4double> * X3 = &(run->Get_X3() );
    std::vector<G4double> * Y3 = &(run->Get_Y3() );
    std::vector<G4double> * Z3 = &(run->Get_Z3() );
    std::vector<G4double> * T3 = &(run->Get_T3() );
    std::vector<G4double> * PX1 = &(run->Get_PX1() );
    std::vector<G4double> * PY1 = &(run->Get_PY1() );
    std::vector<G4double> * PZ1 = &(run->Get_PZ1() );
    std::vector<G4double> * KE1 = &(run->Get_KE1() );
    std::vector<G4double> * Edep = &(run->Get_Edep() );
    std::vector<G4double> * Ptype = &(run->Get_Ptype() );
    std::vector<G4double> * PX3 = &(run->Get_PX3() );
    std::vector<G4double> * PY3 = &(run->Get_PY3() );
    std::vector<G4double> * PZ3 = &(run->Get_PZ3() );
    std::vector<G4double> * KE3 = &(run->Get_KE3() );
    std::vector<G4double> * TrkID = &(run->Get_TrkID() );
    std::vector<G4double> * StepN = &(run->Get_StepN() );


    //Fill the primaries
    std::vector<G4double>  *  primary_Ptype  =  &(run->  Get_primary_Ptype()  );
    std::vector<G4double>  *  primary_X      =  &(run->  Get_primary_X()      );
    std::vector<G4double>  *  primary_Y      =  &(run->  Get_primary_Y()      );
    std::vector<G4double>  *  primary_Z      =  &(run->  Get_primary_Z()      );
    std::vector<G4double>  *  primary_PX     =  &(run->  Get_primary_PX()     );
    std::vector<G4double>  *  primary_PY     =  &(run->  Get_primary_PY()     );
    std::vector<G4double>  *  primary_PZ     =  &(run->  Get_primary_PZ()     );
    std::vector<G4double>  *  primary_KE     =  &(run->  Get_primary_KE()     );
    std::vector<G4double>  *  primary_T      =  &(run->  Get_primary_T()      );






    // get volume of the current step
    G4LogicalVolume* volume 
        = step->GetPreStepPoint()->GetTouchableHandle()
        ->GetVolume()->GetLogicalVolume();

    bool inpanel = false;
    if(volume->GetName() == "LVVetoPanel" ) inpanel = true; //G4cout <<"In Scintillator \n"; 

    // check if we are in scoring volume
    //if (volume != fScoringVolume && !inpanel) return;


    //   std::cout <<"Here 1\n";
    //Get the G4Track and particle associated with this step
    G4Track * track = step->GetTrack();



    //  std::cout <<"Here 2\n";
    const    G4DynamicParticle * particle = track->GetDynamicParticle();
    // std::cout <<"Here 3\n";


    // collect energy deposited in this step
    G4double edepStep = step->GetTotalEnergyDeposit();

    //Get the process
    const G4VProcess * process = track->GetCreatorProcess();
    bool isdecay=false;
    if(process)isdecay= process->GetProcessType() == fDecay;

    //This is easiiest
    //if(volume == fScoringVolume){

    //Let's not pin it down to a particular volume but consider anything inside the chamber
    //    std::cout <<  particle->GetPDGcode()  << std::endl; 
    //if(  1||particle->GetPDGcode() == 1000020040 
    //        //(step->GetPostStepPoint()->GetPosition()).x()/CLHEP::cm <=  30.1625 &&
    //        //(step->GetPostStepPoint()->GetPosition()).x()/CLHEP::cm >=  -30.1625 &&  
    //        //(step->GetPostStepPoint()->GetPosition()).y()/CLHEP::cm <=  30.1625 && 
    //        //(step->GetPostStepPoint()->GetPosition()).y()/CLHEP::cm >=  -30.1625 &&
    //        //(step->GetPostStepPoint()->GetPosition()).z()/CLHEP::cm >=  0 &&
    //        //            (step->GetPostStepPoint()->GetPosition()).z()/CLHEP::cm <=  15
    //  ){  




    //Alpha PDG = 1000020040
    //Let's save the alpha tracks
    //    if( particle->GetPDGcode() > 100 ) G4cout <<   particle->GetPDGcode() << G4endl;
    //if( 
    if( ((particle->GetPDGcode() == 1000020040  || fabs( particle->GetPDGcode()  ) == 13 ) )&&
        volume->GetName() == "XIA_Ar" 
        //  (step->GetPostStepPoint()->GetPosition()).x()/CLHEP::m <=  1.0 &&
        //  (step->GetPostStepPoint()->GetPosition()).x()/CLHEP::m >=  -1.0 &&  
        //  (step->GetPostStepPoint()->GetPosition()).y()/CLHEP::m <=  1.0 && 
        //  (step->GetPostStepPoint()->GetPosition()).y()/CLHEP::m >=  -1.0 &&
        //  (step->GetPostStepPoint()->GetPosition()).z()/CLHEP::m >=  -1.0 &&
        //  (step->GetPostStepPoint()->GetPosition()).z()/CLHEP::m <=  1.0

      ){

        Edep->push_back(   edepStep ); 
        Ptype->push_back(    particle->GetPDGcode()  );

        X1->push_back(     (step->GetPreStepPoint()->GetPosition()).x()/CLHEP::m     );
        Y1->push_back(     (step->GetPreStepPoint()->GetPosition()).y()/CLHEP::m     );
        Z1->push_back(     (step->GetPreStepPoint()->GetPosition()).z()/CLHEP::m     );
        T1->push_back(     (step->GetPreStepPoint()->GetGlobalTime()/CLHEP::s        ));
        X3->push_back(     (step->GetPostStepPoint()->GetPosition()).x()/CLHEP::m    );
        Y3->push_back(     (step->GetPostStepPoint()->GetPosition()).y()/CLHEP::m    );
        Z3->push_back(     (step->GetPostStepPoint()->GetPosition()).z()/CLHEP::m    );
        T3->push_back(     (step->GetPostStepPoint()->GetGlobalTime()/CLHEP::s       ));
        PX1->push_back(    (step->GetPreStepPoint()->GetMomentum()).x()/CLHEP::keV   );
        PY1->push_back(    (step->GetPreStepPoint()->GetMomentum()).y()/CLHEP::keV   );
        PZ1->push_back(    (step->GetPreStepPoint()->GetMomentum()).z()/CLHEP::keV   );
        KE1->push_back(    (step->GetPreStepPoint()->GetKineticEnergy()/CLHEP::keV   ));
        PX3->push_back(    (step->GetPostStepPoint()->GetMomentum()).x()/CLHEP::keV  );
        PY3->push_back(    (step->GetPostStepPoint()->GetMomentum()).y()/CLHEP::keV  );
        PZ3->push_back(    (step->GetPostStepPoint()->GetMomentum()).z()/CLHEP::keV  );
        KE3->push_back(    (step->GetPostStepPoint()->GetKineticEnergy()/CLHEP::keV  ));
        TrkID->push_back(  track->GetTrackID()                                       );
        StepN->push_back(  track->GetCurrentStepNumber()                             );

    }

    if( isdecay ){
        primary_X      ->push_back(  (step->GetPreStepPoint()->GetPosition()).x()/CLHEP::m    );
        primary_Y      ->push_back(  (step->GetPreStepPoint()->GetPosition()).y()/CLHEP::m    );
        primary_Z      ->push_back(  (step->GetPreStepPoint()->GetPosition()).z()/CLHEP::m    );
        primary_T      ->push_back(  step->GetPreStepPoint()->GetGlobalTime()/CLHEP::s       );
        primary_Ptype  ->push_back(  particle->GetPDGcode()                                   );
        primary_PX     ->push_back(  (step->GetPreStepPoint()->GetMomentum()).x()/CLHEP::keV  );
        primary_PY     ->push_back(  (step->GetPreStepPoint()->GetMomentum()).y()/CLHEP::keV  );
        primary_PZ     ->push_back(  (step->GetPreStepPoint()->GetMomentum()).z()/CLHEP::keV  );
        primary_KE     ->push_back(  step->GetPreStepPoint()->GetKineticEnergy()/CLHEP::keV  );
    }


    //This is how we used to store step information
    /*
       G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
       analysisManager->FillNtupleDColumn(13, edepStep );
       analysisManager->FillNtupleDColumn(14, particle->GetPDGcode() );


       analysisManager->FillNtupleDColumn(0, (step->GetPreStepPoint()->GetPosition()).x()/CLHEP::m );
       analysisManager->FillNtupleDColumn(1, (step->GetPreStepPoint()->GetPosition()).y()/CLHEP::m );
       analysisManager->FillNtupleDColumn(2, (step->GetPreStepPoint()->GetPosition()).z()/CLHEP::m );
       analysisManager->FillNtupleDColumn(3, (step->GetPreStepPoint()->GetGlobalTime()/CLHEP::s ));

       analysisManager->FillNtupleDColumn(4, (step->GetPostStepPoint()->GetPosition()).x()/CLHEP::m );
       analysisManager->FillNtupleDColumn(5, (step->GetPostStepPoint()->GetPosition()).y()/CLHEP::m );
       analysisManager->FillNtupleDColumn(6, (step->GetPostStepPoint()->GetPosition()).z()/CLHEP::m );
       analysisManager->FillNtupleDColumn(7, (step->GetPostStepPoint()->GetGlobalTime()/CLHEP::s ));


       analysisManager->FillNtupleDColumn(8,  (step->GetPreStepPoint()->GetMomentum()).x()/CLHEP::keV );
       analysisManager->FillNtupleDColumn(9,  (step->GetPreStepPoint()->GetMomentum()).y()/CLHEP::keV );
       analysisManager->FillNtupleDColumn(10, (step->GetPreStepPoint()->GetMomentum()).z()/CLHEP::keV );
       analysisManager->FillNtupleDColumn(11, (step->GetPreStepPoint()->GetKineticEnergy()/CLHEP::keV ));


       analysisManager->FillNtupleDColumn(15,  (step->GetPostStepPoint()->GetMomentum()).x()/CLHEP::keV );
       analysisManager->FillNtupleDColumn(16,  (step->GetPostStepPoint()->GetMomentum()).y()/CLHEP::keV );
       analysisManager->FillNtupleDColumn(17, (step->GetPostStepPoint()->GetMomentum()).z()/CLHEP::keV );
       analysisManager->FillNtupleDColumn(18, (step->GetPostStepPoint()->GetKineticEnergy()/CLHEP::keV ));


       analysisManager->FillNtupleDColumn(19, track->GetTrackID() );
       analysisManager->FillNtupleDColumn(20, track->GetCurrentStepNumber() );


    //  std::cout <<"Here 5\n";

    analysisManager->AddNtupleRow();
    */

    // }///

    //if(inpanel && fabs(particle->GetPDGcode()) == 13 ){
    if(inpanel){
        //        G4cout<<" In panel : PDG = "<< particle->GetPDGcode() << G4endl;

        if( (step->GetPreStepPoint()->GetPosition()).z() > 0    ){ //Top panel
            fEventAction->AddEdepVetoTop(edepStep);  
        } else{
            fEventAction->AddEdepVetoBottom(edepStep);  
        }

    }
    //    std::cout <<"Here 6\n";
    //    delete track, particle;

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

