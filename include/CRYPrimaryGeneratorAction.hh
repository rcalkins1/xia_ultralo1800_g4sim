//******************************************************************************
// PrimaryGeneratorAction.hh
//
// This class is a class derived from G4VUserPrimaryGeneratorAction for 
// constructing the process used to generate incident particles.
//
// 1.00 JMV, LLNL, JAN-2007:  First version.
//******************************************************************************
// 
#ifndef PrimaryGeneratorAction_h
#define PrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ThreeVector.hh"
#include "G4DataVector.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleGun.hh"
#include "G4GeneralParticleSource.hh"
#include "Randomize.hh"
#include "globals.hh"
#include "CRYSetup.h"
#include "CRYGenerator.h"
#include "CRYParticle.h"
#include "CRYUtils.h"
#include "vector"
#include "RNGWrapper.hh"
#include "CRYPrimaryGeneratorMessenger.hh"

#include "G4GenericMessenger.hh"

class G4Event;

class CRYPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    CRYPrimaryGeneratorAction(const char * filename);
    ~CRYPrimaryGeneratorAction();

  public:
    void GeneratePrimaries(G4Event* anEvent);
    void InputCRY();
    void UpdateCRY(std::string* MessInput);
    void CRYFromFile(G4String newValue);
    void inline SetZoffset(G4double h){ Zoffset= h; return; }
    void inline SetDataDir(std::string s= "/scratch/group/cdms/cry_v1.7/data/"){ datadir  =s; }

  private:
    std::vector<CRYParticle*> *vect; // vector of generated particles
    G4ParticleTable* particleTable;
    G4GeneralParticleSource* fGPS;
    G4ParticleGun* particleGun;
    CRYGenerator* gen;
    G4int InputState;
    CRYPrimaryGeneratorMessenger* gunMessenger;
    G4GenericMessenger * fMessenger;
    float Zoffset;
    std::string datadir;
};

#endif
