#include <iostream>
#include "TH1D.h"
#include <fftw3.h>
#include "FFTW_weights.h"


TH1D * FFTW_pulsefitter(TH1D* h_in){
    int N = h_in->GetNbinsX();
    TH1D* h_out = (TH1D*)h_in->Clone((TString)h_in->GetName()+"_out" );
    h_out->Reset();


    fftw_complex *in, *out, *rout;
    fftw_plan p;
    double realout[N];

    //allocate memory
    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    rout = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);

    //transform forward
    p = fftw_plan_dft_1d(N, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

    //Fill our data
    for(int i=0;i<N;++i){
        in[i][1] = 0 ; //complex 
        in[i][0] = h_in->GetBinContent(1+i); //real
        out[i][1] = 0 ; //complex 
        out[i][0] = 0; //real
    }

    //plug and chug
    fftw_execute(p); 

    //if you need to debug
    //  for(int i=0;i<N;++i) std::cout <<  out[i][1] << ", ";
    //  std::cout<<std::endl;

    //rescale only if 600 samples
    double cutoff=6;
    if(N==600){

        for(int i=0;i<N;++i) {
            //if(i>290 && i<310 ) continue;
     //       if(i<300 ) {
          if(i < cutoff ) {
                continue;
                //              if(sim0[i]!=0)  out[i][0] *= th0[i]/sim0[i];
                //              if(sim1[i]!=0)  out[i][1] *= th1[i]/sim1[i];
            } else {   
                out[i][0] =0 ;
                out[i][1] = 0 ;
            }

     //     if(i > cutoff ) {
     //         out[i][0] *= exp(-30*( i-cutoff )/(300-cutoff) );
     //         out[i][1] *= exp(-30*( i-cutoff )/(300-cutoff) );
     //     }


            //   if( th0[i]/sim0[i] >1e24) out[i][0] = 0;
            //  out[i][0] *= 0.5*erfc( abs(600.0-i)/50 ); 
            //     if(abs(300-i) > 50 ) continue;
            //  out[i][0] /= 1e6;
            //  out[i][1] /= 1e6;
            // if(i>5  && i<595){
            //     out[i][0] =0 ;
            //     out[i][1] = 0 ;

            // }
        }
        //there is one really high amplitude frequency that I don't trust .... yes, this is hand wavy but so is this entire business
        //      out[217][0] = 0;
        //      out[383][0] = 0; 
        //let's average them...
        //   out[217][0] = 0.5*( out[216][0] + out[218][0] );
        //   out[383][0] = 0.5*( out[382][0] + out[384][0] );

    }

    //  //reverse transform
    p = fftw_plan_dft_1d(N, out, rout, FFTW_BACKWARD, FFTW_ESTIMATE );
    //  //p = fftw_plan_dft_1d(N, out, rout, FFTW_BACKWARD, FFTW_ESTIMATE);
    fftw_execute(p); 
    //  

    //Fill our histogram
    for(int i=0;i<N;++i){
        h_out->SetBinContent( i+1, rout[i][0]/600.0 );     //(5704.0/ 338.0) scale factor = 16.8757
        //if(rout[i][1] != 0) std::cerr<<"Complex component! \n";
    }

    //if you need to debug
    //for(int i=0;i<N;++i) std::cout <<  out[i][0] << ", ";
    //std::cout<<std::endl;


    //clean up
    fftw_destroy_plan(p);
    fftw_free(in); 
    fftw_free(out); 
    fftw_free(rout); 



    return h_out;
}


